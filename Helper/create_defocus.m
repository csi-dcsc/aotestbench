function defocus = create_defocus( A )
    n = size(A,1);
    x = linspace(-1,1,n);
    X = meshgrid(x);
    X(~A) = 0;
    X = X/max(max(sqrt(X.^2 + X'.^2)));
    [th,rh] = cart2pol(X,X'); 
    defocus_vec = zernfun(2,0,rh(:),th(:),'norm');
    defocus = reshape(defocus_vec,[n n]);
end

