function [ mask_app ] = ApertureMask(na,r_ap)

    mask_app = zeros(na,na); 
        for ii = 1:na
            for jj = 1:na
                if sqrt(((ii-(na+1)/2)/na)^2 + ((jj-(na+1)/2)/na)^2) > r_ap
                   mask_app(ii,jj) = 0;
                else
                   mask_app(ii,jj) = 1;
                end
            end
        end

end