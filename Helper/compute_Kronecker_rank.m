function [M_APPROX,B,C,ACCURACY] = compute_Kronecker_rank(M,m,n,p,q,k,display)
% Computes the best k-Kronecker rank approximation of a matrix M
% Inputs:
% M : matrix with m x n blocks which are of dimensions p x q
% k : Kronecker rank of approximation

% Outputs:
% M_approx - the best k-Kronecker rank approximation of M
% B,C - matrices of the Kronecker products in the approximation
% rk - actual Kronecker rank of the matrix
% accuracy - vector 1:k of the 1-k-rank approximations
%
% Baptiste Sinquin, April 2017
% Edit: Peter Varnai, August 2017
% Copyright (c) 2018, Delft Center of Systems and Control 
%
% Compute rearrangement of the matrix M

T = SOK_reshuffle(M, m, n, p, q);
T = conj(T);

% The problem is now the best k-rank approximation of the matrix 'tensor',
% which is solved by the SVD
[U,S,V] = svd(T);
if (display)
    s = diag(S);
    figure,semilogy(s,'+','Linewidth',1)
    ll=ylabel('Singular values');
    set(ll,'interpreter','latex')
    ll=xlabel('Index of singular values');
    set(ll,'interpreter','latex')
end

% Compute the best k-rank approximation + accuracies with each added rank
M_APPROX = zeros(m*p,n*q);
ACCURACY = zeros(k,1);
B = cell(1,k);
C = cell(1,k);
for i = 1:k
    % Rearrange ith singular vectors into Kronecker product matrices
    % note: the multiplier singular value is distributed symmetrically,
    % so B & C is not unique
    Si_sqrt = sqrt(S(i,i));
    B{i} = reshape(Si_sqrt*U(:,i),m,n);
    C{i} = conj(reshape(Si_sqrt*V(:,i),p,q));
    
    % Update approximation of M
    M_APPROX = M_APPROX + kron(B{i},C{i});
    
    % Calculate accuracy of approximation in terms of Frobenius-norm
    ACCURACY(i) = norm(M_APPROX-M,'fro')/norm(M,'fro');    
end

 
end
