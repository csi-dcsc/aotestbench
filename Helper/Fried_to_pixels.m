function [Phase_fine] = Fried_to_pixels(Phase_fried,pupil,method)
% Function that interpolates Fried phase to a finer grid.
% For now it uses interp2 function

% Dimensions and pupil definitions
n = size(pupil,1);
m = size(Phase_fried,1);

% Get coordinates of each sample point w.r.t. pixel basis
x = linspace((0.5/n),1-(0.5/n),n); 
[X,Y] = meshgrid(x,x);
xf = linspace((0.5/m),1-(0.5/m),m);
[Xf,Yf] = meshgrid(xf,xf);

% Get interpolated phase 
Phase_fine = interp2(Xf,Yf,Phase_fried,X,Y,method);

% Extrapolate when necessary
not_nan = ~isnan(Phase_fine);
Phase_fine = scatteredInterpolant(X(not_nan),Y(not_nan),Phase_fine(not_nan),'natural','linear');
Phase_fine = Phase_fine(X,Y);
Phase_fine(logical(1-pupil)) = 0;

end
