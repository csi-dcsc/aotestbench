%% AO simulation using OOMAO.
% Last Modification: February 2021
% Pieter Piscaer
% Delft University of Technology 
%
% This simulator uses OOMAO in a black-box framework. All information is
% achieved as much as possible as if it was an experimental setup. Models,
% physical constants, influence matrices, etc. are identified from data and
% not assumed as known. 
clear all; close all; warning off; clc; 

%% Folder information
mkdir(cd); addpath(genpath(cd));
% Change file directories if desired.
folderlocation = cd;              % Directory of Main.m
% resfilebase = folderlocation;   % Results will be stored in here 
% datafilebase = folderlocation;  % Turbulence data will be stored in here (large files!)
resfilebase = 'D:\PhD\OOMAO Wavefront Estimation\Results Update';
datafilebase = 'D:\PhD\OOMAO Wavefront Estimation\Turbulence Data Update';

%%% Auto file generation or use specific data file
DataGeneration = 'auto';  % 'auto' for an automatic check on existing data, or 'file' for manual data input
DataFileName = 'YourFolders/YourFileName.mat'; % In case file selected, give here the datafile name

%% Specify methods to be tested
% A number of methods have been implemented, see CheckMethods for a list:
% Wavefront estimation (prediction) method (see CheckMethods.m for a list)
    Methods = {'MVM','AP','AP','IEKF'};     
    HighResControl = [0,0,0,0];   % 1: use predictions for high-res control, 0: do not apply high-res control
% Type of basis: Pixel (Standard), GRBF, DM, Fried
    BasisType = {'Fried','Pixel','Pixel','Pixel'};    
% If necessary, mention an advanced setting here (e.g. 'KF' or 'IF' if IEKF is used)
    SettingsType = {'','','','IF'};                  
% Type of value truncation for WFSless methods (ybox: only use the pixels within a smaller box around the center of the PSF, or ytrunc: discard/truncate pixels below a certain minimum value)
    TruncType = {'','ybox','ybox','ybox'};       
% Type of model computation: SI (system identification), FP (first principles) 
    ModelType = {'FP','','','SI'};                            
% Type of identification data if SI is used: Real, Reconstructed     
    SIDataType = {'Real','','','Real'};                                                               % 0 or 1 
% Choose control method          
    ControlMethods = {'MVM','MVM','MVM','MVM'};     
    LowResControl = 1;        % 1: use above control methods for low-res control, 0: do not apply any low-res control  
 
%% Simulation parameters 
% The parameters below are based on the input parameters for OOMAO.
% More advanced parameters in set_properties_file.m
% 7 properties structures will be created:
% 1. atm_prop: atmosphere (turbulence) specific properties
% 2. tel_prop: telescope specific properties
% 3. wfs_prop: SH specific properties
% 4. cam_prop: CCD camera specific properties
% 5. source_prop: source (NGS) specific properties
% 6. pred_prop: predictor properties
% 7. sim_prop: simulation properties

%%% Atmosphere
atm_prop.pvwind = [0.73 0.93];          % wind speed per turbulence layer in pixels per sampling time
atm_prop.r0 = 0.2;                      % Fried parameter [m]
atm_prop.dirwind = [0 pi/2];            % Direction of wind speed per turbulence layers [rad]
atm_prop.alt = [0 5000];                % Altitude per turbulence layer [m]
atm_prop.r0frac = [0.8 0.2];            % Fractional contribution of each layer (summed up should equal 1)

%%% Dimensions & Geometry
    % --> Important note: at the moment, the resolution tel_prop.n has to be an 
    % even number! The code will be updated to work for an uneven number of 
    % pixels in the next version.
wfs_prop.nL = 6;                            % SH resolution (also specifies Fried geometry resolution)
wfs_prop.nll = 5;                           % number of pixels per WFS lenslet subaperture
tel_prop.n = wfs_prop.nL*wfs_prop.nll;      % telescope resolution (has to match WFS resolution in OOMAO)
tel_prop.D = 1;                             % telescope diameter [m]
cam_prop.d = [0 2 -2];                      % defocus diversity constants
PhaseDiversities = {0,[2 3],[3],[3]};       % active phase diversity cameras for (high-resolution) prediction (and control)               
PhaseDiversitiesControl = {0,[1],[1],[1]};  % active phase diversity cameras for low-resolution control
          
%%% Brightness & Noise    
source_prop.mag = 8;                        % Magnitude of the source
cam_prop.read_noise = 2;                    % Read-out noise of the CCD
wfs_prop.validLenslet = 0.85;               % Fraction of light that needs to be collected for a lenslet to be considered within the aperture

%%% Value truncation
pred_prop.ytrunc = 5;                       % Value truncation if TruncType = 'ytrunc'
pred_prop.ybox = {0, 2, 2, 16};             % Size of box if TruncType = 'ybox' (take 0 if ytrunc is selected)
pred_prop.ytrunc_control = 0;               % Value truncation if CCD camera is used for control (is separate of the TruncType of the predictor)

%%% Model identification
pred_prop.r_graph_pix = ceil(max(atm_prop.pvwind)) + 1; % Radius constraint for VAR identification (see sparse_ARid.m)
pred_prop.r_graph_fried_pix = 2;                        % Radius constraint for Fried geometry grid
pred_prop.lambda_LS = 1e-5;                             % Regularization parameter of the Least-sqaures-based system identification method (in sparse_ARid.m)
pred_prop.cndQ = 4000;                                  % Regularization parameter for Q (see sparse_ARid.m for more info)

%%% If using GRBFs:
basis_prop.n_grbf = 8;                              % Width of the square GRBF-grid on which the GRBF centers are placed
basis_prop.gauscoef = 2;                            % Coefficient, leave at 2 for standard GRBFs
basis_prop.lambda_grbf = 15^basis_prop.gauscoef;    % Shape parameter, defines the width of the GRBFs
    
%% Simulation parameters (number of timesteps, MC parameters, etc.)    
sim_prop.MCflag = 0;                % make 1 for a MC simulation, when set 1, please check ExperimentSetup.m!
sim_prop.varpar = 'mag';         % parameter to be investigated -- see ExperimentSetup.m for overview!

%%% If a Monte Carlo run, load a pre-set file containing simulation parameters.
if sim_prop.MCflag
    % Load a pre-set mfile containing all simulation parameters,
    % This set can overrule the set parameters!
    % Make sure the tags above are update in the if-statement
    ExperimentSetup
else % Otherwise, use these parameters:
    sim_prop.showplot = 1;  % 1 for plots to be shown during simulation
    sim_prop.Npar = 1;      % Since no parameters are varied, this is 1
    sim_prop.Nt = 1200;      % Number of time steps
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 1;       % Number of Monte Carlo runs
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
    sim_prop.N_total_loops = sim_prop.Npar*sim_prop.NMC*sim_prop.Nt;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% End of initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start Building the AO setup
MethodsChecked = CheckMethods(Methods,ControlMethods);  % Checks if defined methods are known, specifies their type    
CheckInputsScript
for ipar = 1:sim_prop.Npar      % Loop over different parameter settings
for iMC = 1:sim_prop.NMC        % Loop over Monte Carlo Runs    
%% Prepare the final parameter settings
%%% Clear existing run (add parameters you want to reset)
clearvars Results pred
%%% Update settings for Monte Carlo experiments 
% (Some settings are automatically chosen based on the parameter settings,
% therefore it is necessary to update them within the 'ipar' loop.
CheckWhichExperiment
set_properties_file

h = waitbar(0,'Preparing simulation'); misc.pc = 1; % A waitbar is used to monitor the simulation process
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PART 1: Setting up the AO components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The next part will set up the AO simulation
% The code is based on OOMAO. However, this could be changed to other AO
% simulators if desired. In this case all files starting with 'my_' have to
% be adapted accordingly.

%% Telescope
% Create 2 telescope objects (one pixel- and one fried base)
    [tel,tel_fried] = my_telescope(tel_prop);

% Create distance matrices    
    tel_prop.pupilLogical = tel.pupilLogical;
    tel_prop.pupilLogical_fried = tel_fried.pupilLogical;
    tel_prop.DistanceMatrix = DistanceMatrix(tel.resolution,tel.D/(tel.resolution-1),double(tel.pupilLogical));
    tel_prop.DistanceMatrix_fried = DistanceMatrix(tel_fried.resolution,tel_fried.D/(tel_fried.resolution-1),double(tel_fried.pupilLogical));
    
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Telescope successfully generated');           
%% Atmosphere
% Create the atmosphere for simulation
    atm = my_atmosphere(atm_prop);
   
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Atmospere successfully generated')
%% Science cameras (focal plane cameras)
% Create measurement cameras for WFSless AO and result viewing cameras
    [cam,rcam,rcam_fried] = my_cameras(cam_prop);
   
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Cameras successfully generated')
%% Wavefront sensors 
% Set up the SH sensors
    [wfs] = my_wfs(wfs_prop,tel);
    dm_prop.validActuator = wfs{1}.validActuator;
   
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'WFSs successfully generated')
%% Deformable mirror 
% Create deformable mirrors for all paths. The number of deformable mirrors 
% is the same as the number of methods.
% Note: dm is of the same resolution as tel, dm_fried the same as tel_fried
    [dm,dm_fried,dm_hr] = my_dm(dm_prop,tel,tel_fried);

% Compute influence matrices and store them in dm_prop    
    dm_prop.DistanceMatrix = DistanceMatrix(dm_prop.dmres,dm_prop.actuatorSpacing,double(dm_prop.validActuator));
    [dm_prop.H,dm_prop.Hr] = GetInfluenceMatrix(dm{1},tel);
    [dm_prop.H_hr,dm_prop.Hr_hr] = GetInfluenceMatrix(dm_hr{1},tel);
    [dm_prop.H_fried,dm_prop.Hr_fried] = GetInfluenceMatrix(dm_fried{1},tel_fried);

    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'DMs successfully generated')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PART 2: Setting up the AO test benches
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Calibration, reference image, and set phase diversity
% Compute the poke matrix from DM to WFS and its pseudo inverse.
% This is stored in OOMAO's calibration object
%     calibDm = my_calibration(wfs,tel,dm,wfs_prop);
    % Get a reference amplitude corresponding to zero aberration
    ngs_tmp = source('magnitude',source_prop.mag)*tel;
    source_prop.amp = ngs_tmp.amplitude;
    pred_prop.amp = ngs_tmp.amplitude;
    % Create phase diversities
    for i = 1:cam_prop.ndiv
        cam_prop.PD{i} = cam_prop.d(i)*create_defocus(ones(size(tel.pupilLogical)));
    end
    
%% Set up the testbenches / link all corresponding components
% Creates a series of testbenches (lightpaths) based on the desired
% settings and methods to be tested.
    [ngs,ngswfs,ngsview,ngs_fried] = ...
        my_lightpath(source_prop,cam_prop,tel,dm,dm_hr,cam,wfs,rcam,tel_fried,dm_fried,rcam_fried);
    % Take reference images with the new alignments
    for i = 1:cam_prop.nrcams
        rcam{i}.referenceFrame = rcam{i}.frame;
    end

    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Lightpaths successfully generated')    
%% Couple the atmosphere to the telescope object in OOMAO
    tel = tel + atm;      % and couple atmosphere
    tel_fried = tel_fried + atm;      % and couple atmosphere
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PART 3: Basis functions, Modelling and System identification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Identify Fried geometry influence matrix
    dm_prop = FriedInfluenceMatrix(dm_prop,tel_prop,wfs_prop);
    pred_prop.Hfs = dm_prop.Hfs;    % Sparse Fried-geom DM influence matrix
    pred_prop.Hf = dm_prop.Hf;      % Fried-geom DM influence matrix
    pred_prop.validLenslets = wfs{1}.validLenslet; % WFS lenslets within aperture
    pred_prop.validActuators = wfs{1}.validActuator; % DM actuators corresponding to valid lenslets
    
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Influence matrix computed')
    
%% Estimate WFS measurement noise
    wfs_prop = IdentifyRs(dm_prop,tel_prop,wfs_prop);
    pred_prop.R = wfs_prop.R;       % WFS noise covariance matrix   
    
%% Get data regarding basis functions
% This function performs many pre-computations of the pixel and modal bases
% used in the methods to be simulated.
    basis = GetBasisData(pred_prop,dm_prop,tel_prop,cam_prop);

    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Basis data computed')
    
%% Generate or load identification data
% Only when ModelType is set to 'SI'
if strcmp(DataGeneration,'auto') && any(strcmp(ModelType,'SI'))
    try % Check if there is already data available for the chosen set of parameters   
        load(filestr);
        
        misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Data successfully loaded')
    catch % If not, create new data:
        waitbar(misc.pc/12,h,'Data does not exist, simulating new identification data, might take a while')
        
        [Id_data] = IdentificationRun(sim_prop.Nid,tel_prop,atm_prop,wfs_prop,cam_prop,pred_prop);
        save(filestr,'Id_data');
        
        misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Identification data generated') 
    end
elseif strcmp(DataGeneration,'file') && any(strcmp(ModelType,'SI')) 
    load(DataFileName); % Manual file input
    
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Data successfully loaded')
end

%% Identification of the G matrix 
    if any(strcmp(pred_prop.SensorType,'wfs'))  % Only when there is a SH sensor in the simulation
        [wfs_prop.Gphi,wfs_prop.cG] = IdentifyG(Id_data,wfs_prop.G);
    end

%% Get models
    [Models] = ModelIdentification(pred_prop,Id_data,basis,tel_prop,dm,tel,wfs_prop,dm_prop);
    
    misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Models successfully identified') 
    
%% Create Predictor class
   pred = my_predictor(pred_prop,Models,tel_prop,dm_prop,cam_prop,wfs_prop);
   
   misc.pc = misc.pc+1; waitbar(misc.pc/12,h,'Predictor class initialized') 
   close(h);
        
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PART 4: The simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialize simulation
    % Store current predictor in case simulation is reset
    pred0 = pred;
    Results0.Methods = Methods;
    Results0.ModelMSE = Models.ModelPhi_pix.MSE;
    
%% Start simulation
%%% Reset the simulation, in case a simulation is still active in the workspace
    InitPlot        % Initialize a new plot or waitbar (depends on showplot flag)
    ResetSim        % Reset the predictor object and DMs
    
%%% Start time loop
for it = 1:sim_prop.Nt
    UpdateWaitbar   % If showplot is false, update waitbar

%%% Apply dm voltage and simulate turbulence and 
    [ngs,ngswfs,ngsview,tel,dm,dm_hr,ngs_fried,dm_fried] = ...
        UpdateAll(pred,ngs,ngswfs,ngsview,tel,dm,dm_hr,ngs_fried,dm_fried);    

%%% Get measurements
    for k = 1:length(pred)
        pred{k} = GetMeasurements(pred{k},cam,wfs,rcam,ngsview,ngs_fried,dm,dm_hr);
    end

%%% Control and/or prediction   
    for k = 1:length(pred)
        %%% Compute control action low-res DM (if LowResControl == 1)
            pred{k} = ComputeControl(pred{k},control_prop{k},LowResControl);    
        %%% Compute high-res prediction 
            pred{k} = ComputePrediction(pred{k});
        %%% Compute high-res control action (if HighResControl(k) == 1)
            pred{k} = ComputeHighResControl(pred{k},control_prop{k},HighResControl(k));   
    end

%%% Results + Visualize
    Results = GetResults(Results,pred,ngs_fried,ngsview,rcam,cam);  
    UpdatePlot
end
%%%% END OF TIME LOOP %%%%

%% Store Monte Carlo Experiment Results
UpdateMCResults
    
end % END OF MONTE CARLO ITERATIONS
end % END OF PARAMETER CHANGING ITERATIONS

 