function [basis] = GetBasisData(pred_prop,dm_prop,tel_prop,cam_prop)
%%% Collects the basis information from OOMAO and puts it in a single
%%% structure "basis". Currently, the implemented basis functions are:
% - Pixel-wise basis
% - DM actuator influence functions
% - Gaussian radial basis functions

% Warning: This function also pre-computes (part of) the 2D-DFT matrix in order to
% speed up certain methods. Memory problems might occur for very large-scale
% systems.

%% short-hand notations
n = dm_prop.n;
ni = n*2*cam_prop.nyquistrate;
BasisTypes = pred_prop.BasisType;

%% 1. Use the DM influence functions as basis
if any(strcmp(BasisTypes,'DM')) 
    n = dm_prop.n;                              % Number of pixels in one dimension
    H_pix = dm_prop.H;                          % DM influence matrix - pixel basis
    m = dm_prop.dmres;                          % number of actuators in one direction
    H_f = dm_prop.Hfs;                          % DM influence matrix - Fried geometry
    sel = dm_prop.validActuator(:);             % Selection of valid actuators
    nbasis_a = size(H_f,2);                     % Number of valid actuators
    basis.n_active_act = sum(dm_prop.validActuator(:));
        
    % Normalize    
    basis_phase_pix_vec = zeros(n^2,nbasis_a);  % Initialize
    basis_phase_pix = zeros(n,n,nbasis_a);      % "
    for i = 1:nbasis_a
        basis_phase_pix_vec(:,i) = H_pix(:,i)/10^7;%/norm(H_pix(:,i));     % Normalized basis
        basis_phase_pix(:,:,i) = reshape(basis_phase_pix_vec(:,i),[n n]);
    end
    iHx = H_pix\basis_phase_pix_vec;

    % Normalize
    basis_phase_f_vec = zeros(m^2,nbasis_a);    
    basis_phase_f = zeros(m,m,nbasis_a);
    for i = 1:nbasis_a
        basis_phase_f_vec(sel,i) = H_f(:,i)/10^7;%/norm(H_f(:,i));         % Normalized basis 
        basis_phase_f(:,:,i) = reshape(basis_phase_f_vec(:,i),[m m]);
    end
        
    % Save in structure    
    basis.basis_phase_pix_vec = basis_phase_pix_vec;
    basis.basis_phase_pix = basis_phase_pix;
    basis.iH_pix_basis = iHx;
    basis.basis_phase_vec = basis_phase_f_vec;
    basis.basis_phase = basis_phase_f;
end

%% 2. Create (complex-valued) GPF basis using real-valued GRBFs
    % A basis will be constructed using Gaussian Radial Basis Functions 
    % (GRBFs) and this basis will be propagated in the image plane.
    
    % Note: only a regular square grid of GRBFs is implemented so far. 
    
if any(strcmp(BasisTypes,'GRBF'))
    % Dimensions
    n_grbf = pred_prop.basis_prop.n_grbf;
    basis.n_grbf = n_grbf;
    
    % GRBF [square grid]
    % --- Change these lines if the centers of the GRBFs should be
    % different from a regular square grid:
    lambda = pred_prop.basis_prop.lambda_grbf;      % parameter determining the width
    x = linspace(-1,1,dm_prop.n);                   % normalized pixel locations
    a = 1.05;                                       % Relative max. position of GRBF centers, w.r.t. pupil
    crbf = linspace(-a,a,n_grbf);                   % 
    GRBFspace = mean(diff(crbf))/2*tel_prop.D;
    [tmpx,tmpy] = meshgrid(crbf,crbf);              %
    c_cart = [vec(tmpx) vec(tmpy)];                 % GRBF center locations
    [GRBFs] = CreateGRBFs2(lambda,pred_prop.basis_prop.gauscoef,x,c_cart,1.5);
      
    % Fourier transform (focal plane basis)
    fftphasor = CreatePhasor(n,ni);             % FFT Phasor
    for j = 1:length(cam_prop.d)
        for i = 1:size(GRBFs,3)               % Propagate all Basis functions into the image plane
           [~,F_GRBFs_j(:,:,i)] = alt_propagateThrough(...
               GRBFs(:,:,i).*exp(1i*cam_prop.PD{j}),fftphasor,n,ni);
        end
        F_GRBFs{j} = F_GRBFs_j;
    end
    
    % Save
    basis.GRBFspace = GRBFspace;
    basis.GRBFs = GRBFs;
    basis.GRBFs_vec = reshape(GRBFs,[n^2,n_grbf^2]);
    for i = 1:length(cam_prop.d)
        basis.F_GRBFs{i} = F_GRBFs{i};
        basis.F_GRBFs_vec{i} = reshape(F_GRBFs{i},[ni^2,n_grbf^2]);
    end
    
    % Get active GRBF (with reasonable power within the aperture)
    GRBFs_vec_ap = basis.GRBFs_vec(tel_prop.pupilLogical(:),:);
    GRBF_powers = sum(abs(GRBFs_vec_ap));
    GRBF_maxpower = max(GRBF_powers);
    GRBF_threshold = 0.15*GRBF_maxpower;            % Change this coefficient if desired
    active_GRBFs = GRBF_powers >= GRBF_threshold;
    basis.active_GRBFs = active_GRBFs;
    basis.n_active_GRBFs = sum(active_GRBFs);
    
    % Get GRBF coefficients corresponding to a flat wavefront with
    % amplitude equal to the reference image taken
    basis.GRBF_coefs_flat = GRBFs_vec_ap(:,basis.active_GRBFs)\pred_prop.amp(tel_prop.pupilLogical);
end

%% 3. Pixel basis in Fourier domain
% Standard pixel-wise basis function. Due to the potential size of this
% system, it is the DFT matrix for n > 70 is no longer directly stored. 

if n < 71   % compuation of this basis for large systems might cause memory issues
    if any(strcmp(BasisTypes,'Pixel'))
        
        % Fourier transform (Image plane basis)
        fftphasor = CreatePhasor(n,ni);             % FFT Phasor
        for j = 1:length(cam_prop.d)
            for i = 1:n^2               
                % Propagate all basis functions onto the image plane
                pix_i = zeros(n^2,1);
                pix_i(i) = 1;
                pix_i = reshape(pix_i,[n n]);
                [~,F_pix_j(:,:,i)] = alt_propagateThrough(...
                   pix_i.*exp(1i*cam_prop.PD{j}),fftphasor,n,ni);
            end
            F_pix{j} = F_pix_j;
        end
        
        % Save
        for i = 1:length(cam_prop.d)
            basis.F_pix{i} = F_pix{i};
            basis.F_pix_vec{i} = reshape(F_pix{i},[ni^2,n^2]);
        end

        %%% Kronecker representation
        for j = 1:length(cam_prop.d)
            tmp =  basis.F_pix_vec{j};
            basis.F1_pix_vec{j} = tmp(1:sqrt(size(tmp,1)),1:sqrt(size(tmp,2)))./sqrt(tmp(1,1));
            basis.F2_pix_vec{j} = basis.F1_pix_vec{j};
        end  
    end
    basis.largescaleflag = false;
else
    % Resolution too large to store the DFT matrix completely! 
    % Only storing the ybox one. Be careful, unverified method, 
    % errors might occur!
    basis.largescaleflag = true;
end

%%% Store all basis types
basis.BasisTypes = BasisTypes;

%% Other: Kronecker/tensor structures

% Pixel basis
if any(strcmp(BasisTypes,'Pixel'))
    for j = 1:length(cam_prop.d)  
        try
        basis.Kronecker.Cpix{j} = basis.F_pix_vec{j};   
        tmp = basis.Kronecker.Cpix{j};
        basis.Kronecker.C1pix{j} = tmp(1:sqrt(size(tmp,1)),1:sqrt(size(tmp,2)))./sqrt(tmp(1,1));
        basis.Kronecker.C2pix{j} = basis.Kronecker.C1pix{j};
        catch
        end
    end
end

% GRBFs
if any(strcmp(basis.BasisTypes,'GRBF'))
    % Kronecker matrices for GRBF basis Pupil function
    [~,basis.B1,basis.B2,accur] = ...
        compute_Kronecker_rank(basis.GRBFs_vec,...
        tel_prop.n,pred_prop.basis_prop.n_grbf,...
        tel_prop.n,pred_prop.basis_prop.n_grbf,1,0);
    basis.B1 = cell2mat(basis.B1);
    basis.B2 = cell2mat(basis.B2);
    basis.iB1 = pinv(basis.B1);
    basis.iB2 = pinv(basis.B2);

% Kronecker basis Amplitude apodization function
    tmp = svd(pred_prop.amp);
    tmp = find(tmp > 1e-7);
    kr_rank = tmp(end);
    [~,basis.A1,basis.A2,accur] = ...
        compute_Kronecker_rank(vec(pred_prop.amp),...
        tel_prop.n,1,tel_prop.n,1,kr_rank,0);
    basis.A1 = cell2mat(basis.A1);
    basis.A2 = cell2mat(basis.A2);

% y = |Cx|^2 + v    
for j = 1:length(cam_prop.d)  
    basis.Kronecker.C{j} = basis.F_GRBFs_vec{j};   
    basis.Kronecker.iC{j} = pinv( basis.Kronecker.C{j});
    [~,basis.Kronecker.C1{j},basis.Kronecker.C2{j},accur] = ...
        compute_Kronecker_rank(basis.F_GRBFs_vec{j},...
        tel_prop.n*cam_prop.nyquistrate*2,pred_prop.basis_prop.n_grbf,...
        tel_prop.n*cam_prop.nyquistrate*2,pred_prop.basis_prop.n_grbf,1,0);
    basis.Kronecker.C1{j} = cell2mat(basis.Kronecker.C1{j});
    basis.Kronecker.C2{j} = cell2mat(basis.Kronecker.C2{j});
    basis.Kronecker.iC1{j} = pinv(basis.Kronecker.C1{j});
    basis.Kronecker.iC2{j} = pinv(basis.Kronecker.C2{j});
end
end



end

