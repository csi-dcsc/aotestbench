function [g] = CreateGRBFs2(lambda,a,x,c_cart,rx)
%%% Creates a basis of Gaussian Radial Basis Functions squared!!!!!!:

% Arguments:
%   lambda: Shape parameter
%   x: x grid, linear spacing: linspace(-a,a,N), where a is ~ the range
%   c_cart: centers in cartesian coordinates, 2 columns [x y]
%   rx: maximum radius to be considered

% Outputs:
%   g: GRBFs


if size(c_cart,2) ~= 2
    error('centers should be given in a matrix with two columns [x y]')
end
xc = c_cart(:,1);
yc = c_cart(:,2);

N = length(x);
y = x;
K = length(xc);

% Sample grid
[X,Y] = meshgrid(x,y);

% Aperture based on range
Ap = double(sqrt(X.^2 + Y.^2) <= rx);

% Create the GRBFs on the specied square grid
g = zeros(N,N,K);
for i = 1:K
%         g(:,:,i) = exp(-lambda.*((X-xc(i)).^2 + (Y-yc(i)).^2) ).*Ap;
        g(:,:,i) = exp(-lambda.*((X-xc(i)).^(2*a) + (Y-yc(i)).^(2*a)) ).*Ap;
%         dgx(:,:,i) =  -2*lambda*(X - xc(i)).*g(:,:,i);
%         dgy(:,:,i) =  -2*lambda*(Y - yc(i)).*g(:,:,i);
end



end