function [wfs] = my_wfs(wfs_prop,tel)
% Creates a cell array of shack hartman sensors. Each WFS-based method 'i'
% will get its own WFS object wfs{i}
    for i = 1:wfs_prop.nwfss
        wfs{i} = shackHartmann(wfs_prop.nL,wfs_prop.n,wfs_prop.validLenslet);
        ngswfs{i} = source('magnitude',wfs_prop.mag);
        % OOMAO's calibration step
        ngswfs{i} = ngswfs{i}.*tel*wfs{i};
        wfs{i}.INIT;         
        % Set noise properties
        wfs{i}.camera.photonNoise = wfs_prop.photon_noise;       
        wfs{i}.camera.readOutNoise = wfs_prop.read_noise; 
        wfs{i}.camera.frameListener.Enabled = false;
    end    
end
