function [ngs,ngswfs,ngsview,ngsfried] = ...
                my_lightpath(source_prop,cam_prop,tel,dm,dm_hr,cam,wfs,rcam,tel_fried,dm_fried,rcam_fried)
% This function forms all the testbenches, linking all corresponding 
% dm, telescope, atmosphere, camera, wfs and source objects:
%
% ngs: WFSless testbenches linked to the 'measurement taking cameras': cam
%       ngs{i,j} is the j-th WFSless method, i-th phase diversity camera 
% ngsview: noiseless lightpaths for both WEFless and WFS-based methods,
%           ngs{j} corresponds to the j-th method. 
% ngsview{end} corresponds to the noiseless + no control path
% ngswfs: WFS-based testbenches linked to their corresponding WFSs. 
%
%% Short hand notations         
    nmcams = size(cam,2);
    ne = size(cam,1);
    nrcams = length(rcam);
    nwfss = length(wfs);
    tel_ap = tel.pupilLogical;
    msc = source_prop.mag;
    mwfs = source_prop.magwfs;

%% Define phase diversities (defocus) in each of the cameras
    for i = 1:ne
        for j = 1:nmcams
            %  cam{i,j}.PD ....... cam{i,j}.cPD
            PD{i,j} = cam_prop.d(i)*create_defocus(ones(size(tel.pupilLogical))); 
        end
    end

%% WFSless lightpaths
% Set op the WFSless lightpaths (paths that go to a science camera)
    ngs = cell(ne,nmcams);      % lighpaths used for estimation
    ngsview = cell(1,nrcams);   % lightpaths used for viewing the resulting (noiseless + in focus) PSF
    icam = 0;
    for j = 1:length(dm)
        if strcmp(source_prop.PathType{j},'cam') || strcmp(source_prop.PathType{j},'wfs-cam')
            icam = icam + 1;
            for i = 1:ne
               ngs{i,icam} = source('magnitude',msc)*tel*dm{j}*dm_hr{j}*{tel_ap,PD{i,icam}}*cam{i,icam};
    %           set.amplitude(ngs{i,j},tel_ap);
            end
            ngsview{j} = source('magnitude',msc)*tel*dm{j}*dm_hr{j}*rcam{j};
        end
    end
    
%% WFS lightpaths
    iwfs = 0;
    for j = 1:length(dm)
        if strcmp(source_prop.PathType{j},'wfs') || strcmp(source_prop.PathType{j},'wfs-cam') 
            iwfs = iwfs + 1;
            ngswfs{iwfs} = source('magnitude',mwfs)*tel*dm{j}*dm_hr{j}*wfs{iwfs};
            if strcmp(source_prop.PathType{j},'wfs') 
                ngsview{j} = source('magnitude',mwfs)*tel*dm{j}*dm_hr{j}*rcam{j};
            end
        end
    end
    if j == 0
         ngswfs = [];
    end

%% No control path    
    ngsview{end} = source('magnitude',msc)*tel*rcam{nrcams}; % no compensation

%% Fried lightpaths (for reference)
% WFSless lightpaths
    ngsfried = cell(1,nrcams); % lightpaths used for viewing the resulting psf
    for j = 1:length(dm_fried)
        if strcmp(source_prop.PathType{j},'cam') || strcmp(source_prop.PathType{j},'wfs-cam')
            ngsfried{j} = source('magnitude',msc)*tel_fried*dm_fried{j}*rcam_fried{j};
        end
    end
    for j = 1:length(dm_fried)
        if strcmp(source_prop.PathType{j},'wfs')
            ngsfried{j} = source('magnitude',mwfs)*tel_fried*dm_fried{j}*rcam_fried{j};
        end
    end
    ngsfried{end} = source('magnitude',msc)*tel_fried*rcam_fried{nrcams}; % no compensation

%% Display preview
cam_test = cam{1,1}.frame;
for i = 2:ne
    cam_test = [cam_test cam{i,1}.frame];
end
htmp = figure; 
imagesc(cam_test);
colorbar;
title('Example of diversities')
pause(3);
close(htmp)    
    
