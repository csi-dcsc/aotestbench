function calibDm = my_calibration(wfs,tel,dm,wfs_prop)
% This function uses OOMAO's calibration function to calibrate each DM-WFS
% pair.
    for i = 1:wfs_prop.nwfss  
        ngswfs{i} = source('magnitude',wfs_prop.mag);
        ngswfs{i} = ngswfs{i}.*tel;
        calibDm{i} = calibration(dm{end},wfs{i},ngswfs{i},...
                        ngswfs{i}.wavelength,dm{end}.nActuator,'cond',1e2); 
        close all;
    end
end
