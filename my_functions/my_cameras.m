function [ cam,rcam,rcam_fried ] = my_cameras( cam_prop )
% This function creates several cameras:
% cam:  measurement taking cameras. These cameras suffer from read-out noise
%       and shot noise. For each method 'i' and phase diversity image 'j', a
%       dedicated camera object is created: cam{i,j}
% rcam: 'Result viewing cameras'. These cameras are noiseless and serve for
%       reference purposes only. Their resolution is fine, with
%       nyquistsampling it is equal to 2*n, n being the telescope
%       resolution
% rcam_fried: Same as rcam, but for Fried geometry resolution, i.e.
%           assuming Nyquist sampling 2*n_fried

%% Short hand notations
ne = cam_prop.ndiv;
nmcams = cam_prop.nmcams;
nrcams = cam_prop.nrcams;
fieldstopsize = cam_prop.fieldstopsize;
fieldstopsize_fried = cam_prop.fieldstopsize_fried;
photon_noise = cam_prop.photon_noise;
read_noise = cam_prop.read_noise;
nyquistrate = cam_prop.nyquistrate;

%% measurement taking cameras
cam = cell(ne,nmcams);
for j = 1:nmcams
    for i = 1:ne     % 3 diversity images
        cam{i,j} = imager('fieldStopSize',fieldstopsize,'nyquistSampling',nyquistrate); 
        cam{i,j}.frameListener.Enabled = false;
        cam{i,j}.photonNoise = photon_noise;
        cam{i,j}.readOutNoise = read_noise;
        cam{i,j}.nPhotonBackground = cam_prop.nPhotonBackground;
%         cam{i,j}.cPD = cam_prop.d(i);
    end
end

%% result viewing cameras
rcam = cell(nrcams,1);
for i = 1:nrcams
    rcam{i} = imager('fieldStopSize',fieldstopsize,'nyquistSampling',nyquistrate); 
    rcam{i}.frameListener.Enabled = false;
end

%% result viewing Fried cameras
rcam_fried = cell(nrcams,1);
for i = 1:nrcams
    rcam_fried{i} = imager('fieldStopSize',fieldstopsize_fried,'nyquistSampling',nyquistrate); 
    rcam_fried{i}.frameListener.Enabled = false;
end

end

