function [ dm,dm_fried,dm_hr ] = my_dm( dm_prop,tel,tel_fried )
% This function creates DM objects corresponding to both a pixel-base and
% Fried geometry resolution: 
% dm: dm used for control methods based on full (high-resolution) pixel grid
% dm_fried: dm used for methods based on fried-geometry pixel grid
% dm_hr: high resolution dm
%
% Note: this needs minor additions to OOMAO, please use OOMAO as is
% included in the downloaded folder.

%% Short hand notations
ndms = dm_prop.ndms;
nma = dm_prop.dmres;
n = dm_prop.n;
validActuator = dm_prop.validActuator;
cdm = dm_prop.cdm;

%% DMs - pixel base
dm = cell(ndms,1);
for i = 1:ndms
    dm{i} = deformableMirror(nma,...
        'modes',influenceFunction('monotonic',cdm),...
        'resolution',n,...
        'validActuator',validActuator);%,...
%         'telD',tel.D);       
     % Get influence matrix 
%      GetInfluenceMatrix(dm{i},tel);
end

%% DMs - pixel base fried
dm_hr = cell(ndms,1);
for i = 1:ndms
    dm_hr{i} = deformableMirror(n,...
        'modes',influenceFunction('monotonic',0.01*cdm),...
        'resolution',n,...
        'validActuator',logical(ones(size(tel.pupilLogical,1))) );%,...
%         'telD',tel.D);       
     % Get influence matrix 
%      GetInfluenceMatrix(dm_hr{i},tel);
end

%% DMs - Fried
dm_fried = cell(ndms,1);
for i = 1:ndms
    dm_fried{i} = deformableMirror(nma,...
        'modes',influenceFunction('monotonic',cdm),...
        'resolution',nma,...
        'validActuator',validActuator);% ,...
%         'telD',tel.D);       
     % Get influence matrix 
%      GetInfluenceMatrix(dm_fried{i},tel_fried);
end

