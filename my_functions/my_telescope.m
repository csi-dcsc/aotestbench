function [tel,tel_fried] = my_telescope(tel_prop)
% This function creates 2 telescope objects:
% tel: resolution equal to tel_prop.n
% tel_fried: resultion equal to Fried geometry w.r.t. the WFS (tel_prop.n_fried)

%%% Construct telescope
tel = telescope(tel_prop.D,...
    'resolution',tel_prop.n,...
    'fieldOfViewInArcsec',tel_prop.fov,...
    'samplingTime',1/tel_prop.fs);


%%% Construct telescope fried geometry
tel_fried = telescope(tel_prop.D,...
    'resolution',tel_prop.n_fried,...
    'fieldOfViewInArcsec',tel_prop.fov,...
    'samplingTime',1/tel_prop.fs);

end

