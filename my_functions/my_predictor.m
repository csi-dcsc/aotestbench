function [pred] = my_predictor(pred_prop,models,tel_prop,dm_prop,cam_prop,wfs_prop)
%%% This function initialises a Predictor object for the given settings    

%%% Short hand notations
    pred = cell(length(pred_prop.Methods),1);
    Methods = pred_prop.Methods;
    ControlMethods = pred_prop.ControlMethods;
    SensorType = pred_prop.SensorType;
    PredType = pred_prop.PredType;
    BasisType = pred_prop.BasisType;
    SettingsType = pred_prop.SettingsType;
    ModelType = pred_prop.ModelType;
    SIDataType = pred_prop.SIDataType;
    
%%% Initialize counters    
    camera_count = 1;
    wfs_count = 1;
 
%%% Loop through all methods    
    for i = 1:length(Methods)
        
        %%% store the necessary DM info into a new structure
        dm_info{i}.coefs = zeros(size(dm_prop.Hf,2),1);
        dm_info{i}.coefs_hr = zeros(size(dm_prop.H_hr,2),1);
        dm_info{i}.H = dm_prop.H;
        dm_info{i}.Hr = dm_prop.H(tel_prop.pupilLogical(:),:);
        dm_info{i}.H_hr = dm_prop.H_hr;
        dm_info{i}.Hr_hr = dm_prop.H_hr(tel_prop.pupilLogical(:),:);
        dm_info{i}.validActuator = dm_prop.validActuator;
        dm_info{i}.validActuator_hr = tel_prop.pupilLogical;
        
        %%% Control matrices
        dm_info{i}.iHr = pinv(dm_prop.H(tel_prop.pupilLogical(:),:));
        Hrhr = dm_info{i}.Hr_hr;
        tmp = mean(diag(Hrhr'*Hrhr));
        dm_info{i}.iHr_hr = (Hrhr'*Hrhr + 1e-16*tmp*eye(size(Hrhr,2)))\Hrhr';
        
        %%% Lightpath properties
        ngs_info{i}.amplitude = pred_prop.amp;
        
        %% Check if method is WFS-based or WFSless      
        
        %% 1. WFSless
        if strcmp(SensorType(i),'cam') % WFSless predictor
            % This also checks if it is a camera-based or wfs-based method
            % via the type varagin object
            sensor_info{i} = cam_prop;
            sensor_info{i}.tag = 'FPC';     % focal plane camera
            
            % Construct main predictor object
            pred{i} = Predictor(...
                        Methods{i},...          % Method type 
                        ControlMethods{i},...   % Method type 
                        SensorType(i),...       % Sensor type (wfs, cam, wfs-cam)
                        PredType(i),...         % Prediction type (no or KF)
                        BasisType(i),...        % Basis function type (e.g. pixel, DM, GRBF,...)
                        ModelType(i),...        % Model identification type (e.g. SI or FP)
                        SIDataType(i),...       % Identification data type (e.g. Real or Reconstructed)    
                        dm_info{i},...          % corresponding DM
                        tel_prop,...            % the telescope (only one)
                        ngs_info{i},...         % lightpath(s)
                        sensor_info{i}...       % varagin - corresponding camera(s) (phase diversity)
                        );    
            pred{i}.SensorNumber = camera_count;           
            camera_count = camera_count + 1;
            
            % Specify Settings
            pred{i}.PSF_trunc_type = pred_prop.TruncType{i};    
            pred{i}.SettingsType = SettingsType{i};      

            % Basis and model
            pred{i}.basis = models.basis;
            pred{i}.Models = models;   
            
            % PSF related parameters
            pred{i}.ytrunc = pred_prop.ytrunc;   
            pred{i}.ybox = pred_prop.ybox{i};
            pred{i}.ytrunc_control = pred_prop.ytrunc_control;   
            pred{i}.activePDs = pred_prop.PhaseDiversities{i};
            pred{i}.activePDsControl = pred_prop.PhaseDiversitiesControl{i};
            
            % Check if dimensions very large for the compuation of the
            % DFT matrices, if so only partially compute
            if ~models.basis.largescaleflag 
                % Normal
                if strcmp(pred{i}.PSF_trunc_type,'ybox')
                    clear pred{i}.basis.F_pix_vec
                    pred{i}.basis.F_pix_box = ComputeFourierBox(pred_prop.ybox{i},models.basis.F_pix,pred{i}.pupilLogical);             
                end 
                clear pred{i}.basis.F_pix
            else    
                % Directly compute ybox one as the full DFT is too large to store
                if strcmp(pred{i}.PSF_trunc_type,'ybox')
                    ybox = min(10,pred_prop.ybox{i});
                    pred{i}.basis.F_pix_box = ComputeFourierBoxOnly(ybox,pred{i}.pupilLogical);  
                end
            end
            
        %% 2. WFS-based predictor
        elseif strcmp(SensorType(i),'wfs')
            % This also checks if it is a camera-based or wfs-based method
            % via the type varagin object
            sensor_info{i} = wfs_prop;
            sensor_info{i}.tag = 'WFS'; 
            
            % Construct main predictor object
            pred{i} = Predictor(...
                        Methods{i},...          % Method type 
                        ControlMethods{i},...   % Method type 
                        SensorType(i),...       % Sensor type (WFS or cam)
                        PredType(i),...         % Prediction type (no or KF)
                        BasisType(i),...        % Basis function type (e.g. pixel, DM, GRBF,...)
                        ModelType(i),...        % Model identification type (e.g. SI or FP)
                        SIDataType(i),...       % Identification data type (e.g. Real or Reconstructed)
                        dm_info{i},...          % corresponding DM
                        tel_prop,...            % the telescope (only one)
                        ngs_info{i},...         % corresponding lightpath(s) (phase diversity)
                        sensor_info{i}...
                     );          
            pred{i}.SensorNumber = wfs_count;      
            wfs_count = wfs_count + 1;  
                 
            % Models
            pred{i}.Models = [];
            pred{i}.Models.ModelPhi_diag.iH = models.ModelPhi_diag.iH; 
            
            %%% Wavefront reconstruction / MVM matrix
            Cphi_diag = models.ModelPhi_diag.Cm;
            G_diag = models.ModelPhi_diag.G;
            pred{i}.Models.ModelPhi_diag.nullG = null(G_diag);
            R_aug = blkdiag(pred_prop.R,pred_prop.R(1,1)*eye(2));
            G_diag_aug = [G_diag; null(G_diag)'];
            pred{i}.MVM = Cphi_diag*G_diag_aug'/...
                (G_diag_aug*Cphi_diag*G_diag_aug' + R_aug);

        %% 3. Combination WFS and camera
        elseif strcmp(SensorType(i),'wfs-cam') 
            % This also checks if it is a camera-based or wfs-based method
            % via the type varagin object
            sensor_info1{i} = cam_prop;
            sensor_info1{i}.tag = 'FPC';     % focal plane camera
            sensor_info2{i} = wfs_prop;
            sensor_info2{i}.tag = 'WFS'; 
            
            % Construct main predictor object
            pred{i} = Predictor(...
                        Methods{i},...          % Method type 
                        ControlMethods{i},...          % Method type 
                        SensorType(i),...       % Sensor type (wfs, cam, wfs-cam)
                        PredType(i),...         % Prediction type (no or KF)
                        BasisType(i),...        % Basis function type (e.g. pixel, DM, GRBF,...)
                        ModelType(i),...        % Model identification type (e.g. SI or FP)
                        SIDataType(i),...       % Identification data type (e.g. Real or Reconstructed)                       
                        dm_info{i},...          % corresponding DM
                        tel_prop,...            % the telescope (only one)
                        ngs_info{i},...         % lightpath(s)
                        sensor_info1{i},...
                        sensor_info2{i}...       % varagin - corresponding camera(s) (phase diversity)
                        );    
            pred{i}.SensorNumber = [wfs_count, camera_count];           
            camera_count = camera_count + 1;
            wfs_count = wfs_count + 1;  
            
            % Specify Settings
            pred{i}.TruncType = pred_prop.TruncType{i};    
            pred{i}.SettingsType = SettingsType{i};      

            % Model and basis
            pred{i}.basis = models.basis;
            pred{i}.Models = models;   
            
            % PSF related parameters
            pred{i}.ytrunc = pred_prop.ytrunc;   
            pred{i}.ybox = pred_prop.ybox{i};
            pred{i}.ytrunc_control = pred_prop.ytrunc_control;   
            pred{i}.activePDs = pred_prop.PhaseDiversities{i};
            
            % Check if dimensions very large for the compuation of the
            % DFT matrices, if so only partially compute
            if ~models.basis.largescaleflag 
                % Normal
                if strcmp(pred{i}.TruncType,'ybox')
                    clear pred{i}.basis.F_pix_vec
                    pred{i}.basis.F_pix_box = ComputeFourierBox(pred_prop.ybox{i},models.basis.F_pix,pred{i}.pupilLogical);             
                end 
                clear pred{i}.basis.F_pix
            else    
                % Directly compute ybox one as the full DFT is too large to store
                if strcmp(pred{i}.TruncType,'ybox')
                    ybox = min(10,pred_prop.ybox{i});
                    pred{i}.basis.F_pix_box = ComputeFourierBoxOnly(ybox,pred{i}.pupilLogical);  
                end
            end         
            
            %%% Wavefront reconstruction / MVM matrix
            pred{i}.Models.ModelPhi_diag.iH = models.ModelPhi_diag.iH; 
            Cphi_diag = models.ModelPhi_diag.Cm;
            G_diag = models.ModelPhi_diag.G;
            pred{i}.Models.ModelPhi_diag.nullG = null(G_diag);
            R_aug = blkdiag(pred_prop.R,pred_prop.R(1,1)*eye(2));
            G_diag_aug = [G_diag; null(G_diag)'];
            pred{i}.MVM = Cphi_diag*G_diag_aug'/...
                (G_diag_aug*Cphi_diag*G_diag_aug' + R_aug);
            
        end    
        
        %% For all methods 
        pred{i}.PathNumber = i;    
        
    end % To next method

end
