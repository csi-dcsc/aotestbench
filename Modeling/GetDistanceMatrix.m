function GetDistanceMatrix(obj)
    n = obj.nActuator;
    mask = double(obj.validActuator);
    sel = obj.validActuator(:);
    infA = (mask./mask);
    infA(isnan(infA)) = inf;
    x_grid = meshgrid([1:n]*obj.actuatorSpacing).*infA;
    posvecs = [vec(x_grid) vec(x_grid')];
    DiM = zeros(n^2,n^2);
    for i = 1:n^2
        DiM(i,:) = vec(sqrt( (x_grid - posvecs(i,1)).^2 + (x_grid'-posvecs(i,2)).^2))';
    end
    obj.DistanceMatrix = DiM(sel,sel);
end