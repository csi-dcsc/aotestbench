function [C,L,obj] = Lin_PSF(obj,GPF0,ysel)    
% Linearize the output equation for WFSless AO

% Get info from object
npix = size(GPF0,1);
nyquistrate = obj.NyquistSampling;
if strcmp(obj.BasisType,'Pixel')            
%     phase_basis = reshape(eye(npix^2),[npix npix npix^2]);
elseif strcmp(obj.BasisType,'DM')
    phase_basis = obj.basis.basis_phase_pix;
    m = size(phase_basis,3);
elseif strcmp(obj.BasisType,'GRBF')
    phase_basis = obj.basis.GRBFs;
    m = size(phase_basis,3);
end
ap = obj.pupilLogical;
% ap = logical(ones(size(ap)));
%             phase_basis = obj.basis.GRBFs;

%%% Get dimensions
    n = size(GPF0,1);
    ni = 2*nyquistrate*n;
    fftPhasor = obj.CreatePhasor(n,ni);

%     % tmp - square pupil
%     ap_sq = zeros(n);
%     n_sq = n/2;
%     ap_sq(n/4+1:3*n/4,n/4+1:3*n/4) = 1;
%     xsel_sq = logical(ap_sq(obj.pupilLogical));     
    
    
%%% Constant term
    [~,Img0] = alt_propagateThrough(GPF0,fftPhasor,n,ni);
    Img0_vec = reshape(Img0,[ni^2,1]);
    Img0_vec_sel = Img0_vec(ysel,:);
    Cm = abs(Img0).^2;
    Cv = reshape(Cm,[ni^2,1]);
    Cv_sel = Cv(ysel,:);

%%% Linear term
    if strcmp(obj.BasisType,'Pixel')
%         FTM_sel = 1i*F_pix_vec(ysel,ap(:));
%         GPF0_vec = GPF0(ap(:));
%         d_Img1_vec_sel = 1i*F_pix_vec(ysel,ap(:)).*GPF0(ap(:)).';
        tmp1 = GPF0(ap(:)).';
        tmp2 = 2*conj(Img0_vec_sel);
        if strcmp(obj.TruncType,'ybox')
            FF = obj.basis.F_pix_box.F_pix_box_vec_ap{1};
        else
            F_pix_vec = obj.basis.F_pix_vec{1};
            FF = F_pix_vec(ysel,ap(:));
        end
        Lv_sel = real(1i*FF.*tmp1.*tmp2);
%         Lv_sel2 = real(2*1i*diag(conj(Img0_vec_sel))*F_pix_vec(ysel,ap(:))*diag(GPF0(ap(:)).'));
    else
        d_Img1 = zeros(ni,ni,m);
        d_Pup1 = 1i*phase_basis.*repmat(GPF0,[1 1 m]);
        val = d_Pup1.*repmat(fftPhasor/ni,[1 1 m]);
        d_Img1 = fft2(val,ni,ni);
        Lv_sel = real(d_Img1_vec_sel.*(2*conj(Img0_vec_sel)));
    end    

%%% Save
    C = Cv_sel;
    L = Lv_sel;

% d_Img1_vec_full = 1i*F_pix_vec.*GPF0(:)';
% Img0_vec_full = Img0_vec;
% L_full = real(d_Img1_vec_full.*(2*conj(Img0_vec_full)));
% [p,n] = size(L_full);
% pk = sqrt(p);
% nk = sqrt(n);
% [~,test1,test2,accur] = compute_Kronecker_rank(L_full,pk,nk,pk,nk,10,1);
% [~,test1,test2,accur] = compute_Kronecker_rank(F_pix_vec,pk,nk,pk,nk,10,1);
% [~,test1,test2,accur] = compute_Kronecker_rank(Img0_vec_full,pk,1,pk,1,10,1);

% sel_sq = logical(ap_sq(:));
% d_Img1_vec_square = 1i*F_pix_vec(:,sel_sq).*GPF0(sel_sq)';
% L_square = real(d_Img1_vec_full(:,sel_sq).*(2*conj(Img0_vec_full)));
% L_square = L_full(:,xsel_sq);
% [~,test1,test2,accur] = compute_Kronecker_rank(L_square'*L_square,n_sq,n_sq,n_sq,n_sq,10,1);
% [~,test1,test2,accur] = compute_Kronecker_rank(L_square,pk,n_sq,pk,n_sq,10,1);


% 
% test = real(2*1i*diag(conj(Img0_vec_sel))*F_pix_vec(ysel,ap(:))*diag(GPF0(ap(:)).'));
% norm(L - test)


% 
% FF = F_pix_vec;
% d2 = GPF0(:);
% D2 = diag(d2);
% d1 = conj(Img0_vec);
% D1 = diag(d1);
% 
% norm(L - D1*FF*D2)

end
