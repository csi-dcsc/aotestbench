function [G,const] = IdentifyG(Id_data,G0)
% Identifies the geometry matrix. It is assumed that it is of the same
% sparsity pattern as G0, only with a different constant. 

    % Formulate Fried data in vectorized form
    [n,~,N] = size(Id_data.phase_pix_mat_fried);
    phi = reshape(Id_data.phase_pix_mat_fried,[n^2,N]);
    validActuator = ( Id_data.phase_pix_mat_fried(:,:,1) ~= 0);
    Id_data.phase_pix_fried = phi(validActuator(:),:);

    % Get data
    phi = Id_data.phase_pix_fried;
    s = Id_data.slopes;

    % Use structure of G that is known
    Gphi = G0*phi;

    % Identify constant
    vec_s = vec(s);
    vec_Gp = vec(Gphi);
    const = vec_Gp\vec_s;
    G = const*G0;

end