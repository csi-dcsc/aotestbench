function [W] = ComputeW(E,strucvecs)

p = size(E,1);
N = size(E,2);

W = zeros(p,p); 
W(1,1)= 1/norm(E(1,:)); 
for i = 2:p
    %imin =max(1,i-b); % b is size band
    %irange = imin:i-1;
    irange = strucvecs{i};     
    irange = irange(irange < i);        % Sparsity, lower-triangular
    w = -pinv(E(irange,:)')*E(i,:)';    % orthogonalisation
    wp = [w; 1];                        % Diagonal is equal to 1 (normalization)
    nrm = norm(E([irange i],:)'*wp);    % ?
    W(i,[irange i]) = wp/nrm;           % ?
end
W = sqrtm(N-1)*W;


end