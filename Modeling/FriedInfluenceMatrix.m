function [dm_prop] = FriedInfluenceMatrix(dm_prop,tel_prop,wfs_prop)
    %%% This function checks the DM influence matrix for Fried geometry
    %%% based on (noisy) SH readings. This one is a lower resolution than 
    %%% the pixel-based influence matrix. 
    %%%
    
    %%% Pieter Piscaer, 2021
    %% Generate a new lightpath identical to the one in the simulation
    tel_tmp = telescope(tel_prop.D,'resolution',tel_prop.n,...
    'fieldOfViewInArcsec',tel_prop.fov,'samplingTime',1/tel_prop.fs);
    dm_tmp = deformableMirror(dm_prop.dmres,...
        'modes',influenceFunction('monotonic',dm_prop.cdm),...
        'resolution',dm_prop.n,'validActuator',dm_prop.validActuator);

    %% WFS with noise
    wfs_tmp = shackHartmann(wfs_prop.nL,wfs_prop.n,wfs_prop.validLenslet);
    ngs_tmp = source('magnitude',wfs_prop.mag);
    ngs_tmp = ngs_tmp.*tel_tmp*wfs_tmp;
    wfs_tmp.INIT;    
    wfs_tmp.camera.photonNoise = wfs_prop.photon_noise;    
    wfs_tmp.camera.readOutNoise = wfs_prop.read_noise;
    
    %% DM influence function sparsity pattern 
    actuatorSpacing = tel_tmp.D/(dm_tmp.nActuator - 1);
    DistMat = DistanceMatrix(dm_tmp.nActuator,...
        actuatorSpacing,...
        double(dm_tmp.validActuator));
    DM_graph = DistMat < 2*actuatorSpacing;
    
    %% G matrix and augmented G
    G = full(sparseGradientMatrix(wfs_tmp));
    Ga = [G; null(G)'];
    iGa = pinv(Ga,1e-1);

    %% Start random poking test
    Ht = zeros(dm_tmp.nValidActuator); % influence function mirror actuator to dm phase
    % DM command poking
    W = 0.3*ngs_tmp.wavelength*repmat(eye(dm_tmp.nValidActuator),[1 4]);
    data = zeros(size(W));
    % Setup lightpath with noisy WFS
    ngs_tmp_noisy = source('magnitude',2).*tel_tmp*dm_tmp*wfs_tmp;
    disp('Start random poking test for DM influence matrix identification')     
    h = waitbar(0,'Poking test for DM influence matrix');
    for i = 1:size(W,2)
        waitbar(i/size(W,2),h);
        dm_tmp.coefs = W(:,i); % poke
        +ngs_tmp_noisy;
        +wfs_tmp;
        data(:,i) = iGa*[wfs_tmp.slopes;0;0];
        % For testing: show a preview as a test
%         if i == round(size(eye(dm_tmp.nValidActuator),1)/2)
%            htmp = figure;
%            imagesc(wfs_tmp.camera.frame);
%            colorbar;
%            title('Example frame actuator poking');
%         end
    end
    close(h); 
    
    %% Sparse least squares to compute the DM influence matrix
    Hf = data/W;
    Hfs = zeros(dm_tmp.nValidActuator);
    for i = 1:dm_tmp.nValidActuator
        sel = DM_graph(:,i);
        Hfs(i,sel) = data(i,:)/W(sel,:); 
    end
    dm_prop.Hf = Hf;
    dm_prop.Hfs = Hfs;
    
end