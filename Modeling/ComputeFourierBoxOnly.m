function out = ComputeFourierBoxOnly(ybox,aperture)  
    n = size(aperture,1);
    ni = 2*n;

    % Get Phase Diversities
    divcoefs = 2*[0 1 -1];
    PD = cell(length(divcoefs));
    for i = 1:length(divcoefs)
        PD{i} = divcoefs(i)*create_defocus(ones(size(aperture))); 
    end

    %%% Fourier transform (Image plane basis)

    F_pix_j = zeros(ybox,ybox,n^2);
    fftphasor = CreatePhasor(n,ni);             % FFT Phasor
    for j = 1:length(divcoefs)
        for i = 1:n^2               % Propagate all Basis functions into the image plane
            pix_i = zeros(n^2,1);
            pix_i(i) = 1;
            pix_i = reshape(pix_i,[n n]);
            [F_pix_j(:,:,i)] = altbox_propagateThrough(pix_i.*exp(1i*PD{j}),fftphasor,n,ni,ybox);
        end
        F_pix{j} = F_pix_j;
    end

    % Save
    for i = 1:length(divcoefs)
    %     F_pix{i} = F_pix{i};
        F_pix_vec{i} = reshape(F_pix{i},[ybox^2,n^2]);
    end

    %%% BOX
    inpupil = find(aperture);
    q = length(inpupil);
    vec_tmp = zeros(ybox^2,q);
    for j = 1:length(F_pix)
        for ii = 1:q
            tmp = F_pix{j}(:,:,inpupil(ii));
            vec_tmp(:,ii) = tmp(:);
        end
        out.F_pix_box_vec_ap{j} = vec_tmp;
    end
end