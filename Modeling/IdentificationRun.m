function [out] = IdentificationRun(Nide,tel_prop,atm_prop,wfs_prop,cam_prop,pred_prop)
% Identification data collection in OOMAO, the output depends on simulation
% setup and settings specified in Main.m.
% 
% Currently, the following important data is stored:
% phase_pix_mat: true high-resolution zero-mean wavefront data
% slopes: (noisy) vectorized WFS slope data
% phase_wfr: reconstructed Fried-geometry wavefront data
% phase_pix_mat_fried: true Fried-geometry zero-mean wavefront data

% Note: currently, no focal-plane WFS reconstruction is implemented, this
% will be added in the next version of the code.
% 2020, Pieter Piscaer

%% Decide Which sensors do we have in which lightpath
% and which type of data will they need for the identification

%%% wavefront sensors:
wfsFlag = false; % by default
[wfss_found] = find(strcmp(pred_prop.SensorType,'wfs'));
count = 1;
for i = wfss_found
    wfsFlag = true;
    if strcmp(pred_prop.ModelType{i},'SI')
        % Found a wfs that wants to use an identified model!
        wfss_SI_found(count) = i;  
        wfss_SIDataType{count} = pred_prop.SIDataType{i};
        count = count + 1;
    end
end

% if wfsFlag
    %%% NOTE: At the moment both flags are fixed to true.
    %%% This is due to the identification of the matrix G
%     if any(strcmp(wfss_SIDataType,'Reconstructed'))
        wfsRecFlag = true;
%     else
%         wfsRecFlag = false;
%     end
    %%% Do we need real (Fried) phase data?
%     if any(strcmp(wfss_SIDataType,'Real'))
        wfsRealFlag = true;
%     else
%         wfsRealFlag = false;
%     end
% end

%%% focal-plane cameras
camFlag = false;
[cams_loc] = (strcmp(pred_prop.SensorType,'cam'));
[wfscams_loc] = (strcmp(pred_prop.SensorType,'wfs-cam'));
cams_found = find(logical(cams_loc + wfscams_loc));
count = 1;
for i = cams_found
    if strcmp(pred_prop.ModelType{i},'SI')
        % Found a focal plane camera that wants to use an identified model!
        camFlag = true;
        cams_SI_found(count) = i;  
        cams_SIDataType{count} = pred_prop.SIDataType{i};
        count = count + 1;
    end
end
if camFlag
    %%% Do we need WFS-based reconstruction?
    if any(strcmp(cams_SIDataType,'Reconstructed'))
        camRecFlag = true;
    else
        camRecFlag = false;
    end
    %%% Do we need real (Fried) phase data?
    if any(strcmp(cams_SIDataType,'Real'))
        camRealFlag = true;
    else
        camRealFlag = false;
    end
end

%% Setup experiment lightpaths
%%% Create new telescope - both high- and Fried resolution.
[tel_id,tel_id_fried] = my_telescope(tel_prop);   
tel_ap = tel_id.pupilLogical(:);

%%% Atmosphere / turbulence
atm_id = my_atmosphere(atm_prop);

%%% Setup WFS based lightpaths
if wfsFlag
    wfs_id = shackHartmann(wfs_prop.nL,wfs_prop.n,wfs_prop.validLenslet);
%     wfs_id.camera.photonNoise = false; %wfs_prop.photon_noise;        
%     wfs_id.camera.frameListener.Enabled = false;
%     wfs_id.camera.readOutNoise = 0; %wfs_prop.read_noise;
    if wfsRealFlag
        ngs_tmp_fried = source('magnitude',0).*tel_id_fried;
    end
    if wfsRecFlag
        ngs_tmp_wfs = source('magnitude',wfs_prop.mag).*tel_id*wfs_id;
    end
    wfs_id.INIT; 
    wfs_id.camera.photonNoise = wfs_prop.photon_noise; 
    wfs_id.camera.readOutNoise = wfs_prop.read_noise;
end

%%% Setup cam based lightpaths
if camFlag

%%% Setup cameras
    cam_id = cell(cam_prop.ndiv);
    for i = 1:cam_prop.ndiv     % 3 diversity images
        cam_id{i} = imager('fieldStopSize',cam_prop.fieldstopsize,'nyquistSampling',cam_prop.nyquistrate); 
        cam_id{i}.frameListener.Enabled = false;
        cam_id{i}.photonNoise = cam_prop.photon_noise;
        cam_id{i}.readOutNoise = cam_prop.read_noise;
        cam_id{i}.nPhotonBackground = cam_prop.nPhotonBackground;
%         cam_id{i}.cPD = cam_prop.d(i);
%         cam_id{i}.PD = cam_prop.d(i)*create_defocus(ones(size(tel_id.pupilLogical)));
    end
    for i = 1:cam_prop.ndiv     % 3 diversity images
        ngs_tmp_cam{i} = source('magnitude',0).*tel_id*...
            {tel_id.pupilLogical,cam_prop.PD{i}}*cam_id{i};
    end
end

% Initializations
if wfsFlag
    if wfsRecFlag
        xSlopes = zeros(size(wfs_id.validLenslet,1),size(wfs_id.validLenslet,2),Nide);
        ySlopes = zeros(size(wfs_id.validLenslet,1),size(wfs_id.validLenslet,2),Nide);
        slopes = zeros(size(wfs_id.slopes,1),Nide);
        phase = zeros(size(wfs_id.finiteDifferenceWavefront,1),Nide);
    end
    if wfsRealFlag
        Phase_zm_f = zeros(size(wfs_prop.iGa,1),Nide); 
    end
end

%%% couple atmosphere
tel_id = tel_id + atm_id;
if wfsFlag && wfsRealFlag
    tel_id_fried = tel_id_fried + atm_id;
end

%% Start data generation
h = waitbar(0,'Collecting identification data...');
for i = 1:Nide    
    waitbar(i/Nide,h);
    
    %% Propagate and update cameras
    %%% Pixel base   
    +tel_id;
    if camFlag && camRealFlag
        +ngs_tmp_cam{1}
    end
    if camFlag && camRecFlag
        error('Not yet implemented')
    end
    %%% Fried geometry base
    if wfsFlag && wfsRealFlag
%         +tel_id_fried;
        +ngs_tmp_fried;
    end
    if wfsFlag && wfsRecFlag
        +ngs_tmp_wfs;
        +wfs_id;
    end
    
    %% WFSless-based Phase / High resolution
    if camFlag && camRealFlag
        % Phase_zm_pix_mat(:,:,i) = ngs_tmp.meanRmPhase;
        Phase_zm_pix_mat(:,:,i) = ngs_tmp_cam{1}.phase;
    end
    if camFlag && camRecFlag
        % Nothing implemented yet
    end
       
    %% WFS-based Phase / Fried geometry resolution
    if wfsFlag && wfsRecFlag
        slopes(:,i) = wfs_id.slopes;
        xSlopes(:,:,i) = wfs_id.xSlopesMap;
        ySlopes(:,:,i) = wfs_id.ySlopesMap;
        Phase_zm_f(:,i) = wfs_prop.iGa*[slopes(:,i);0;0];
%         phase(:,i) = wfs_id.finiteDifferenceWavefront;  % should be similar to Phase_zm_f ??
    end
    if wfsFlag && wfsRealFlag
    %%% Fried telescope wavefronts (infeasible in practice)
        % tmpphase_fried = ngs_tmp_fried.meanRmPhase;
        tmpphase_fried = ngs_tmp_fried.phase;
        Phase_zm_pix_fried(:,i) = tmpphase_fried(tel_id_fried.pupilLogical ~= 0); 
        Phase_zm_pix_mat_fried(:,:,i) = tmpphase_fried; 
    end
end
close(h);

%% Output
if camFlag && camRealFlag
    out.phase_pix_mat = Phase_zm_pix_mat;
elseif camFlag && camRecFlag
    % Nothing implemented yet
end

if wfsFlag && wfsRecFlag
    out.xslopes = xSlopes;
    out.yslopes = ySlopes;
    out.slopes = slopes;
    out.phase_wfr = Phase_zm_f;
%     out.phase_fd = phase;
end    
if wfsFlag && wfsRealFlag
    out.phase_pix_fried = Phase_zm_pix_fried;
    out.phase_pix_mat_fried = Phase_zm_pix_mat_fried;
end



end

