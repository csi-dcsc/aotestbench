function [C,J] = Lin_PSF_fun(obj,GPF0)    
% Linearize the output equation for WFSless AO
% Does not explicitly form the Jacobian, but rather stores the usefull data
% in an efficient format. This method is faster than Lin_PSF for larger
% matrices
% Pieter Piscaer, TU Delft, 2020

% Get info from object
nyquistrate = obj.NyquistSampling;

%%% Get dimensions
n = size(GPF0,1);
ni = 2*nyquistrate*n;
fftPhasor = obj.CreatePhasor(n,ni);

%%% Constant term
[~,Img0] = alt_propagateThrough(GPF0,fftPhasor,n,ni);
Img0_vec = reshape(Img0,[ni^2,1]);
C = abs(Img0_vec).^2;

%%% Linear term
% J.FF = obj.basis.F_pix_vec{1};
J.d1 = conj(Img0_vec);
J.d2 = GPF0(:);
J.fftphasor = fftPhasor;
J.xsel = obj.pupilLogical(:);


% Jv_sel = real(1i*F_pix_vec(ysel,ap(:)).*GPF0(ap(:)).'.*(2*conj(Img0_vec_sel)));
% Jv_sel2 = real(2*1i*diag(conj(Img0_vec_sel))*F_pix_vec(ysel,ap(:))*diag(GPF0(ap(:)).'));

end
