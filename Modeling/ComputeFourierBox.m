function [out] = ComputeFourierBox(nb,F_pix,pupilLogical)  
% Pre-computes a smaller inner block of the 2D-DFT matrix. Block size is
% specified by nb. F_pix contains the full 2D-DFT matrices to be converted.
    n = size(pupilLogical,1);
    ni = size(F_pix{1},1);
    
    n_pad_left = floor(0.5*(ni-nb));
    n_pad_right = ni - nb - n_pad_left;
    range_box = n_pad_left + 1 : n_pad_left + nb; 
%         ybox_mask = zeros(ni);
%         ybox_mask(range_box,range_box) = 1;
%         iy = 1:ni^2;
%         ysel_box = iy(logical(ybox_mask(:)));

    inpupil = find(pupilLogical);
    q = length(inpupil);
    vec_tmp = zeros(nb^2,q);
    for j = 1:length(F_pix)
        for ii = 1:q
            tmp = F_pix{j}(range_box,range_box,inpupil(ii));
            vec_tmp(:,ii) = tmp(:);
        end
        out.F_pix_box_vec_ap{j} = vec_tmp;
%         F_pix_box{j} = F_pix{j}(range_box,range_box,:);
    end
    % Save
%     for i = 1:length(F_pix)
%         F_pix_box{i} = F_pix_box{i};
%         F_pix_box_vec{i} = reshape(F_pix_box{i},[nb^2,n^2]);
%         tmp = reshape(F_pix_box{i},[nb^2,n^2]);
%         tmp(:,~pupilLogical(:)) = [];
%         out.F_pix_box_vec_ap{i} = tmp;
%     end
out.n = n;
out.ni = ni;
out.ybox = nb;
end