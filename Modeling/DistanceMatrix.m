function DistMat = DistanceMatrix(n,pixelsize,mask)
% Creates a matrix containing all distances between different pixels on a
% 2D grid. This will be used to determine sparsity patterns in the system
% identification step.

% Get selection vector, make sure zeros will be Inf distances
    sel = mask(:);
    infA = (mask./mask);
    infA(isnan(infA)) = inf;
% Compute grid of centers     
    x_grid = meshgrid([1:n]*pixelsize).*infA;   % outside mask corresponds to position in infinity
    posvecs = [vec(x_grid) vec(x_grid')]; 
% Compute distance matrix    
    DiM = zeros(n^2,n^2);
    for i = 1:n^2
        DiM(i,:) = vec(sqrt( (x_grid - posvecs(i,1)).^2 + (x_grid'-posvecs(i,2)).^2))';
    end
    DistMat = DiM(logical(sel),logical(sel));  % Remove positions outside the mask
end