function [Sparsity] = GetSparsity(DistanceMatrix,radius)
% Gives a structure Sparsity 
% Needs a (square) distance matrix and a radius 

%%% Load
n = size(DistanceMatrix,1);

%%% Get sparsit pattern
M = zeros(n);
for i = 1:n
    E{i} = find(DistanceMatrix(i,:) <= radius);
    M(i,E{i}) = true;
end

%%% Output
Sparsity.E = E;
Sparsity.Mask = sparse(M);

end