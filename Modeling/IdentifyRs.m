function [wfs_prop] = IdentifyRs(dm_prop,tel_prop,wfs_prop)
    %%% This function performs a simple experiment to estimate the WFS 
    %%% measurement noise variance, please keep in mind that this is a 
    %%% rough estimate only. 
    %%% 
    %%% It only measures the effect of photon and shot noise, all other
    %%% noise sources are not included. Therefore, the estimate might be an
    %%% underestimation in certain cases. 
    %%%
    
    %%% Pieter Piscaer, 2021.
    
    %% Generate a new lightpath identical to the one in the simulation
    tel_tmp = telescope(tel_prop.D,'resolution',tel_prop.n,...
    'fieldOfViewInArcsec',tel_prop.fov,'samplingTime',1/tel_prop.fs);
    dm_tmp = deformableMirror(dm_prop.dmres,...
        'modes',influenceFunction('monotonic',dm_prop.cdm),...
        'resolution',dm_prop.n,'validActuator',dm_prop.validActuator);%,...
%         'telD',tel_prop.D);          
%     GetInfluenceMatrix(dm_tmp,tel_tmp);  

    %% WFS with noise
    wfs_tmp = shackHartmann(wfs_prop.nL,wfs_prop.n,wfs_prop.validLenslet);
    ngs_tmp = source('magnitude',wfs_prop.mag);
    ngs_tmp = ngs_tmp.*tel_tmp*wfs_tmp;
    wfs_tmp.INIT;    
    wfs_tmp.camera.photonNoise = wfs_prop.photon_noise;    
    wfs_tmp.camera.readOutNoise = wfs_prop.read_noise;
    
    %% WFS with no noise
    wfs_tmp_n = shackHartmann(wfs_prop.nL,wfs_prop.n,wfs_prop.validLenslet);
    ngs_tmp_n = source('magnitude',wfs_prop.mag);
    ngs_tmp_n = ngs_tmp_n.*tel_tmp*wfs_tmp_n;
    wfs_tmp_n.INIT;    
    wfs_tmp_n.camera.photonNoise = 'false';    
    wfs_tmp_n.camera.readOutNoise = 0;
    
    %% G matrix and augmented G
    G = full(sparseGradientMatrix(wfs_tmp));
    iG = pinv(G,1e-1);
    Ga = [G; null(G)'];
    iGa = pinv(Ga,1e-1);
    wfs_prop.G = G;
    wfs_prop.iG = iG;
    wfs_prop.Ga = Ga;
    wfs_prop.iGa = iGa;

    %% identify of the measurement noise variance
    % Initialize
    Ht = zeros(dm_tmp.nValidActuator); % influence function mirror actuator to dm phase
    % Generate random set of DM input commands
    W = ngs_tmp.wavelength/6*randn(dm_tmp.nValidActuator,1000);
    % Initialize lightpaths, noisy and noiseless
    slopes = zeros(size(G,1),size(W,2));
    ngs_tmp_withnoise = source('magnitude',wfs_prop.mag).*tel_tmp*dm_tmp*wfs_tmp;
    ngs_tmp_nonoise = source('magnitude',wfs_prop.mag).*tel_tmp*dm_tmp*wfs_tmp_n;
    % Start random poking test
    disp('Start random poking test for DM influence matrix identification')     
    h = waitbar(0,'Identifying measurement noise variance');
    for i = 1:size(W,2)
       waitbar(i/size(W,2),h);
       dm_tmp.coefs = W(:,i); % no poking
       +ngs_tmp_withnoise;
       +ngs_tmp_nonoise;
       +wfs_tmp;
       +wfs_tmp_n;
       % store noise and noiseless slope data
       slopes(:,i) = wfs_tmp.slopes;
       slopes_nonoise(:,i) = wfs_tmp_n.slopes;           
       % preview of a single frame 
%        if i == 10
%            htmp = figure;
%            imagesc(wfs_tmp.camera.frame);
%            colorbar;
%            title('Example frame R identification');
%        end
    end
    close(h)
%     close(htmp)
    

    % Sort data
    vec_slopes = slopes(:);
    vec_slopes_nn = slopes_nonoise(:);
    [sort_slopes,idx] = sort(vec_slopes,'descend');
    sort_slopes_nn = vec_slopes_nn(idx);
    sort_noise = sort_slopes - sort_slopes_nn;
    
    % Compute noise
    wfs_prop.SNRs = snr(vec_slopes_nn, vec_slopes - vec_slopes_nn);
    wfs_prop.r = var(sort_slopes - sort_slopes_nn);
    wfs_prop.R = wfs_prop.r*eye(size(G,1));
    wfs_prop.validLenslets = wfs_tmp.validLenslet;  % WFS lenslets within aperture
end