function [out] = ModelIdentification(pred_prop,Id_data,basis,tel_prop,dm,tel,wfs_prop,dm_prop)
% This function identifies different dynamical models from OOMAO wavefront
% data. It exploits sparisty in the AR-1 model structure that is used to
% represent the turbulence dynamics. 
%
% Note: not all combinations of basis functions, source of data and type 
% of model computation method are implemented. This function will give an 
% error when an unimplemented combination is chosen in Main.m.

% 2020, Pieter Piscaer

%% General info about the model and its sparsity
    pixelSpacing = tel_prop.D/(tel_prop.n - 1);
    r_graph = pred_prop.r_graph_pix*pixelSpacing;
    actuatorSpacing = tel_prop.D/(dm{1}.nActuator - 1);
    r_graph_fried = pred_prop.r_graph_fried_pix*actuatorSpacing;
    ARorder = pred_prop.ARorder;
    out.r_graph = r_graph;
    lambda_LS= pred_prop.lambda_LS;
    cndQ = pred_prop.cndQ;
    out.cG = wfs_prop.cG; 
    out.basis = basis;
    
%% Check which Basis functions are used and process the corresponding data
    PixelBaseFlag = false;
    DMBaseFlag = false;
    FriedBaseFlag = false;
    GRBFBaseFlag = false;
    %%% Pixel:
    if any(strcmp(basis.BasisTypes,'Pixel'))
        PixelBaseFlag = true;
        pixsize = pixelSpacing;
%         r_graph_tel = 1.01*ceil(r_graph./pixsize)*pixsize
        r_graph_tel = 1.01*r_graph;
        DiM_tel = tel_prop.DistanceMatrix;        
        Id_data.phase_pix = reshape(Id_data.phase_pix_mat,size(Id_data.phase_pix_mat,1)^2,size(Id_data.phase_pix_mat,3));
    end
    %%% DM      
    if any(strcmp(basis.BasisTypes,'DM'))
        DMBaseFlag = true;
        tmp2 = max(1.5,ceil(r_graph./actuatorSpacing));       % minimum of 9 actuators
        r_graph_dm = 1.01*tmp2*actuatorSpacing;
        DiM_dm = dm_prop.DistanceMatrix;
        Id_data.phase_basis = pinv(basis.basis_phase_vec(tel_prop.pupilLogical_fried(:),:),1e-4)*Id_data.phase_pix_fried;
    end
    %%% Fried      
    if any(strcmp(basis.BasisTypes,'Fried'))
        FriedBaseFlag = true;
        r_graph_fried = 1.01*r_graph_fried;
        DiM_dm = dm_prop.DistanceMatrix;
    end
    %%% GRBF
    if any(strcmp(basis.BasisTypes,'GRBF'))
        GRBFBaseFlag = true;
        GRBFspace = basis.GRBFspace;
        tmp2 = max(1.5,ceil(r_graph./GRBFspace));               % minimum of 9 GRBFs
        r_graph_grbf = 1.01*tmp2*GRBFspace;
        DiM_grbf = DistanceMatrix(basis.n_grbf,GRBFspace,reshape(double(basis.active_GRBFs),[basis.n_grbf,basis.n_grbf]));
        Id_data.phase_GRBF = pinv(basis.GRBFs_vec(tel.pupilLogical(:),basis.active_GRBFs),1e-4)*Id_data.phase_pix(tel.pupilLogical(:),:);
    end
    
%% Decide the System Identification cases
    %%% wavefront sensors:
    wfsFlag_SI = false; % by default
    [wfss_found] = find(strcmp(pred_prop.SensorType,'wfs'));
    count = 1;
    for i = wfss_found
        if strcmp(pred_prop.ModelType{i},'SI')
            % Found a wfs that wants to use an identified model!
            wfsFlag_SI = true;
            wfss_SI_found(count) = i;  
            wfss_SIDataType{count} = pred_prop.SIDataType{i};
            count = count + 1;
        end
    end
    %%% focal plane cameras
    camFlag_SI = false;
    [cams_found] = find(strcmp(pred_prop.SensorType,'cam'));
    count = 1;
    for i = cams_found
        if strcmp(pred_prop.ModelType{i},'SI')
            % Found a focal plane camera that wants to use an identified model!
            camFlag_SI = true;
            cams_SI_found(count) = i;  
            cams_SIDataType{count} = pred_prop.SIDataType{i};
            count = count + 1;
        end
    end
    %%% WFS + focal plane cameras
%     camFlag_SI = false;
    [wfscams_found] = find(strcmp(pred_prop.SensorType,'wfs-cam'));
    count = 1;
    for i = wfscams_found
        if strcmp(pred_prop.ModelType{i},'SI')
            % Found a focal plane camera that wants to use an identified model!
            camFlag_SI = true;
            cams_SI_found(count) = i;  
            cams_SIDataType{count} = pred_prop.SIDataType{i};
            count = count + 1;
        end
    end

%% Decide the first principles cases    
    %%% wavefront sensors:
    wfsFlag_FP = false; % by default
    count = 1;
    for i = wfss_found
        if strcmp(pred_prop.ModelType{i},'FP')
            % Found a wfs that wants to use an identified model!
            wfsFlag_FP = true;
            wfss_FP_found(count) = i;  
            count = count + 1;
        end
    end
    %%% focal plane cameras
    camFlag_FP = false;
    count = 1;
    for i = cams_found
        if strcmp(pred_prop.ModelType{i},'FP')
            % Found a focal plane camera that wants to use an identified model!
            camFlag_FP = true;
            cams_FP_found(count) = i;  
            count = count + 1;
        end
    end

%% Identified WFS based models
%%% Fried
if wfsFlag_SI && FriedBaseFlag
    if any(strcmp(wfss_SIDataType,'Real'))
    %%% AR Identification phi based on real phase info
        out.ModelPhi_tel = Sparse_ARid(Id_data.phase_pix_fried,DiM_dm,r_graph_fried,ARorder,lambda_LS,cndQ);  
        out.ModelPhi_tel.aperture = pred_prop.validActuators;
        out.ModelPhi_tel.aperture_wfs = pred_prop.validLenslets;
        out.ModelPhi_tel.H = dm_prop.H_fried(pred_prop.validActuators(:),:);
        out.ModelPhi_tel.iH = pinv(dm_prop.H_fried(pred_prop.validActuators(:),:));
        out.ModelPhi_tel.G = wfs_prop.Gphi;
        out.ModelPhi_tel.R = wfs_prop.R;
    end
    if any(strcmp(wfss_SIDataType,'Reconstructed'))
    %%% AR Identification phi based on measured slopes (via WFR)
        out.ModelPhi = Sparse_ARid(Id_data.phase_wfr,DiM_dm,r_graph_fried,ARorder,lambda_LS,cndQ);  
        out.ModelPhi.aperture = pred_prop.validActuators;
        out.ModelPhi.aperture_wfs = pred_prop.validLenslets;
        out.ModelPhi.H = dm_prop.Hfs;
        out.ModelPhi.G = wfs_prop.G;
        out.ModelPhi.R = wfs_prop.R; 
    end
end 

%%% DM basis coefficients
if wfsFlag_SI && DMBaseFlag
    if any(strcmp(wfss_SIDataType,'Real'))
    %%% Wavefront models (AR 1) in terms of basis coefficients   
        out.ModelPhi_basis = Sparse_ARid(Id_data.phase_basis,DiM_dm,r_graph_dm,ARorder,cndQ);
    end
    if any(strcmp(wfss_SIDataType,'Reconstructed'))
        error('This method is not yet implemented, use DataType "Real".')
    end
end

%% First principles WFS based models
%%% Fried Geometry
if wfsFlag_FP && FriedBaseFlag 
    %%% Diagonal A matrix and C_phi identified from data
        out.ModelPhi_diag.A = 0.98*eye(dm{1}.nValidActuator);
        tmp = Sparse_ARid(Id_data.phase_pix_fried,DiM_dm,r_graph_fried,ARorder,lambda_LS,cndQ);  
        out.ModelPhi_diag.Cm = tmp.Cm;
        out.ModelPhi_diag.Q = out.ModelPhi_diag.Cm - ...
        out.ModelPhi_diag.A*out.ModelPhi_diag.Cm*out.ModelPhi_diag.A';
        out.ModelPhi_diag.H = dm_prop.H_fried(pred_prop.validActuators(:),:);   
        out.ModelPhi_diag.iH = pinv(out.ModelPhi_diag.H,1e-1);       
        out.ModelPhi_diag.G = wfs_prop.Gphi;
        out.ModelPhi_diag.R = wfs_prop.R;
end

%% Identified WFSless based models
%%% Pixel base
if camFlag_SI && PixelBaseFlag
    if any(strcmp(cams_SIDataType,'Real'))
        %%% Pixel basis
            Id_data.phase_pix = reshape(Id_data.phase_pix_mat,[tel_prop.n^2,size(Id_data.phase_pix_mat,3)]);
            Id_data.phase_pix = Id_data.phase_pix(tel.pupilLogical(:),:); 
            out.ModelPhi_pix = Sparse_ARid(Id_data.phase_pix,DiM_tel,r_graph_tel,ARorder,lambda_LS,cndQ);  
%             out.ModelPhi_pix = Sparse_ARid_old(Id_data.phase_pix,DiM_tel,r_graph_tel,ARorder,cndQ);  
            out.ModelPhi_pix.Asp = sparse(out.ModelPhi_pix.A);
            
         %%% Get some sparsity patterns
            out.ModelPhi_pix.DiM_tel = DiM_tel;
            out.ModelPhi_pix.SparsityGraph = GetSparsity(DiM_tel,r_graph_tel);          % radius = r_graph_tel
            out.ModelPhi_pix.SparsityApprox = GetSparsity(DiM_tel,2.5*r_graph_tel);     % radius = 2.5*r_graph_tel
            out.ModelPhi_pix.Sparsity9 = GetSparsity(DiM_tel,1.01*sqrt(2)*pixsize);     % radius = 1.414*pixel_size (max 9 non-zero per row)

        %%% Get sparsity pattern of A for an approximate Q and P
            n = size(out.ModelPhi_pix.A,1);
            [idx] = find(DiM_tel <= r_graph_tel);
            inv_cov_sparsity = zeros(n);
            inv_cov_sparsity(idx) = true;
            out.ModelPhi_pix.GraphSparsityPattern = inv_cov_sparsity;

        %%% Precompute iQ*A for an information filter application
            out.ModelPhi_pix.iQA = out.ModelPhi_pix.iQ_spgs*out.ModelPhi_pix.Asp;
            out.ModelPhi_pix.iQAapprox = out.ModelPhi_pix.iQA;
            out.ModelPhi_pix.iQAapprox(~out.ModelPhi_pix.GraphSparsityPattern) = 0;
            out.ModelPhi_pix.ATiQA = out.ModelPhi_pix.Asp(:,1:n)*out.ModelPhi_pix.iQ_spgs*out.ModelPhi_pix.Asp(:,1:n);
            out.ModelPhi_pix.ATiQAapprox = out.ModelPhi_pix.ATiQA;
            out.ModelPhi_pix.ATiQAapprox(~out.ModelPhi_pix.GraphSparsityPattern) = 0;

    end
    
    %%% Gaussian radial basis functions
    if camFlag_SI && GRBFBaseFlag
        out.ModelPhi_GRBF = Sparse_ARid(Id_data.phase_GRBF,DiM_grbf,r_graph_grbf,ARorder,lambda_LS,cndQ); 
    end
    
    if any(strcmp(cams_SIDataType,'Reconstructed'))
        error('This method is not yet implemented')   
    end
end

%% First principles WFSless 
%%% Pixel base
if camFlag_FP && PixelBaseFlag
        error('This method is not yet implemented')
end

%% GPF Kronecker/Tensor model
out.ModelGPF = basis.Kronecker;

end