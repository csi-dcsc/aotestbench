function [H,Hr] = GetInfluenceMatrix(obj,tel)
    % Influence matrix
    Ht = zeros(tel.resolution^2,obj.nValidActuator); % influence function mirror actuator to dm phase
    W = 1e-7*eye(obj.nValidActuator);
    infl_sys = source('magnitude',2).*tel*obj;
    for i = 1:obj.nValidActuator
       obj.coefs = W(:,i); % poke
       +infl_sys;
%                basis_DM(:,:,i) = infl_sys.meanRmPhase.*tel.pupilLogical;
       basis_DM(:,:,i) = infl_sys.phase.*tel.pupilLogical;
       basis_DM(:,:,i) = (basis_DM(:,:,i) - max(basis_DM(:,:,i))).*tel.pupilLogical;
       Ht(:,i) = vec(basis_DM(:,:,i)); % read phase
    %    H(:,i) = H(:,i)/norm(H(:,i));
    end
    sel = tel.pupilLogical(:);
    H = Ht/W;
    Hr = H(sel,:);
%     dm_prop.DMbasis = basis_DM;
%     dm_prop.coefs = 0; % reset mirror
end
        