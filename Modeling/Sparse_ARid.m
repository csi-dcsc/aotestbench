function out = Sparse_ARid(Data,DiM,r_graph,ARorder,lambda_LS,cndQ)            
% Identification of a sparse AR model where the sparsity is depending on
% the distance between to sample locations, i.e. two locations that are at
% a distance of r_graph or higher are considered to be conditionally
% independent and therefore their corresponding entries in the AR matrices
% and inverse process noise covariance are zero. 
% 
% Idea based on "Sparse VARX model identification for large-scale AO" 
% Pieter Piscaer, TU Delft, 2016
%
% Last modified: December 2020, Pieter Piscaer
%
% Inputs:
% Data: A matrix of size (n x N) containing data vectors. Each column is a
% time step.
% DiM: Distance matrix, this is used to define the sparsity pattern of the
% matrix.
% r_graph: radius in which points are considered to be correlated.
% ARorder: order of the AR model
% lambda_LS: regularization parameter
% cndQ: regularization parameter
    
%%% Parameters
    Method = 'LS';          % LS or ADMM
%     lambda_LS = 1e-4;       % TUNING PARAMETER, important to properly tune for KF-based prediction algorithms
    % ADMM related
    lambda_ADMM = 1;        % TUNING PARAMETER
    rho_ADMM = 1;           % TUNING PARAMETER
    ADMMstop = 1e-5;
    
%%% Check if dimensions too large
    if size(Data,1) > 3000
        toolargeflag = true;
    else
        toolargeflag = false;
    end
    
%%% Short hand notations, initializations and pre-processing
    out.r_graph = r_graph;
    out.ARorder = ARorder;
    out.tag = 'AR';              
    n2 = size(Data,1);
    N = size(Data,2);
    Ni = round(0.9*N);
    p = ARorder;
    tmp = DiM;
    tmp(tmp == 0) = Inf;
    pixel_size = min(min(abs(tmp)));
   
%%% Covariance matrix
    if ~toolargeflag
        out.Cm = cov( Data' );      
    end
    
%% Create data matrices
%%% Create future and past data matrices
    xsel_f = p+1 : Ni;
    for i = 1:p
        xsel_p{i} = p+1-i : Ni-i;
    end
    Xf = Data( : , xsel_f);     % Future data
    Xp = [];
    for i = 1:p
        Xp = [Xp; Data( : , xsel_p{i})];    % Shifted data 1,..,p steps in past
    end

%%% Create future and past data matrices for validation data
    xsel_fid = Ni+p+1 : N;
    for i = 1:p
        xsel_pid{i} = Ni+p+1-i : N-i;
    end
    Xfid = Data( : , xsel_fid);     % Future data
    Xpid = [];
    for i = 1:p
        Xpid = [Xpid; Data( : , xsel_pid{i})];    % Shifted data 1,..,p steps in past
    end

%%% Pre-compute the transpose for the identification step
    Xp_ls = Xp';
    Xf_ls = Xf';
    
%% A identification from data
    A = zeros(n2,n2*p);
    A2 = zeros(n2,n2*p);
    for i = 1:n2
        band{i} = [];   
        for ii = 1:p
            band_i = find(DiM(i,:) <= ii*r_graph);
            band{i} = [band{i} band_i + n2*(ii-1)];
        end
        Xp_ls_r = Xp_ls(:,band{i}); 
        if strcmp(Method,'LS')
            % Solve using Least squares with 2 norm regularization
            left = Xp_ls_r'*Xp_ls_r + lambda_LS*norm(Xp_ls_r'*Xp_ls_r)*eye(size(Xp_ls_r,2));
            right = Xp_ls_r'*Xf_ls(:,i);
            A(i,band{i}) = left\right;
        elseif strcmp(Method,'ADMM')
            % Solve using ADMM with 1 norm regularization (enhances sparsity)
            left = Xp_ls_r'*Xp_ls_r + rho_ADMM*eye(size(Xp_ls_r,2));
            right = Xp_ls_r'*Xf_ls(:,i);
            % Initialize for the ADMM iterations
            x = zeros(size(Xp_ls_r,2),1); %  
            z = x;  %
            u = x;  % 
            zp = z; % past z
            for iter = 1:1e6
                x = left\(right + rho_ADMM*(z-u));
                z = max(0,1-(lambda_ADMM/rho_ADMM)./abs(x+u)).*(x+u);
                u = u + x - z;
                normr = norm(x-z);
                norms = norm(rho_ADMM*(z-zp));
                if normr < sqrt(size(Xp_ls_r,2))*ADMMstop + ADMMstop*max(norm(x),norm(z)) 
                    if norms < sqrt(size(Xp_ls_r,2))*ADMMstop + ADMMstop*norm(rho_ADMM*u)  
                        break
                    end
                end
                zp = z;
            end % END of ADMM iterations
             A(i,band{i}) = z';       % the rest of XX
        end % END of row i identification
    end     % END of A identification
    A2 = A;
%% Q identification from residual
%%% Remove mean and compute residual
    Xfzm = Xf - mean(Xf);        
    Xpzm = Xp - mean(Xp);        
    E = Xfzm - A2*Xpzm;
    E_Q = E;
    
%%% Compute sample covariance matrix    
    if ~toolargeflag
        Q0 = cov(E_Q');  % Full Q sample covariance matrix
    else 
        Q0 = [];
    end
    
%%% validation data
    if ~toolargeflag
        Xfidzm = Xfid - mean(Xfid);        
        Xpidzm = Xpid - mean(Xpid);        
        Eid = Xfidzm - A2*Xpidzm; 
        Eid = Eid - mean(Eid);
        Eid_0 = Xfidzm - Xpidzm(1:n2,:);    % static approxmation, for normalization purposes
        Eid_0 = Eid_0 - mean(Eid_0);
        Eid_Q = Eid;
        Q0id = cov(Eid_Q');  % Full Q sample covariance matrix
        Q0 = (Ni/N)*Q0 + ((N - Ni)/N)*Q0id;
    end
    
%%% Correction for ill-conditioned Q
    Q = Q0;
    if ~toolargeflag
        count = 1;
        while cond(Q) > cndQ
            E_Q = E + count*0.2*mean(mean(abs(E_Q)))*randn(size(E_Q));
            Q = cov(E_Q');
            count = count + 1
        end
    else
        E_Q = E + mean(mean(abs(E_Q)))*randn(size(E_Q));
    end


%%% If very large dimensions, fix graph Q to 8 close neighbours
%     if N/n2 < 50
        r_graph_Q = 1.02 * sqrt(2) * pixel_size;
        for i = 1:n2
            band_Q{i} = find(DiM(i,:) <= r_graph_Q);
        end
%     else
%         band_Q = band;
%     end
    
%%% Sparse approximate inverse solution
    if ~toolargeflag
        % Sparse regularized
        iQ_sp = SparseApproxInv(Q,band_Q);
        iQ_sp = (iQ_sp + iQ_sp')/2;      
        % Sparse unregularized
        iQ0_sp = SparseApproxInv(Q0,band_Q);
        iQ0_sp = (iQ0_sp + iQ0_sp')/2;
    else
        iQ_sp = [];
        iQ0_sp = [];
    end
    
%%% Sparse Gramm-Schmidt solution
    % Gramm-Schmidt -- ill-conditioning compensation
    W_r = ComputeW(E_Q,band_Q);
    iQ_spgs = full(W_r'*W_r);
    
    % Gramm-Schmidt -- no compensation
    W = ComputeW(E,band_Q);
    iQ0_spgs = full(W'*W);
    
    % Make sure the matrices are symmetric
    if ~toolargeflag
        iQ_spgs = (iQ_spgs + iQ_spgs')/2;
        iQ0_spgs = (iQ0_spgs + iQ0_spgs')/2;
    end
    
    % Scaling so that 1./(diag(Q)) approx diag(iQ)
    c1 = 1./mean(diag(Q0));
    c2 = mean(diag(iQ_spgs));
    iQ_spgs = c1/c2*iQ_spgs;
    
%%% If too large, compute Q from Qi via sparse approximate inverse 
    r_graph_Qi = 3*1.02 * sqrt(2) * pixel_size;
    for i = 1:n2
        band_Qi{i} = find(DiM(i,:) <= r_graph_Qi);
    end
    if toolargeflag
        Q = SparseApproxInv(iQ_spgs,band_Qi);
    end

%%% Get variances and diagonal approximation
    if ~toolargeflag
        var_E = diag(1/(size(E,2))*E*E');
        var_EQ = diag(1/(size(E_Q,2))*E_Q*E_Q');
    else
        var_E = diag(1/5000*E(1:3000,1:5000)*E(1:3000,1:5000)');
        var_EQ = diag(1/5000*E_Q(1:3000,1:5000)*E_Q(1:3000,1:5000)');
    end
    mean_var_E = mean(var_E);
    mean_var_EQ = mean(var_EQ);
    iQ_sp_diag = diag(1./var_E);
 
% %%% Rescale based on variances:
%     c_ivar = 1/mean_var_E;
%     if 1   
%         iQ_sp = (c_ivar/mean(diag(iQ_sp)))*iQ_sp;
%         iQ_spgs = (c_ivar/mean(diag(iQ_spgs)))*iQ_spgs;
%     end
    
%%% Output
    out.A = A2;    
    out.mean_var_E = mean_var_E;
    out.mean_var_EQ = mean_var_EQ;
    out.Q = Q; 
    out.Q0 = Q0;
    out.iQ_sp = sparse(iQ_sp);
    out.iQ_spgs = sparse(iQ_spgs);
    out.iQ0_sp = sparse(iQ0_sp);
    out.iQ0_spgs = sparse(iQ0_spgs);
    out.iQ_sp_diag = iQ_sp_diag;
    out.bandA = band;
    out.bandQ = band_Q;
    if ~toolargeflag
        out.Qsqrt = sqrtm(out.Q);   
    else 
        out.Qsqrt = [];   
    end
    
%%% Identification accuracy
    if ~toolargeflag
        out.VAF_id = 100*(1 - sum(E.^2,2)./sum(Xf.^2,2));
        out.MSE = sum(sum(Eid.^2,2));
        out.MSE0 = sum(sum(Eid_0.^2,2));
        out.meanVAFid = mean(out.VAF_id);
    end

end

