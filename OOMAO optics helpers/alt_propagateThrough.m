function [wavePrgted,wavePrgted_full] = alt_propagateThrough(GPFin,fftPhasor,npix,npix_out)
    % The source of this function is from the toolbox OOMAO.   
    % It performs a propagation from the pupil plane to the image plane. 
    %
    % TEMPORARY FUNCTION -- DOES NOT WORK FOR ODD NUMBER OF PIXELS 
    %
    evenOdd = rem(npix,2);
    val = GPFin;    
    if evenOdd % ODD # OF PIXELS 
        error('Does not work for odd number pixels yet!!!')
    else % EVEN # OF PIXELS
        v = [];
        centerIndex = ceil((npix_out+1)/2) + rem(npix,2);
        halfLength  = floor(npix/2);
        v           = true(npix_out,1);
        v((0:npix-1)+centerIndex-halfLength) = false;
        val = val.*fftPhasor/npix_out;
        wavePrgted = fft2(val,npix_out,npix_out);
        wavePrgted_full = wavePrgted;
        wavePrgted(v,:) = [];
        wavePrgted(:,v) = [];
    end
end