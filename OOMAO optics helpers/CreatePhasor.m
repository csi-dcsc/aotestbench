function fftPhasor = CreatePhasor(npix,npixpad)
%     evenOdd       = rem(npix,2);
%     if ~rem(npixpad,2) && evenOdd
%         npixpad = npixpad + evenOdd;
%     end
%     npixpad = max(npixpad,npix);

    if ~rem(npix,2)
        % shift the intensity of half a pixel for even sampling
        [u,v] = ndgrid((0:(npix-1)).*(~rem(npix,2)-npixpad)./npixpad);
        fftPhasor = exp(-1i.*pi.*(u+v));
    else
        fftPhasor = [];
    end            
end
            