function [wavePrgted] = alt_propagateThrough_box(GPFin,fftPhasor,npix,npix_out,box_range)
    % The source of this function is from the toolbox OOMAO.   
    % It performs a propagation from the pupil plane to the image plane. 
    %
    % TEMPORARY FUNCTION -- DOES NOT WORK FOR ODD NUMBER OF PIXELS 
    %
    evenOdd = rem(npix,2);
    if evenOdd % ODD # OF PIXELS 
        error('Does not work for odd number pixels yet!!!')
    else % EVEN # OF PIXELS
        wavePrgted_1 = fft(GPFin.*fftPhasor/npix_out,npix_out);
        wavePrgted_1 = wavePrgted_1(box_range,:).';
        wavePrgted_2 = fft(wavePrgted_1,npix_out);
        wavePrgted = wavePrgted_2(box_range,:).';
    end
end