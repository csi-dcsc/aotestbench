function PSF = SimulatePSF(obj,GPF,method,noise)
% Simulates a PSF including noise based on a given GPF

p1 = size(obj.PSFs,1);

if size(GPF,3) > 1
    warning('simultaneous processing is not yet fully checked!')
end

% Get complex PSF
if strcmp(method,'full')
    [~,complex_image] = alt_propagateThrough(GPF,obj.fftphasor,p1,p1);
elseif strcmp(method,'box')
    nb = obj.ybox;
    n_pad_left = floor(0.5*(p1-nb));
    range_box = n_pad_left + 1 : n_pad_left + nb;  
    if size(GPF,3) == 1
        [complex_image] = alt_propagateThrough_box(GPF,obj.fftphasor,p1,p1,range_box);
    else
        [complex_image] = alt_set_propagateThrough_box(GPF,obj.fftphasor,p1,p1,range_box);
    end
end
% Get PSF
image = abs(complex_image).^2;
image_noisy = image; 

% Add noise
if strcmp(noise,'noisy')
    if obj.photonNoise
            % this is too slow...
    %     image_noisy = poissrnd(image_noisy + obj.nPhotonBackground) - obj.nPhotonBackground;
            % Gaussian approximation
        image_noisy = image + sqrt(image + obj.nPhotonBackground).*randn(size(image));
    end
    if obj.readOutNoise>0
        image_noisy = image_noisy + randn(size(image)).*obj.readOutNoise;
    end
end
PSF = image_noisy;


    
end