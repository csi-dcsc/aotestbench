function [waveBPrgted] = alt_propagateBack(Iin,fftPhasor,npix,npix_out)
    evenOdd       = rem(npix,2);
    val = Iin;    
        if evenOdd % ODD # OF PIXELS 
            error('Function not yet working for uneven pixels')
        else % EVEN # OF PIXELS
            if size(val,1) == npix
                v = [];
                centerIndex = ceil((npix_out+1)/2) + rem(npix,2);
                halfLength  = floor(npix/2);
                v           = true(npix_out,1);
                v((0:npix-1)+centerIndex-halfLength) = false;
                [iv,jv] = meshgrid(1-v); idc = logical(iv.*jv);
                test = zeros(npix_out);
                test(idc) = Iin;
                val = ifft2(test);
            elseif size(val,1) == npix_out
                val = ifft2(Iin);
            end
            val = val(1:npix,1:npix);       % matlab fft2 pads the zeros all on the end!!
            waveBPrgted = val*npix_out./fftPhasor;
        end
end