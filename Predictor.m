classdef Predictor
    %% Basis functions

    properties       
        % Basic information regarding the method being simulated
            Method;                 % Prediction method
            ControlMethod;          % Control method (low-resolution)
            PredictionType;         % 'KF' if method relies on a KF
            SettingsType;           % Further settings w.r.t. PredictionType (e.g. 'IF' for information filter implementation)
            SensorType;             % WFS-based, WFS-less, or combined (in case of control + prediction)
            SensorNumber;           % Sensor numbers that this method uses
            PathNumber;             % Light path number for this method
            BasisType;              % Type of basis function
            TruncType;              % Type of PSF truncation (box or value)
            ModelType;              % Type of model identification being used (System identification or First principles)
            SIDataType;             % Type of identification data being used
        % Pupil geometry of the wavefront, DM and WFS
            pupilLogical;           % Pupil of wavefront
            validActuators;         % Pupil for DM actuators
            validLenslets;          % Pupil for WFS lenslets
        % Phase and GPF estimates and results
            Phase_est;              % Phase estimate
            GPF_est;                % Generalized Pupil Function estimate
            VPhase_est;             % " (V is standing for vectorized)
            VGPF_est;               % "
            x_est;                  % Model coefficient estimate (e.g. when using Gaussian radial basis functions)
        % Real values of the phase, GPF, and GPF amplitude
            Phase_real;             % True phase
            Phase_real_fried;       % True phase on Fried geometry resolution
            GPF_real;               % True GPF
            GPF_real_fried;         % True GPF on Fried resolution
            VPhase_real;            % " V standing for vectorized
            VPhase_real_fried;      % "
            PupilPlaneAmplitude;    % True GPF amplitude
        % PSF measurements
            NyquistSampling;        % Nyquist sampling rate (1 for Nyquist sampling)
%             ComplexField;           % Complex-valued focal plane field (complex PSF)
            fftphasor;              % FFT Phasor 
            PSFs;                   % PSFs in original 2D form
            PSFs_old;               % Previous PSF measurements
            PSFmeasurement;         % Active PSF measurements (given a phase diversity) 
        % Phase Diversity information
            nPD;                    % number of PD images (including non-active PDs)
            PhaseDiversity;         % the actual phase diversities corresponding to each camera
            activePDs;              % vector containing the camera channels used PD cameras for prediction [length(activePDs) <= nPD]
            activePDsControl;       % vector containing the camera channels used PD cameras for control
            activePD;               % currently active PD (single integer value <= nPD)
        % PSF processing
            yPSF;                   % current PSF image corresponding to currently active PD (single image) 
            active_pixels;          % list of all active pixels after truncation
            n_active_pixels;        % number of pixels of PSF used after truncation
            ybox;                   % width of ybox in case TruncType = 'ybox'
            ytrunc;                 % truncation value in case TruncType = 'ytrunc'
            ytrunc_control;         % truncation value if a seperate focal-plane sensing-based controller is used
        % Noise properties
            photonNoise;            % photon noise: true or false
            readOutNoise;           % read-out noise value
            nPhotonBackground;      % photon background value
        % Basis
            basis;                  % structure containing basis function information
        % Model
            Models;                 % structure containing dynamic models
        % WFS 
            WFSmeasurement;         % WFS signal
            SH;                     % structure containing SH info
        % DM
            DM;                     % Structure containing DM info
        %Control        
            u;                      % increment low-res DM
            us;                     % all input low-res DM increments over time
            u_hr;                   % increment high-res DM
            us_hr;                  % all high-res DM increments over time
            u_dm;                   % applied to low-res DM
            us_dm;   
            u_dm_hr;                % applied to high-res DM
            us_dm_hr;    
            MVM;                    % MVM control matrix
        % KF
            KF                      % structure with Kalman filter info
        % timing
            times                   % structure with time info
    end
            
    methods
        
        %% Constructor
        function obj = Predictor(Method,ControlMethod,SensorType,PredType,...
                BasisType,ModelType,SIDataType,...
                dm,tel,ngs,...
                varargin)      
            
            %%% Add parameters
%             p = inputParser;
%             addParameter(p,'ARorder',1,@isnumeric)
%             addParameter(p,'r_graph',[],@isnumeric)
%             parse(p)
                       
            %%% Add inputs to object;
            obj.Method = Method;
            obj.ControlMethod = ControlMethod;
            obj.SensorType = SensorType;
            obj.PredictionType = PredType;
            obj.BasisType = BasisType;
            obj.ModelType = ModelType;
            obj.SIDataType = SIDataType;
            obj.times.k = 1;
               
            %%% Extract info from dm
            obj.u = dm.coefs;
            obj.u_hr = dm.coefs_hr;
            obj.DM.H = dm.H;
            obj.DM.Hr = dm.Hr;
            obj.DM.iHr = dm.iHr;
            obj.u_hr = zeros(size(dm.H_hr,2),1);
            obj.DM.H_hr = dm.H_hr;
            obj.DM.Hr_hr = dm.Hr_hr;
            obj.DM.iHr_hr = dm.iHr_hr;
            obj.validActuators = dm.validActuator;
            
            %%% Sort out varargin
            for i = 1:length(varargin)
                tmp = varargin{i};
                %%% If input argument is FPC (focal plane camera) or WFS
                if strcmp(tmp.tag,'FPC')   
                    cam = varargin{i};
                    %%% Set Camera Properties
                    obj.nPD = length(cam.PD);
                    for ipd = 1:obj.nPD
                        obj.PupilPlaneAmplitude{ipd} = ngs.amplitude;
                        obj.PhaseDiversity{ipd} = cam.PD{ipd};
                    end    
                    %%% Info from other objects that are often accessed in the
                    %%% predictor class:
                    obj.NyquistSampling = cam.nyquistrate;
                    obj.pupilLogical = tel.pupilLogical;    
                    obj.photonNoise = cam.photon_noise;
                    obj.readOutNoise = cam.read_noise;
                    obj.nPhotonBackground = cam.nPhotonBackground;
                elseif strcmp(tmp.tag,'WFS')
                    obj.pupilLogical = tel.pupilLogical; 
                    wfs = varargin{i};
                    obj.SH.R = wfs.R;
                    obj.SH.G = wfs.Gphi;
                    obj.SH.Ga = [obj.SH.G; null(obj.SH.G)'];
                    obj.validLenslets = wfs.validLenslets;
                end
                
            end

        end
                               
        %% Output        
        function obj = GetMeasurements(obj,cam,wfs,rcam,ngsview,ngs_fried,dm,dm_hr)
            % Extract measurements from all cameras and wfss
                if strcmp(obj.SensorType,'cam')
                    %%% Extract Camera Measurement
                    camnr = obj.SensorNumber;
                    obj.PSFmeasurement = zeros(cam{1,camnr}.resolution(1)^2,size(cam,1));
                    for k = 1:size(cam,1)   % nr of diversities
                        obj.PSFmeasurement(:,k) = vec(grab(cam{k,camnr}));      % read out camera
                        obj.PSFs(:,:,k) = grab(cam{k,camnr});
                    end
                    obj.ComplexField = rcam{camnr}.imgLens.complexfield;
                    obj.ysPSF = obj.PSFmeasurement(:,obj.activePDs);
                elseif strcmp(obj.SensorType,'wfs')
                    wfsnr = obj.SensorNumber;
                    obj.WFSmeasurement = wfs{wfsnr}.slopes;
                elseif strcmp(obj.SensorType,'wfs-cam')
                    camnr = obj.SensorNumber(2);
                    wfsnr = obj.SensorNumber(1);
                    %%% Extract Camera Measurement
                    obj.PSFmeasurement = zeros(cam{1,camnr}.resolution(1)^2,size(cam,1));
                    for k = 1:size(cam,1)   % nr of diversities
                        obj.PSFmeasurement(:,k) = vec(grab(cam{k,camnr}));      % read out camera
                        if obj.times.k > 1
                            obj.PSFs_old(:,:,k) = obj.PSFs(:,:,k);
                        else
                            obj.PSFs_old(:,:,k) =  grab(cam{k,camnr});
                        end
                        obj.PSFs(:,:,k) = grab(cam{k,camnr});
                    end
                    %%% WFS slopes
                    obj.WFSmeasurement = wfs{wfsnr}.slopes;
                end
                
                % Real data
                obj.Phase_real = ngsview{obj.PathNumber}.meanRmPhase;
                obj.GPF_real = ngsview{obj.PathNumber}.amplitude.*exp(1i*obj.Phase_real);
                
                % Mirror surfaces
                obj.DM.DM_surf = dm{obj.PathNumber}.surface;
                obj.DM.DM_hr_surf = dm_hr{obj.PathNumber}.surface;
                
                % Real Fried geom data (useful for theoretical optimum of
                % WFS-based control and prediction)
                obj.Phase_real_fried = ngs_fried{obj.PathNumber}.meanRmPhase;
                
        end
        
        %% Control - Selection of method
        function obj = ComputeControl(obj,control_prop,LowResControl)
            
            tstart = tic;
            if LowResControl == 1   
                if strcmp(obj.ControlMethod,'MVM')
    %                 obj = MVMControl(obj,0);
                    obj = MVMControl(obj,control_prop.cMVM);
                elseif strcmp(obj.ControlMethod,'DMopt')
                    obj = DMopt_Control(obj);
                elseif strcmp(obj.ControlMethod,'AP')
                    obj = AP_Control(obj);
                %%% Add your new control function here by replacing
                % ControlMethodName and NewFunction_Control with the new
                % method name and function name.
%               elseif strcmp(obj.ControlMethod,ControlMethodName)
%                   obj = NewFunction_Control(obj);
                elseif strcmp(obj.ControlMethod,'None')
                    obj.u = zeros(size(obj.u));
                    obj.us = [obj.us obj.u];
                end
            end
            %%% Get timing
%             tlapsed = toc(tstart);
%             try
%                 obj.times.all = [obj.times.all tlapsed];
%             catch
%                 obj.times.all = tlapsed;
%             end
%             obj.times.avg = mean(obj.times.all);
%             obj.times.k = obj.times.k + 1;
        end
        
        %% Prediction - Selection of method
        function obj = ComputePrediction(obj)
            % Compute the prediction using the method specified in
            % obj.Method.
            
            tstart = tic;   % track the prediction time
            
            if obj.times.k <= 3
                % First run, AP or MVM for a number of time steps, until
                % the closed-loop controller has converged. 
                if strcmp(obj.SensorType,'cam')
                    obj = AP_Prediction(obj);   
                elseif strcmp(obj.SensorType,'wfs')
                    obj = MVM_Prediction(obj);   
                elseif strcmp(obj.SensorType,'wfs-cam')
                    obj = AP_Prediction(obj);   
                end
                % And set the estimates to zero, since they are generally
                % not correct, nor obtained using the chosen methods.
                obj.GPF_est = abs(obj.GPF_est);
                obj.VGPF_est = abs(obj.VGPF_est);
                obj.Phase_est = 0*obj.Phase_est;
                obj.VPhase_est = 0*obj.VPhase_est;
            else
                % Please add any new function here to link it with the
                % desired tag. 
                if strcmp(obj.Method,'MVM')
                    obj = MVM_Prediction(obj);
                elseif strcmp(obj.Method,'DMopt')
                    obj = DMopt_Prediction(obj);
                elseif strcmp(obj.Method,'IEKF')
                    obj = IEKF_Prediction(obj);    
                elseif strcmp(obj.Method,'AP')
                    obj = AP_Prediction(obj);
                elseif strcmp(obj.Method,'MBAP')
                    obj = ModalAP_Prediction(obj);
                % Please add your new prediction method by replacing
                % MethodName and NewFunction_Prediction with the new
                % method name and function name.
%                 elseif strcmp(obj.Method,MethodName)
%                     obj = NewFunction_Prediction(obj);
                else
                    error('Method unknown, make sure your method has been added to the function "ComputePrediction" in "Predictor.m"')
                end
            end            
            
            %%% Get timing
            tlapsed = toc(tstart);
            try
                obj.times.all = [obj.times.all tlapsed];
            catch
                obj.times.all = tlapsed;
            end
            obj.times.avg = mean(obj.times.all);
            obj.times.k = obj.times.k + 1;
        end
        
        
        %% Control - Selection of method
        function obj = ComputeHighResControl(obj,control_prop,HighResControl)  
            if HighResControl == 1 && obj.times.k > 3
                % Here you can program your own control method ...
                
                %%% High-res DM control law:
                    % Compute new wavefront increment
                    d_phase_pred = (obj.Phase_est(tel_prop.pupilLogical) ...
                        - obj.DM.Hr*dm{k}.coefs - obj.DM.Hr_hr*dm_hr{k}.coefs)...
                        + obj.DM.Hr*obj.u_dm;
                    % Map on high-res DM
                    obj.u_hr = -1*obj.DM.iHr_hr*d_phase_pred - dm_hr{k}.coefs;
                    obj.u_dm_hr = dm_hr{k}.coefs + obj.u_hr;
                %%% Store     
                    obj.us_hr = [obj.us_hr obj.u_hr];   
                    obj.us_dm_hr = [obj.us_dm_hr obj.u_dm_hr]; 
            else
                %%% If HighResControl is off, it will simply set the dm
                %%% commands to zero.
                obj.u_hr = zeros(size(obj.DM.iHr_hr,1),1); 
                obj.us_hr = [obj.us_hr obj.u_hr];   
                obj.us_dm_hr = [obj.us_dm_hr obj.u_dm_hr]; 
            end
        end
                        
    end
    
    methods (Static)
        
        function fftPhasor = CreatePhasor(npix,npixpad)
            %%% OOMAO's fftPhasor function
            %%% NOTE: DUE TO A PRIOR ERROR THE CODE CURRENTLY ONLY WORKS
            %%% FOR EVEN VALUES OF npix. 
            %%% THIS WILL BE UPDATED IN A FUTURE VERSION.
            
        %     evenOdd       = rem(npix,2);
        %     if ~rem(npixpad,2) && evenOdd
        %         npixpad = npixpad + evenOdd;
        %     end
        %     npixpad = max(npixpad,npix);

            if ~rem(npix,2)
                % shift the intensity of half a pixel for even sampling
                [u,v] = ndgrid((0:(npix-1)).*(~rem(npix,2)-npixpad)./npixpad);
                fftPhasor = exp(-1i.*pi.*(u+v));
            else
                fftPhasor = [];
            end            
        end
                      
    end
    
end