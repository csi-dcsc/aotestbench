function obj = IEKF_mu(obj)
    % IEKF measurement update
    
    %% Parameters: 
%     nss = 200;%length(obj.yPSF);
    maxiter = 10;
    serialupdt = false;
    
    %% Obtain filter type
    type = obj.KF.type;
    if strcmp(type,'KF')
        typeflag = 1;
        solver = 'Direct';
    elseif strcmp(type,'sqrtKF')
        typeflag = 2; 
        solver = 'Direct';
    elseif  strcmp(type,'IF')
        typeflag = 3;
        solver = 'CG';
    end
        
    %% Get data from object 
    
    %%% Output && measurement noise
    y = obj.yPSF;      
    rcnst = 0;  % since the estimation y equals the 'true' y is very unsure, adding an additional constant to r might be beneficial.
    r = abs(y + obj.nPhotonBackground) + obj.readOutNoise.^2 + rcnst;   
    
    %%% Time update
    x_t = obj.KF.x_tu; 
    if typeflag == 1
        P_t = obj.KF.P_tu;      
    elseif typeflag == 2
        Psqrt_t = obj.KF.Psqrt_tu;       
    elseif typeflag == 3
        iP_t = obj.KF.iP_tu;
    end
    
    %%% Phase diversity   
    GPF0 = obj.PupilPlaneAmplitude{obj.activePD}...
        .*exp(1i*obj.PhaseDiversity{obj.activePD});     % Diversity + amplitude info

    %%% Sparsities
    GraphMask = obj.Models.ModelPhi_pix.GraphSparsityPattern;
%     BandedMask = obj.Models.ModelPhi_pix.BandedSparsityPattern;

    
    %% Process output date
    
    %%% Data cutoff
%     [y_sort,y_idx] = sort(y,'descend'); 
%     ytrunc =  obj.ytrunc;
%     y_cutoff_idx = find(abs(y_sort) <= ytrunc,1);
%     ysel = y_idx(1:y_cutoff_idx);


    %%% select active pixels
    obj = select_active_pixels(obj);

    if strcmp(solver,'CG')  
        % Truncate
        ytrunc = 1;%1 + obj.readOutNoise;     
        y(y<ytrunc) = 0;
        [ysort,yselsort] = sort(y,'descend');
        % Box or value truncation
        if strcmp(obj.TruncType,'ybox')
            ysel_tr = obj.active_pixels;
        else
            nss = 400;%length(obj.yPSF);
            ysel_tr = yselsort(1:nss);
        end
        % need all data to perform the FFTs
        obj.active_pixels = 1:length(obj.yPSF);
        obj.n_active_pixels = length(obj.active_pixels);
        ysel = obj.active_pixels;
    else
        ytrunc = 1 + obj.readOutNoise;        
        y(y<ytrunc) = 0;
        [ysort,yselsort] = sort(y,'descend');
        ysel = obj.active_pixels;
    end
        
    % Data selection
    ys = y(ysel);
    
    %%% construct truncated measurement noise covariance
    if typeflag == 1
        Rs = sparse(diag(r(ysel)));  % Measurement covariance matrix
        obj.Models.RPSF = Rs;
    elseif typeflag == 2
        Rssqrt = sparse(diag(sqrt(r(ysel))));
    elseif typeflag == 3
        irs = 1./r(ysel);
    end
    
    %% Define basis & dimensions
    npix = size(GPF0,1);
    if strcmp(obj.BasisType,'Pixel')    
        phase_basis = reshape(eye(npix^2),[npix npix npix^2]); 
    elseif strcmp(obj.BasisType,'DM')    
        phase_basis = obj.basis.basis_phase_pix;       
    elseif strcmp(obj.BasisType,'GRBF')    
        phase_basis = obj.basis.GRBFs(:,:,obj.basis.active_GRBFs);
    end

    % Dimensions
    n = size(phase_basis,1);            % number of pixels in phase basis
    m = size(phase_basis,3);            % number of basis functions (=n^2 for pixel grid)
    p = 2*obj.NyquistSampling*n;        % number of PSF pixels
    ns = length(x_t);
    ps = length(ysel);
    
    %% Initialize
    % Initialization -- use previous IEKF iteration's measurement
    % update or time update if first iteration
    x = x_t;                                    % Initialize basis coef.
    if strcmp(obj.BasisType,'Pixel')
        Phase_t = zeros(n);
        Phase_t(obj.pupilLogical(:)) = x_t;   % Phase initialization
    else
        phase_basis_vec = reshape(phase_basis,[n^2,m]);
        Phase_t = phase_basis_vec*x_t;   % Phase initialization
    end
    GPF_t = reshape(exp(1i*Phase_t),[n n]);    % GPF based on initial guess
    GPF_i = GPF0.*GPF_t;                    % Add amplitude info and diversity

    %% IEKF iterations
    if serialupdt
        nii = length(ysel);
        ysel_all = ysel;
        ys_all = ys;
    else
        nii = 1;
    end

for ii = 1:nii
    if serialupdt
        ysel = ysel_all(ii);
        ys = y(ysel);
        Rs = r(ysel);
        iRs = 1./r(ysel);
    end
    for iter = 1:maxiter
        %%% Kalman filter measurement update 
        if typeflag == 1     
            [Cs,Js] = Lin_PSF(obj,GPF_i,ysel);
            ry = ys - Cs - Js*(x_t - x);
            PJ = P_t*Js';
            if iter ~= maxiter
                Kmry = PJ*((Js*PJ + Rs)\ry);
                x = x_t + Kmry;
            else
                Km = PJ/(Js*PJ + Rs);
                x = x_t + Km*ry;
            end
        elseif typeflag == 2
            [Cs,Js] = Lin_PSF(obj,GPF_i,ysel);
            ry = ys - Cs - Js*(x_t - x);
            [~,Rfac] = qr([(Js*Psqrt_t)' Psqrt_t'; -Rssqrt zeros(ps,ns)]);
            Rfac = Rfac';
            Kmry = Rfac(ps+1:end,1:ps)*(Rfac(1:ps,1:ps)\ry);
            x = x_t + Kmry;
        elseif typeflag == 3
            if strcmp(solver,'CG')
                % Linearization around current estimate GPF_i
                [Cf,Jf] = Lin_PSF_fun(obj,GPF_i);
                % Construct normal equations M*x = b
                M.J = Jf;
                M.iP = iP_t;
                M.ir = irs;
                iPx = iP_t*(x_t - x);
                iry = irs.*(ys - Cf);
                JTiRy = J_mvm(Jf,iry,'transposed');
                b = JTiRy(obj.pupilLogical) + iPx;
                % Run CG method
                [dx,info] = IEKF_mu_CG_solver(M,b,obj,true);
%                 dx2 = (iP_t + Js'*iRs*Js)\(Js'*iRs*(ys - Cf) + iP_t*(x_t - x)  );
                if norm(dx)/norm(x) < 1 % Extra guarrantee against instability (not necessary in most cases)
                    x = x + dx(obj.pupilLogical(:));
                elseif norm(dx)/norm(x) < 6 
                    x = 0.99*x + 0.1*dx(obj.pupilLogical(:));
                    disp(['k = ' num2str(obj.times.k) ': Possible unstable Meas. updt scaled down'])
                else
                    x = 0.99*x;
                end
                if iter == maxiter
                    [Css,Jss] = Lin_PSF(obj,GPF_i,ysel_tr);
                    JiRJ = PartialMMM(Jss',(irs(ysel_tr).*Jss),...
                        obj.Models.ModelPhi_pix.SparsityApprox.E); %SparsityGraph.E, SparsityApprox.E
                end
            else % standard direct method
                [Cs,Js] = Lin_PSF(obj,GPF_i,ysel);
                ry = ys - Cs - Js*(x_t - x);
                JiR = irs'.*Js';
    %             [jirj,rows,cols] = PartialMatrixProd(JiR,Js,GraphMask);
    %             JiRJ = sparse(rows,cols,jirj);
                JiRJ = JiR*Js;
                Kmry = (iP_t + JiRJ)\(JiR*ry); 
                x = x_t + Kmry;
            end
        elseif typeflag == 4
            [Cs,Js] = Lin_PSF(obj,GPF_i,ysel);
            ry = ys - Cs - Js*(x_t - x);
            [Qfac,Rfac] = qr([iPsqrt_t; iRssqrt*Js],'econ');
            x = Rfac\(Qfac*([iPsqrt_t*x_t; iRssqrt*y]));
        end
        
        %%% Construct new GPF for linearization
        if strcmp(obj.BasisType,'DM')
            tmp = reshape(exp(1i*reshape(phase_basis,[n^2 m])*x),[n n]);
        elseif strcmp(obj.BasisType,'GRBF')
            tmp = reshape(exp(1i*reshape(phase_basis,[n^2 m])*x),[n n]);
        elseif strcmp(obj.BasisType,'Pixel')
            tmp = zeros(n);
            tmp(obj.pupilLogical) = exp(1i*x);
        end
        GPF_i = GPF0.*tmp;
        
        % Uncomment this block to see the cost function for analysis   
        %  nrys(iter) = norm(ry);
        %  x_all(iter) = x;
        %  if iter > 1
        %     cost(iter-1) = (x_t - x_all(:,iter-1))'*(P_t\(x_t - x_all(:,iter-1))) + ...
        %     (y(ysel) - Cs)'*((y(ysel) - Cs)./(diag(Rs)));
        %  end
    
    end
  
    %%% Measurement update
    obj.KF.x_mu = x;
    obj.KF.x_tu = obj.KF.x_mu;
    
    %%% Covariance updates
    if typeflag == 1
        obj.KF.P_mu = P_t - Km*(Js*P_t);
        obj.KF.P_tu = obj.KF.P_mu;             % Useful in case of multiple measurement updates
    elseif typeflag == 2
        obj.KF.Psqrt_mu = Rfac(ps+1:ps+ns,ps+1:ps+ns);
        obj.KF.Psqrt_tu = obj.KF.Psqrt_tu;
    elseif typeflag == 3
        obj.KF.iP_mu = iP_t + JiRJ;
%         obj.KF.iP_tu = obj.KF.iP_mu;   
    elseif typeflag == 4
        obj.KF.iPsqrt_mu = Rfac;
        obj.KF.iPsqrt_tu = obj.KF.iPsqrt_tu;
    end
end
    
   
end
