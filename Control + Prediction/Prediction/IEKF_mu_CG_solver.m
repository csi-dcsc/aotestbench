function [x,info] = IEKF_mu_CG_solver(M,b,obj,analyse)
% Solves the Measurement update using a conjugate gradient method
% exploiting certain structures in the system

% Solves a system of equations of the form
% A*x = b  -- (x being the wavefront phi)
% where 
% A = iP + J'*iR*J
% b = J'*iR*y + iP*phi_hat

% M should be in the following format:
% M.J -- has the info stored for the jacobian
% M.iP -- has the inverse covariance matrix stored in a sparse format
% M.ir -- has the diagonal of the inverse measurement noise covariance

% b should be the right hand size (vector) that is constant throughout the
% CG iterations

% Pieter Piscaer, 2020

%% Dimensions
n = length(M.J.d2);  
xsel = obj.pupilLogical(:);

%% Initializations
x0 = zeros(n,1);
p = zeros(n,1);
threshold = 1e-6;
iPs = sparse(M.iP);
% iP = full(M.iP);
% iPg = gpuArray(iP);
% iPgs = gpuArray(iPs); 
% iPd = randn(size(iP));
count_stop = 0;

%% CG solver
x = x0;
Ax = J_mvm( M.J,(  M.ir.*J_mvm(M.J,x) ), 'transposed');
Ax = Ax(xsel) + M.iP*x(xsel);
r = b - Ax;
nb = b'*b;
p(xsel) = r;
rsold = r' * r;

for i = 1:length(b)
    tmp1 = J_mvm(M.J,p);
    tmp2 = M.ir.*tmp1;
    Apf = J_mvm(M.J,tmp2,'transposed') ;
%     Apf = J_mvm( M.J,(  M.ir.*J_mvm(M.J,p) ), 'transposed') ;
%     Apfs = Apf(xsel);
%     test = randn(n,1);
    Ap = Apf(xsel) + iPs*p(xsel);
    alpha = rsold / (p(xsel)' * Ap);
    x = x + alpha * p;
    r = r - alpha * Ap;
    rsnew = r' * r;
    if rsnew > rsold    % stop algorithm if it starts rising
        count_stop = count_stop + 1;
    else
        count_stop = 0;
    end
    if count_stop > 20 && i > 30
        break
    end
    if rsnew/nb < threshold && i > 1
        break
    end
    p(obj.pupilLogical) = r + (rsnew / rsold) * p(xsel);
    rsold = rsnew;
    if analyse 
        r_all(i) = rsold;
    end
end
%% Analysis output
if analyse
    info.iterations = i;
    info.residuals = r_all;
    info.threshold = threshold;
else
    info.info = 'analysis mode was turned off';
end
   
    
end