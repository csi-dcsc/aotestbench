function obj = ModalAP_Prediction(obj)
% Modal based alternating projection method.
% This function uses Gaussian radial basis function to represent the
% complex-valued GPF. The phase retrieval problem is solved in terms of the
% basis function coefficient using Alternating Projections

%%% Parameters
    iteration_num = 1000;
    
%%% Initializations and short-hand notations    
    camnrused = obj.activePDs;
    y = obj.PSFs(:,:,camnrused); 
    d = length(camnrused);
    p = size(y,1);
    
%%% Value truncation of y and take square root
    y(y < 0.5*obj.ytrunc) = 0;
    vec_y = reshape(y,[p^2,d]);
    abs_y = sqrt(vec_y);
    
%%% Store matrices to be used    
    ii = 1;
    for i = camnrused
        C{ii} = obj.Models.ModelGPF.C{i};
        C1{ii} = obj.Models.ModelGPF.C1{i};
        C2{ii} = obj.Models.ModelGPF.C2{i};
%         iC{ii} = obj.Models.ModelGPF.iC{i};
        iC1{ii} = obj.Models.ModelGPF.iC1{i};
        iC2{ii} = obj.Models.ModelGPF.iC2{i};
        ii = ii+1;
    end
    n = size(C1{1},2);
    
%%% Tmp    
%     iB1 = obj.Models.basis.iB1;
%     iB2 = obj.Models.basis.iB2;
%     A = abs(obj.GPF_real);
%     A1 = obj.Models.basis.A1;
%     A2 = obj.Models.basis.A2;
%     ax = sum(kr(iB1*A1,iB2*A2),2);  % least squares amplitude of x

%%% Initialize x (basis function coefficients)
    if norm(obj.x_est) > 0                
        x_init = obj.x_est;  %obj.Models.basis.GRBFs_vec\vec(obj.GPF_real);
    else
        x_init = 10*(randn(size(C{1},2),1) + 1i*randn(size(C{1},2),1));  % obj.Models.basis.GRBFs_vec\vec(obj.GPF_real);
    end
    x = x_init;
    x2 = reshape(x,[n n]);
    
%%% Initialize y
    vec_y_est = zeros(size(vec_y));
    for i = 1:d
        vec_y_est(:,i) = abs(C{i}*x).^2;
    end


%%% AP iterations
    r(1) = NaN;
    stopcount = 0;
    for i = 1:iteration_num
        % LOOP over phase diversity images
        for j = 1:d               
            y_iter0 = sum(kr(C1{j},C2{j}*x2),2);            % Focal plane field based on estimated x
            y_iter = abs_y(:,j).*exp(1i*angle(y_iter0));    % substitute known |y|
            y2 = reshape(y_iter,[p p]);                     % Matricize y
            x = sum(kr(iC1{j},iC2{j}*y2),2);                % Compute x based on y
            x2 = reshape(x,[n n]);                          % Matricize x
            vec_y_est(:,j) = abs(y_iter0);                  % Estimation square root of PSF
        end 
        xs(:,i) = x;
        
        %%% CHECK residual and stopping criterea
        r(i+1) = norm(abs_y - vec_y_est)/norm(abs_y); 
        if abs(r(i)) - abs(r(i+1)) < 1e-4*abs(r(i+1)) && i > 10
            stopcount = stopcount + 1;
        else 
            stopcount = 0;
        end
        if stopcount == 20
            break
        end
    end     % END OF AP ITERATIONS


%%% Compute GPF
    obj.x_est = x;
    obj.GPF_est = obj.pupilLogical.*reshape(obj.basis.GRBFs_vec*obj.x_est,...
        [size(obj.GPF_real,1),size(obj.GPF_real,2)]);

%%% Unwrap and remove piston
    obj.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        obj.pupilLogical.*fun_phaseunwrap(angle(obj.GPF_est),'Miguel'))); 
%             for tmp = 1:10
%             obj.Phase_est = obj.pupilLogical.*...
%                 Remove_mean(double(...
%                 fun_phaseunwrap(obj.Phase_est,'Miguel'))); 
%             end
    obj.VGPF_est = vec(obj.GPF_est(obj.pupilLogical(:)));
    obj.VPhase_est = vec(obj.Phase_est(obj.pupilLogical(:)));

end
