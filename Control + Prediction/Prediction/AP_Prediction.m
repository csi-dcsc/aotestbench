function obj = AP_Prediction(obj)

    iteration_num = 3000;
    camnrused = obj.activePDs;

    % Load data 
    d = length(camnrused);
    y = obj.PSFs(:,:,camnrused); 
    y_trunc = obj.ytrunc;
    y(y < y_trunc) = 0;
    p = size(y,1);
    vec_sqrty = reshape(sqrt(y),[p^2,d]);
    I_a = sqrt(y);
    n = size(obj.GPF_real,1);
    n_out = 2*obj.NyquistSampling*n;
    ni = size(y,1);
    for ii = 1:d
        P_div{ii} = obj.PhaseDiversity{camnrused(ii)};
    end

    % Initialize x 
    if norm(obj.GPF_est) > 0
        x_init = obj.GPF_est;
    else
        x_init = 0.01*(randn(n,n) + 1i*randn(n,n));
    end
    
    % Initialize
    x = x_init;
    r(1) = 1;
    stopcount = 0; 

    %%% fft phasor
    fftphasor = obj.CreatePhasor(n,n_out);

    % AP iterations
    for i=1:iteration_num
        for ii = 1:d      
            % Uncomment this line for GS-based AP assuming knowledge of the pupil, 
            x = abs(obj.PupilPlaneAmplitude{1}).*exp(1i*angle(x));
            % Alternating projections
            [I1] = alt_propagateThrough(x.*exp(1i*P_div{ii}),fftphasor,ni,n_out);
            I2 = I_a(:,:,ii).*exp(1i*angle(I1));
            x = alt_propagateBack(I2,fftphasor,n,n_out).*exp(-1i*P_div{ii});  
            y_est(:,ii) = vec(abs(I1));
        end
                
        % Save residual
        r(i+1) = norm(vec_sqrty - y_est)/norm(vec_sqrty);
        
        % Stopping criteria
        if abs(r(i)) - abs(r(i+1)) < 1e-5*abs(r(i+1)) && i > 50
            stopcount = stopcount + 1;
            if stopcount == 20
                break
            end
        else 
            stopcount = 0;
        end
    end

    % Compute GPF
    obj.GPF_est = obj.pupilLogical.*x;
    obj.VGPF_est = vec(obj.GPF_est(obj.pupilLogical(:)));

    % Unwrap and remove piston
    obj.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        fun_phaseunwrap(angle(obj.GPF_est),'Miguel'))); 
    obj.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        fun_phaseunwrap(obj.Phase_est,'Miguel'))); 
    obj.VPhase_est = vec(obj.Phase_est(obj.pupilLogical(:)));

end
