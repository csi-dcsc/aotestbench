function obj = IEKF_Prediction(obj)

    %% Different method types and computation methods
    type = obj.SettingsType; 
    
    %%% type flags
    if strcmp(type,'KF') 
        typeflag = 1;
    elseif strcmp(type,'sqrtKF') 
        typeflag = 2;
        error('Not yet implemented.')
    elseif strcmp(type,'IF') 
        typeflag = 3;
    elseif strcmp(type,'sqrtIF')
        error('Not yet implemented.')
    end
        
    %%% Pre-computations for speed-up 
    % If you use different masks, uncomment the corresponding lines.
    % Mask of AR graph A
%     GraphMask = obj.Models.ModelPhi_pix.GraphSparsityPattern;
%     GraphMaskSp = (obj.Models.ModelPhi_pix.GraphSparsityPattern); 
    GraphSparsityCell = obj.Models.ModelPhi_pix.SparsityGraph.E;
    % Over-estimation of a sparsity pattern
    ApproxSparsityCell = obj.Models.ModelPhi_pix.SparsityApprox.E;
    ApproxMaskSp = obj.Models.ModelPhi_pix.SparsityApprox.Mask;
    % Tight mask (9 nnz per row)
    GraphSparsity9Cell = obj.Models.ModelPhi_pix.Sparsity9.E;
%     GraphMask9Sp = (obj.Models.ModelPhi_pix.Sparsity9.Mask);

    
    %% Load Models
    % TODO: Does not work properly for GRBF and DM at the moment!
    if strcmp(obj.BasisType,'Pixel')
        A = obj.Models.ModelPhi_pix.Asp;
        if typeflag == 1    
                Q = obj.Models.ModelPhi_pix.Q;    
        elseif typeflag == 2
                Qsqrt = obj.Models.ModelPhi_pix.Qsqrt;
        elseif typeflag == 3 
            iQ = obj.Models.ModelPhi_pix.iQ_spgs;   
%             iQ = 0.5*obj.Models.ModelPhi_pix.iQ_spgs;     % At very low brightness conditions, decreasing tuning Q^{-1} (or R) can improve performance 
            if isempty(obj.KF)   
                obj.Models.ModelPhi_pix.iQA = sparse(iQ*A);
                obj.Models.ModelPhi_pix.ATiQA =  sparse(A'*iQ*A);
            end
        end  
    elseif strcmp(obj.BasisType,'GRBF')
        A = 0.8*obj.Models.ModelPhi_GRBF.A;
        Q = obj.Models.ModelPhi_GRBF.Q;
        A_pix = obj.Models.ModelPhi_pix.A;
        Q_pix = obj.Models.ModelPhi_pix.Q;
    end
    
    %% Initializations
    if isempty(obj.KF)        
        
        % Create the KF structure in the object
        obj.KF.type = type;
        
        %%% Initialize P
        if typeflag == 1
            obj.KF.P_tu = Q;
        elseif typeflag == 2
            obj.KF.Psqrt_tu = Qsqrt;
        elseif typeflag == 3
            obj.KF.iP_tu = iQ; 
        end
        
        %%% initialize time updates
        if strcmp(obj.BasisType,'Pixel')
            obj.KF.x_tu = 0*vec(obj.Phase_est(obj.pupilLogical));
%             obj.KF.x_tu = 0.05*randn(size(obj.KF.x_tu)); 
        elseif strcmp(obj.BasisType,'GRBF')  
            GRBF_vec = obj.basis.GRBFs_vec(obj.pupilLogical(:),obj.basis.active_GRBFs);
            obj.KF.x_tu = GRBF_vec\vec(obj.Phase_est(obj.pupilLogical));
        end
        
        % Initialize measurement updates
        obj.KF.x_mu = obj.KF.x_tu; 
        if typeflag == 1
            obj.KF.P_mu = obj.KF.P_tu;
        elseif typeflag == 2
            obj.KF.Psqrt_mu = obj.KF.Psqrt_tu;
        elseif typeflag == 3
            obj.KF.iP_mu = obj.KF.iP_tu; 
        end
    else
        % Add DM update to current measurement
        uk = obj.us_dm(:,end);      %sum(obj.us(:,1:end-1),2);
        ukm1 = obj.us_dm(:,end-1);   %sum(obj.us(:,1:end-2),2);
        uk_hr = obj.us_dm_hr(:,end); %sum(obj.us_hr(:,1:end),2);
        ukm1_hr = obj.us_dm_hr(:,end-1);   %sum(obj.us_hr(:,1:end-1),2);
        if strcmp(obj.BasisType,'Pixel')
            Phase_dm = obj.DM.Hr*uk - A*obj.DM.Hr*ukm1;
            Phase_dm_hr = obj.DM.Hr_hr*uk_hr - A*obj.DM.Hr_hr*ukm1_hr;
            Phase_dm = Phase_dm + Phase_dm_hr;
            obj.KF.x_tu = obj.KF.x_tu + Phase_dm;
            obj.KF.x_tu = obj.KF.x_tu - mean(obj.KF.x_tu);
        elseif strcmp(obj.BasisType,'GRBF')
            Phase_dm = obj.DM.Hr*uk - A_pix*obj.DM.Hr*ukm1; 
            GRBF_vec = obj.basis.GRBFs_vec(obj.pupilLogical(:),obj.basis.active_GRBFs);
            obj.KF.x_tu = obj.KF.x_tu + GRBF_vec\Phase_dm; 
        end
    end   
    
        %% Measurement update post processing
        %%% Wrap back if a pixelbasis is used to remove high values
        if strcmp(obj.BasisType,'Pixel')
            obj.KF.x_tu = angle(exp(1i*obj.KF.x_tu));
        end

    
    %% Measurement update
    for j = 1:length(obj.activePDs)
        obj.activePD = obj.activePDs(j);
        obj.yPSF = obj.PSFmeasurement(:,obj.activePD);
%         obj.PSFmeasurements = [obj.PSFmeasurements obj.yPSF];
        obj = IEKF_mu(obj);
    end   

    %% Measurement update post processing
    %%% Wrap back if a pixelbasis is used to remove high values
        if strcmp(obj.BasisType,'Pixel')
            obj.KF.x_mu = angle(exp(1i*obj.KF.x_mu));
        end

    %% Time update
    obj.KF.x_tu = A*obj.KF.x_mu;  
    if typeflag == 1           
        obj.KF.P_tu = A*obj.KF.P_mu*A' + Q;  
    elseif typeflag == 2
        [~,Rfac] = qr([-A*obj.KF.Psqrt_tu Qsqrt]');
        Rfac = Rfac';
        obj.KF.Psqrt_tu = Rfac(:,1:size(Rfac,1)); 
    elseif typeflag == 3
        iQA = obj.Models.ModelPhi_pix.iQA;
        ATiQA = obj.Models.ModelPhi_pix.ATiQA;
        tmpM = obj.KF.iP_mu + ATiQA;
        tmpiM = SparseApproxInv(tmpM,ApproxSparsityCell); %GraphSparsityCell,GraphSparsity9Cell, ApproxSparsityCell
        tmp1 = iQA*tmpiM;
        tmp2 = (tmp1*iQA').*ApproxMaskSp; % GraphMaskSp
        obj.KF.iP_tu = iQ - tmp2;
    end

    %% Time update post processing
    %     apply smoothing by convolution
%         tmp = zeros(size(obj.pupilLogical,1));
%         tmp(obj.pupilLogical) = exp(1i*obj.KF.x_tu);
%         tmp2 = conv2(tmp,1/9*ones(3),'same');
%         obj.KF.x_tu = angle(tmp2(obj.pupilLogical));

    %% Reconstruction 
        if strcmp(obj.BasisType,'Pixel')
            obj.Phase_est = zeros(size(obj.pupilLogical));
            obj.Phase_est(obj.pupilLogical) = obj.KF.x_mu;
            tmp = obj.pupilLogical.*...
            Remove_mean(double(fun_phaseunwrap(obj.Phase_est,'Miguel'))); 
%             while max(max(abs(tmp))) > 2*pi
%                 tmp(tmp > pi) = tmp(tmp > pi) - 2*pi;
%                 tmp(tmp < -pi) = tmp(tmp < -pi) + 2*pi;
%             end
            obj.Phase_est = obj.pupilLogical.*Remove_mean(tmp); 
        elseif strcmp(obj.BasisType,'DM')
            obj.Phase_est = obj.pupilLogical.*...
                reshape(real(obj.basis.basis_phase_pix_vec*obj.KF.x_mu),...
                size(obj.GPF_real));
        elseif strcmp(obj.BasisType,'GRBF')
            obj.Phase_est = obj.pupilLogical.*...
                reshape(real(obj.basis.GRBFs_vec(:,obj.basis.active_GRBFs)*obj.KF.x_mu),...
                size(obj.GPF_real));    
        end
        obj.GPF_est = obj.PupilPlaneAmplitude{1}.*exp(1i*obj.Phase_est);
        obj.VPhase_est = vec(obj.Phase_est(obj.pupilLogical(:)));

end
