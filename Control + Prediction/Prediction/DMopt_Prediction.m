function obj = DMopt_Prediction(obj)
    % Non-realistic controller mapping real phase onto DM
    % Only to be used as a theoretical upper bound of compensation
%             obj.Phase_est = obj.Phase_real_fried;
%             obj.VPhase_est = obj.Phase_real_fried(obj.validActuators(:));
    obj.VPhase_est = obj.Models.ModelPhi_diag.H*(obj.DM.Hr\obj.Phase_real(obj.Phase_real ~= 0));   
    obj.Phase_est = zeros(size(obj.validActuators));
    obj.Phase_est(obj.validActuators) = obj.VPhase_est;
end