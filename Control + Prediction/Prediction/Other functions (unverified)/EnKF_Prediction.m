function obj = EnKF_Prediction(obj)

%%% Parameters (make variable later)
N = 500;
obj.KF.EnKF.c = 1.01;    %1.1
obj.KF.EnKF.method = 'Sampling';
obj.KF.Psparse = true;

%%% Load Models
    if strcmp(obj.BasisType,'Pixel')
        A = obj.Models.ModelPhi_pix.A;
        if obj.KF.Psparse
            Q = obj.Models.ModelPhi_pix.Q_sp;
        else
            Q = obj.Models.ModelPhi_pix.Q;
        end
    end
    
    %%% Notation / Initializations
    try 
        obj.KF.k;
        firstiterflag = false;
    catch
        firstiterflag = true;
    end
    
    if firstiterflag 
        obj.KF.k = 1;
        if strcmp(obj.BasisType,'Pixel')
            obj.KF.x_tu = 0*vec(obj.Phase_est(obj.pupilLogical));
            obj.KF.X_tu = obj.KF.x_tu*ones(N,1)' + 0.5*randn(length(obj.KF.x_tu),N);
        end
        % Initialize measurement updates
        obj.KF.x_mu = obj.KF.x_tu; 
        obj.KF.X_mu = obj.KF.X_tu; 
    else
        obj.KF.k = obj.KF.k+1;
        % Add DM update to current measurement
        if strcmp(obj.BasisType,'Pixel')
            uk = sum(obj.us(:,1:end-1),2);
            ukm1 = sum(obj.us(:,1:end-2),2);
            Phase_dm = obj.DM.Hr*uk - A*obj.DM.Hr*ukm1;
            obj.KF.x_tu = obj.KF.x_tu + Phase_dm;
            obj.KF.x_tu = obj.KF.x_tu - mean(obj.KF.x_tu);
            obj.KF.X_tu = obj.KF.X_tu + Phase_dm;
            obj.KF.X_tu = obj.KF.X_tu - mean(obj.KF.X_tu);
        end
        % In case of no IEKF iterations
        obj.KF.x_mu = obj.KF.x_tu; 
        obj.KF.X_mu = obj.KF.X_tu; 
    end   

    %%% Measurement update
    for j = 1:length(obj.activePDs)
        obj.activePD = obj.activePDs(j);
        obj.yPSF = obj.PSFmeasurement(:,obj.activePD);
        obj.PSFmeasurements = [obj.PSFmeasurements obj.yPSF];
        obj = EnKF_mu(obj);
    end   

    if strcmp(obj.BasisType,'Pixel')
    %%% Wrap back
        tmp = obj.KF.x_mu;
        while any(abs(tmp) > pi)
            tmp(tmp > pi) = tmp(tmp > pi) - 2*pi;
            tmp(tmp < -pi) = tmp(tmp < -pi) + 2*pi;
        end
        obj.KF.x_mu = tmp;
%                   obj.KF.x_mu = angle(exp(1i*obj.KF.x_mu));
    end

    %%% Time update
        obj = EnKF_tu(obj);

    %%% Smoothen initial guess -- apply smoothing by convolution
        tmp = zeros(size(obj.pupilLogical,1));
        tmp(obj.pupilLogical) = exp(1i*obj.KF.x_tu);
        tmp2 = conv2(tmp,1/9*ones(3),'same');
        obj.KF.x_tu = angle(tmp2(obj.pupilLogical));

    %%% Reconstruction
        if strcmp(obj.BasisType,'Pixel')
            obj.Phase_est = zeros(size(obj.pupilLogical));
            obj.Phase_est(obj.pupilLogical) = obj.KF.x_mu;
        end
        obj.GPF_est = obj.PupilPlaneAmplitude{1}.*exp(1i*obj.Phase_est);
%                 obj.Phase_est = obj.pupilLogical.*angle(obj.GPF_est);
        obj.VPhase_est = vec(obj.Phase_est(obj.pupilLogical(:)));

end