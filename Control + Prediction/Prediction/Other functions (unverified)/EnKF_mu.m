function obj = EnKF_mu(obj)
% Assumes the model:
% x(k+1) = A x(k) + w(k)
% y(k) = h(x(k)) + v(k)
% 
% Where the linearization around x = x0 is defined by:
% y(k) = h0 + Jh*(x(k) - x0) + v(k)
%

%%% Check method
method = obj.KF.EnKF.method;
if strcmp(method,'Sampling') 
    mflag = 1;
elseif strcmp(method,'ETKF')
    mflag = 2;
elseif strcmp(method,'SerialSampling')
    mflag = 3;
elseif strcmp(method,'Direct')
    mflag = 4;
end

%%% Get data
X_tu = obj.KF.X_tu;
x_tu = obj.KF.x_tu;
c = obj.KF.EnKF.c;
yfull = obj.yPSF;

%%% Output channel selection
obj = select_active_pixels(obj);
ysel = obj.active_pixels;
ytrunc = obj.ytrunc;

%%% Dimensions
[n,N] = size(X_tu);
n1 = size(obj.pupilLogical,1);
p1 = size(obj.PSFs,1);
p = length(ysel);


%%% Sparsities
% localization = true;
GraphMask = obj.Models.ModelPhi_pix.GraphSparsityPattern;
if obj.KF.Psparse    
    BandedMask = obj.Models.ModelPhi_pix.BandedSparsityPattern;
end

%%% Batches
if mflag == 3
%     batchsize = 3*floor(sqrt(length(ysel)));
    batchsize = 1;
else
    batchsize = length(ysel);
end 

idx_batch = 1;
for jb = 1:floor(length(ysel)/batchsize)
    batch{jb} = ysel(idx_batch:idx_batch+batchsize-1);
    idx_batch = idx_batch + batchsize;
end
if idx_batch <= length(ysel)
    batch{jb+1} = ysel(idx_batch:end);
end

%%% Get diversity 
Phi0 = obj.PhaseDiversity{obj.activePD};
Amp0 = obj.PupilPlaneAmplitude{obj.activePD};
GPF0 = Amp0.*exp(1i*Phi0);     % Diversity + amplitude info

%%% Define non-linear output function in OOMAO
obj.fftphasor = obj.CreatePhasor(n1,p1);

% Pre compute
% T_tilde = eye(N) - 1/N*ones(N,1)*ones(N,1)';    % Transform for deviation computation
% f_tilde = @(x) x*eye(N) - 1/N*(x*ones(N,1))*ones(N,1)';

% GPF_tu_t = zeros(n1,n1,N);
% tmp_M = zeros(n1);
% for ii = 1:N
%     tmp_M(obj.pupilLogical(:)) = X_tu(:,ii);
%     GPF_tu = exp(1i*tmp_M);  
%     GPF_tu_t(:,:,ii) = GPF0.*GPF_tu;     
% end
% PSF_ensemble = SimulatePSF(obj,GPF_tu_t,'box','nonoise');
% Y_tu2 = reshape(PSF_ensemble,[size(PSF_ensemble,1)^2 N]);         

if mflag == 1 || mflag == 3
    for jbatch = 1:length(batch)
        % Output selection
        y = yfull(batch{jbatch});
        y(y < ytrunc) = 0;
        
        % Output ensemble
%         Y_tu = zeros(length(batch{jbatch}),N);
%         for ii = 1:N
%             Phase_tu = zeros(n1);
%             Phase_tu(obj.pupilLogical(:)) = X_tu(:,ii);
%             GPF_tu = exp(1i*Phase_tu);  
%             GPF_tu = GPF0.*GPF_tu;     
%             if strcmp(obj.PSF_trunc_type,'ybox')
%                 % now only works for batchsize = length(ysel);
%                 PSF_ensemble = SimulatePSF(obj,GPF_tu,'box','nonoise');
%                 tmp = vec(PSF_ensemble);
%                 Y_tu(:,ii) = tmp;
%             else
%                 PSF_ensemble = SimulatePSF(obj,GPF_tu,'full','noisy');
%                 tmp = vec(PSF_ensemble);
%                 Y_tu(:,ii) = tmp(batch{jbatch});
%             end
%         end
        GPF_tu_t = zeros(n1,n1,N);
        tmp_M = zeros(n1);
        for ii = 1:N
            tmp_M(obj.pupilLogical(:)) = X_tu(:,ii);
            GPF_tu = exp(1i*tmp_M);  
            GPF_tu_t(:,:,ii) = GPF0.*GPF_tu;     
        end
        PSF_ensemble = SimulatePSF(obj,GPF_tu_t,'box','nonoise');
        Y_tu = reshape(PSF_ensemble,[size(PSF_ensemble,1)^2 N]);    
        
        % Deviations
%         tic;
%         X_dev = X_tu*T_tilde;
%         Y_dev = Y_tu*T_tilde;
%         toc
%         tic
        X_dev = X_tu - (X_tu*ones(N,1))*(ones(N,1)/N)';
        Y_dev = Y_tu - (Y_tu*ones(N,1))*(ones(N,1)/N)';
%         toc
%         tic
%         X_dev = f_tilde(X_tu);
%         Y_dev = f_tilde(Y_tu);
%         toc
        % Covariance inflation
        X_tu = x_tu*ones(N,1)' + c*X_dev;
        X_dev = X_tu - (X_tu*ones(N,1))*(ones(N,1)/N)';

%         % Kalman Gain
%         % Solves a LS: K*Y_dev = X_dev


        % Implementation 1 - faster for small value of p and large n (p<N)
%         Rs = diag( abs(y + obj.nPhotonBackground) + obj.readOutNoise.^2 );
%         tmp_Y = (y*ones(N,1)' - Y_tu);
%         tmp_P = Rs + (1/(N-1))*(Y_dev*Y_dev');
%         tmp_M = tmp_P\tmp_Y;
%         tmp_Z = Y_dev'*tmp_M; 
% %         tmp_K = 1/(N-1)*(X_dev*Y_dev')/tmp_P;
%         X_mu = X_tu + 1/(N-1)*X_dev*tmp_Z;
        
        % Implementation 2 - faster for large p and n (N<p)
        iRs = diag( 1./(abs(y + obj.nPhotonBackground) + obj.readOutNoise.^2) );
        tmp_Y2 = (y*ones(N,1)' - Y_tu);
        tmp_Q2 = eye(N) + Y_dev'*iRs*(1/(N-1))*Y_dev;
        tmp_V2 = Y_dev'*iRs*tmp_Y2;
        tmp_W2 = tmp_Q2\tmp_V2;
        tmp_M2 = iRs*(tmp_Y2 - (1/(N-1))*Y_dev*tmp_W2);
        tmp_Z2 = Y_dev'*tmp_M2; 
        X_mu = X_tu + 1/(N-1)*X_dev*tmp_Z2;
%                 
%         % Measurement update
%         X_mu1 = X_tu + K1*(y*ones(N,1)' - Y_tu);
        x_mu = mean(X_mu,2);




        % Prepare for next batch
        X_tu = X_mu;
        x_tu = x_mu;
    
        %%% Save
        obj.KF.X_mu = X_mu;
        obj.KF.x_mu = x_mu;
%         obj.KF.K = K;
        obj.KF.X_tu = obj.KF.X_mu;
        obj.KF.x_tu = obj.KF.x_mu;
    end
    
elseif mflag == 2
    
    % Compute 1st order Taylor approximation
    Phase_tu = zeros(n1);
    Phase_tu(obj.pupilLogical(:)) = x_tu;
    GPF_tu = reshape(exp(1i*Phase_tu),[n1 n1]);  
    GPF_hat = GPF0.*GPF_tu;
    [Cs,Js] = Lin_PSF(obj,GPF_hat,ysel);    % Taylor approximation
   
    % Select y
    y = yfull(ysel);     
    
    % Propagate, y_est
%     y_est = SimulatePSF(obj,GPF_tu);
%     y_est = vec(y_est);
%     y_est = y_est(ysel);
    y_est = Cs;
    
    % Noise model
    Rs = diag( abs(y + obj.nPhotonBackground) + obj.readOutNoise.^2 );
    sRs = diag(sqrt(diag(Rs)));
    siRs = diag(1./sqrt(diag(Rs)));
    
    % Deviation + Covariance inflation
    X_dev = X_tu*T_tilde;
    X_tu = x_tu*ones(N,1)' + c*X_dev;
    X_dev = X_tu*T_tilde;
    
    % Form matrix and perform eigenvalue decomposition
    Z = Js*X_dev;
    tmp = Z'*siRs;
    M = tmp*tmp';
    [C,G] = eig(M);
    
    % Ensemble propagation
%     sG = diag(sqrt(diag(G));
    sqrt_L = C*diag((1./sqrt(diag(eye(N) + G))));
    sqrt_C = C*diag((1./sqrt(diag(G))));
    X_mu = X_tu*sqrt_L;  
    
%     % Kalman gain
%     Mk = 1/(N-1)*X_dev*Z';
    tmp = [1/sqrt(N-1)*Z'; sRs];
    [~,sSkT] = qr(tmp);
    tmp = Z'/sSkT/sSkT';
    Km = 1/(N-1)*X_dev*tmp;
    
    % Measurement update
    x_mu = x_tu + Km*(y - y_est);
    % Save 
    obj.Models.RPSF = Rs;
    obj.KF.X_mu = X_mu;
    obj.KF.x_mu = x_mu;
    obj.KF.X_tu = obj.KF.X_mu;
    obj.KF.x_tu = obj.KF.x_mu;
    
    
elseif mflag == 4
    % Compute 1st order Taylor approximation
    Phase_tu = zeros(n1);
    Phase_tu(obj.pupilLogical(:)) = x_tu;
    GPF_tu = reshape(exp(1i*Phase_tu),[n1 n1]);  
    GPF_hat = GPF0.*GPF_tu;
    [Cs,Js] = Lin_PSF(obj,GPF_hat,ysel);    % Taylor approximation

    % Select y
    y = yfull(ysel);     

    % Noise model
    Rs = diag( abs(y + obj.nPhotonBackground) + obj.readOutNoise.^2 );


    % Output ensemble
    Y_tu = zeros(length(ysel),N);
    for ii = 1:N
        Phase_tu = zeros(n1);
        Phase_tu(obj.pupilLogical(:)) = X_tu(:,ii);
        GPF_tu = reshape(exp(1i*Phase_tu),[n1 n1]);  
        GPF_tu = GPF0.*GPF_tu;
        [~,complex_image] = alt_propagateThrough(GPF_tu,obj.fftphasor,p1,p1);
        if strcmp(obj.PSF_trunc_type,'ybox')
            nb = obj.ybox;
            n_pad_left = floor(0.5*(p1-nb));
            range_box = n_pad_left + 1 : n_pad_left + nb; 
            [~,complex_image] = alt_propagateThrough_box(GPF,obj.fftphasor,p1,p1,range_box);
        else
            [complex_image] = alt_propagateThrough(GPF,obj.fftphasor,p1,p1);
        end
        
        PSF_ensemble = abs(complex_image).^2;
        tmp = vec(PSF_ensemble);
        Y_tu(:,ii) = tmp(ysel);
    end

    % Deviations
    X_dev = X_tu*T_tilde;       %A
    Y_dev = Y_tu*T_tilde;       %HA

    % Covariance inflation
    X_tu = x_tu*ones(N,1)' + c*X_dev;
    X_dev = X_tu*T_tilde;

    % Kalman Gain
    % Solves a LS: K*Y_dev = X_dev
%     K = X_dev/Y_dev;

    tmp_Y = (y*ones(N,1)' - Y_tu);
    tmp_P = Rs + (1/(N-1))*(Y_dev*Y_dev');
    tmp_M = tmp_P\tmp_Y; 
    tmp_Z = Y_dev'*tmp_M; 

    % Measurement update
%     X_mu = X_tu + K*(y*ones(N,1)' - Y_tu);
    X_mu = X_tu + 1/(N-1)*X_dev*tmp_Z;
    x_mu = mean(X_mu,2);

    % Prepare for next batch
    X_tu = X_mu;
    x_tu = x_mu;

    %%% Save
    obj.KF.X_mu = X_mu;
    obj.KF.x_mu = x_mu;
%     obj.KF.K = K;
    obj.KF.X_tu = obj.KF.X_mu;
    obj.KF.x_tu = obj.KF.x_mu;
end



end