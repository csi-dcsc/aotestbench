function obj = EnKF_tu(obj)
% Assumes the model:
% x(k+1) = A x(k) + w(k)
% y(k) = h(x(k)) + v(k)
% 
% Where the linearization around x = x0 is defined by:
% y(k) = h0 + Jh*(x(k) - x0) + v(k)
%

%%% Check method
method = obj.KF.EnKF.method;
if strcmp(method,'Sampling') 
    mflag = 1;
elseif strcmp(method,'ETKF')
    mflag = 2;
elseif strcmp(method,'Direct')
    mflag = 4;
end

%%% Get data
X_mu = obj.KF.X_mu;
[n,N] = size(X_mu);

%%% Define state dynamics
% A = obj.Models.ModelPhi_pix.A;
A = obj.Models.ModelPhi_pix.Asp;
Qs = obj.Models.ModelPhi_pix.Qsqrt;    % Square root of Q!
% Qs = obj.Models.ModelPhi_pix.Qsqrt_trunc;    % Square root of Q!

% Pre compute
% T_tilde = eye(N) - 1/N*ones(N,1)*ones(N,1)';    % Transform for deviation computation

if mflag == 1 || mflag == 3     
    % Time update
%     X_tu = A*X_mu;% + Qs*randn(n,N);
    X_tu = A*X_mu + Qs*randn(n,N);
    x_tu = mean(X_tu,2);
elseif mflag == 2 || mflag == 4
    X_tu = A*X_mu;
    x_tu = mean(X_tu,2);
end

% Save
obj.KF.X_tu = X_tu;
obj.KF.x_tu = x_tu;



end