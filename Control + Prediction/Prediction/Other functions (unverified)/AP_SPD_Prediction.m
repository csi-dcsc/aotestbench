function obj = AP_SPD_Prediction(obj)

    iteration_num = 1000;
    camnrused = obj.activePDs;

    % Load data 
    d = 2;
    ni = size(obj.PSFs,1);
    n = size(obj.GPF_real,1);
    n_out = 2*obj.NyquistSampling*n;
    
    
    % Diversity image
    y = zeros(ni,ni,d);
    y(:,:,1) = obj.PSFs(:,:,camnrused); 
    y(:,:,2) = obj.PSFs_old(:,:,camnrused); 
    
    % First diversity
    P_div{1} = obj.PhaseDiversity{camnrused};
    
    % Sequential phase diversity:
    uk = obj.us_dm(:,end);      %sum(obj.us(:,1:end-1),2);
    ukm1 = obj.us_dm(:,end-1);   %sum(obj.us(:,1:end-2),2);
    uk_hr = obj.us_dm_hr(:,end); %sum(obj.us_hr(:,1:end),2);
    ukm1_hr = obj.us_dm_hr(:,end-1);   %sum(obj.us_hr(:,1:end-1),2);
    d_Phase_dm = obj.DM.Hr*uk - obj.DM.Hr*ukm1;
    d_Phase_dm_hr = obj.DM.Hr_hr*uk_hr - obj.DM.Hr_hr*ukm1_hr;
    d_Phase_dm = d_Phase_dm + d_Phase_dm_hr;
    d_Phase_dm_mat = zeros(n);
    d_Phase_dm_mat(obj.pupilLogical) = d_Phase_dm;
    P_div{2} = P_div{1} - d_Phase_dm_mat;

    % processing, square root
    y_trunc = obj.ytrunc;
    y(y < y_trunc) = 0;
    vec_sqrty = reshape(sqrt(y),[ni^2,d]);
    I_a = sqrt(y);
   
    % Initialize x 
    if norm(obj.GPF_est) > 0
        x_init = obj.GPF_est;
%                 x_init = 10*(randn(n,n) + 1i*randn(n,n));
    else
        x_init = 1*(randn(n,n) + 1i*randn(n,n));
    end
%     x_init = randn(n,1);
    
    % Initialize
    x = x_init;
%             vec_y_est = zeros(size(vec_y));
    r(1) = 1;
    stopcount = 0; 

    %%% fft phasor
    fftphasor = obj.CreatePhasor(n,n_out);

    % AP iterations
    for i=1:iteration_num
        for ii = 1:d
%             x = abs(obj.GPF_real).*exp(1i*angle(x));
            [I1] = alt_propagateThrough(x.*exp(1i*P_div{ii}),fftphasor,ni,n_out);
            I2 = I_a(:,:,ii).*exp(1i*angle(I1));
            x = alt_propagateBack(I2,fftphasor,n,n_out).*exp(-1i*P_div{ii});  
            y_est(:,ii) = vec(abs(I1));
        end
                
        % Save residual
        r(i+1) = norm(vec_sqrty - y_est)/norm(vec_sqrty);
        
        % Stopping criteria
        if abs(r(i)) - abs(r(i+1)) < 1e-5*abs(r(i+1)) && i > 10
            stopcount = stopcount + 1;
            if stopcount == 20
                break
            end
        else 
            stopcount = 0;
        end
    end
%             figure(473); plot(r);

    % Compute GPF
    obj.GPF_est = obj.pupilLogical.*x;
    obj.VGPF_est = vec(obj.GPF_est(obj.pupilLogical(:)));

%             % Get phase
%             obj.Phase_est = angle(obj.GPF_est);
    % Unwrap and remove piston
    obj.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        fun_phaseunwrap(angle(obj.GPF_est),'Miguel'))); 
    obj.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        fun_phaseunwrap(obj.Phase_est,'Miguel'))); 
    obj.VPhase_est = vec(obj.Phase_est(obj.pupilLogical(:)));

    
end
