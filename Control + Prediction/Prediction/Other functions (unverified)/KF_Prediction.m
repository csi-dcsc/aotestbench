function obj = KF_Prediction(obj)

    %%% Model selection
    R = obj.Models.ModelPhi_diag.R;
    G = obj.Models.ModelPhi_diag.G;
    A = obj.Models.ModelPhi_diag.A;
    Q = obj.Models.ModelPhi_diag.Q;     

    %%% Compute Kalman Gain
    if isempty(obj.KF)
        [~,~,K] = dare(A',G',Q,R);
        obj.KF.K = K';
    end

    %%% Compute prediction
    if isempty(obj.VPhase_est)
        obj.VPhase_est = zeros(size(A,1),1);
    end
    obj.VPhase_est = (A - obj.KF.K*G)*obj.VPhase_est + obj.KF.K*obj.WFSmeasurement;
end
