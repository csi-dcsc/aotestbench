function obj = MVM_Prediction(obj)
    obj.VPhase_est = obj.MVM*[obj.WFSmeasurement;0;0];
    % Remove Piston and Waffle
%             G = obj.Models.ModelPhi_diag.G;
%             nullG = obj.Models.ModelPhi_diag.nullG;
%             for i = 1:4
%                 obj.VPhase_est = obj.VPhase_est-(nullG(:,1))\obj.VPhase_est*nullG(:,1);
%                 obj.VPhase_est = obj.VPhase_est-(nullG(:,2))\obj.VPhase_est*nullG(:,2);
%             end
    obj.Phase_est = zeros(size(obj.validActuators));
    obj.Phase_est(obj.validActuators) = obj.VPhase_est;
end