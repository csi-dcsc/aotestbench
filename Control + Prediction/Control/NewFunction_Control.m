function obj = NewFunction_Control(obj)
    % You can use and rename this function in order to implement a new
    % control method. 
    % Make sure it finds an estimate of the complex-valued GPF: 
        GPF_est = ...;
    % If it assumes the absolute value of the GPF to be known, this might
    % be implemented as follows:
        Phase_est = ...; 
        GPF_est = abs(obj.PupilPlaneAmplitude{1}).*exp(1i*Phase_est);
    
    %%% Always make sure it outputs the following:
    % Compute GPF
        obj.GPF_est = obj.pupilLogical.*GPF_est;
        obj.VGPF_est = vec(obj.GPF_est(obj.pupilLogical(:)));

    % Unwrap and remove piston
        obj.Phase_est = obj.pupilLogical.*...
            Remove_mean(double(...
            fun_phaseunwrap(angle(obj.GPF_est),'Miguel'))); 
        obj.VPhase_est = vec(obj.Phase_est(obj.pupilLogical(:)));

    % Apply control action (just an example, feel free to change this)
        % Mapping high-resolution estimate on the DM:
        obj.u = -(obj.DM.iHr*Phase_est(obj.Phase_est ~= 0)); 
        % Or, mapping Fried-geometry resolution estimate on the DM:
        % obj.u = -obj.Models.ModelPhi_diag.iH*Phase_est(tmp.Phase_est ~= 0)); 
        % If the above u represents an increment in the DM signal:
        obj.u_dm  = obj.u_dm + obj.u;      
        % Otherwise
        %  obj.u_dm = obj.u;      
        obj.us = [obj.us obj.u]; 
end
