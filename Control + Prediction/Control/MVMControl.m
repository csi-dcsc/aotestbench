function obj = MVMControl(obj,cMVM)
    phi_est = obj.MVM*[obj.WFSmeasurement;0;0];  
%             phi_est = obj.MVM_pinvG_reg*[obj.WFSmeasurement;0;0];  
    ue = -cMVM*obj.Models.ModelPhi_diag.iH*phi_est;
    obj.u = ue;
    obj.us = [obj.us obj.u];
    obj.u_dm  = obj.u_dm + obj.u;          
    obj.us_dm = [obj.us_dm obj.u_dm]; 
end