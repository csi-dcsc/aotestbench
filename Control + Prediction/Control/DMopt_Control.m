function obj = DMopt_Control(obj)
    % Non-realistic controller mapping real phase onto DM
    % Only to be used as a theoretical upper bound of compensation
    obj.u = -obj.DM.Hr\obj.Phase_real(obj.Phase_real ~= 0);   
    obj.us = [obj.us obj.u];             
end
