function obj = AP_Control(obj)

    iteration_num = 1000;
    camnrused = obj.activePDsControl;

    % Load data 
    d = length(camnrused);
    y = obj.PSFs(:,:,camnrused); 

    y_trunc = obj.ytrunc_control;
    y(y < y_trunc) = 0;

    p = size(y,1);
    vec_sqrty = reshape(sqrt(y),[p^2,d]);
    I_a = sqrt(y);
    n = size(obj.GPF_real,1);
    n_out = 2*obj.NyquistSampling*n;
    ni = size(y,1);
    for ii = 1:d
        P_div{ii} = obj.PhaseDiversity{camnrused(ii)};
    end

    % Initialize x 
    if norm(obj.GPF_est) > 0
        x_init = obj.GPF_est;
%                 x_init = 10*(randn(n,n) + 1i*randn(n,n));
    else
        x_init = 1*(randn(n,n) + 1i*randn(n,n));
    end
%     x_init = randn(n,1);
    
    % Initialize
    x = x_init;
%             vec_y_est = zeros(size(vec_y));
    r(1) = 1;
    stopcount = 0; 

    %%% fft phasor
    fftphasor = obj.CreatePhasor(n,n_out);

    % AP iterations
    for i=1:iteration_num
        for ii = 1:d
%             x = abs(obj.GPF_real).*exp(1i*angle(x));
            [I1] = alt_propagateThrough(x.*exp(1i*P_div{ii}),fftphasor,ni,n_out);
            I2 = I_a(:,:,ii).*exp(1i*angle(I1));
            x = alt_propagateBack(I2,fftphasor,n,n_out).*exp(-1i*P_div{ii});  
            y_est(:,ii) = vec(abs(I1));
        end
                
        % Save residual
        r(i+1) = norm(vec_sqrty - y_est)/norm(vec_sqrty);
        
        % Stopping criteria
        if abs(r(i)) - abs(r(i+1)) < 1e-5*abs(r(i+1)) && i > 10
            stopcount = stopcount + 1;
            if stopcount == 20
                break
            end
        else 
            stopcount = 0;
        end
    end
%             figure(473); plot(r);

    % Compute GPF
    tmp.GPF_est = obj.pupilLogical.*x;
    tmp.VGPF_est = vec(tmp.GPF_est(obj.pupilLogical(:)));

%             % Get phase
%             obj.Phase_est = angle(obj.GPF_est);
    % Unwrap and remove piston
    tmp.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        fun_phaseunwrap(angle(tmp.GPF_est),'Miguel'))); 
    tmp.Phase_est = obj.pupilLogical.*...
        Remove_mean(double(...
        fun_phaseunwrap(tmp.Phase_est,'Miguel'))); 
    tmp.VPhase_est = vec(tmp.Phase_est(obj.pupilLogical(:)));

    
    % Apply control action
    obj.u = -(obj.DM.iHr*tmp.Phase_est(tmp.Phase_est ~= 0)); 
    obj.us = [obj.us obj.u]; 
end
