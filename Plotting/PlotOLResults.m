function PlotOLResults(Results,pred)
% Show the performance of the methods, updates every time step

%% Distance measure (Predicted GPF - Real GPF, amplitude fixed)
    subplot(3,4,1:4)
        semilogy(Results.distGPF_A,'LineWidth',1.3)
        legend([Results.Methods,{'Zero'}],'Location','southwest')
        title('NMSE of GPF estimate')
        grid on
 
%% Display the estimated wavefronts    
    n = size(Results.PredictedPhaseZM,1);
    M = length(Results.Methods);
    PhaseScreenMatrix = reshape(Results.PredictedPhaseZM,[n n*M]);
    PhaseScreenMatrix2 = [PhaseScreenMatrix Results.RealPhaseZM];
    
    subplot(3,4,5:7);   
        imagesc(PhaseScreenMatrix2)
        colorbar
        colormap('jet')
        title('Phase estimates, f.l.t.r: Methods in order, right: True zero-mean wavefront')
        caxis([-pi pi])

%% Display wavefront residuals
    PhaseScreenMatrix_res = PhaseScreenMatrix - repmat(Results.RealPhaseZM,[1 M]);
    for k = 1:3
        PhaseScreenMatrix_res(PhaseScreenMatrix_res < -pi) = PhaseScreenMatrix_res(PhaseScreenMatrix_res < -pi) + 2*pi;
        PhaseScreenMatrix_res(PhaseScreenMatrix_res > pi) = PhaseScreenMatrix_res(PhaseScreenMatrix_res > pi) - 2*pi;
    end
    
    subplot(3,4,9:10);   
        imagesc(PhaseScreenMatrix_res)
        colorbar
        colormap('jet')
        title('Phase estimation errors')
        caxis([-pi pi])
        
%%  Run times
    for i = 1:M
        xcats{i} = pred{i}.Method;
%         timeavg(i) = pred{i}.times.avg;
        timeavg(i) = pred{i}.times.all(end);
    end
    
    subplot(3,4,8);
        bar(log10(1000*timeavg))
        set(gca,'xticklabel',xcats)
        ylabel('log_{10}(t) [ms]')  
        title('Run times')
        
%% Display phase diversity PSFs
    %%% Apply a 'zoom' factor (only display the center, set zoom = 1 for full PSF)
    zoom = 2;
    ni = 2*n;
    nn = round(ni/zoom);
    if rem(nn,2) == 0
        center_left = (ni/2 - nn/2) + 1;
        center_right = (ni/2 + nn/2);
        center = center_left:center_right;
    else
        center_left = (ni/2 - (nn-1)/2) + 1;
        center_right = (ni/2 + (nn-1)/2) + 1;
        center = center_left:center_right;
    end
    PSFZoom = Results.PSFmeas(center,center,1);
    for i = 2:size(Results.PSFmeas,3)
        PSFZoom = [PSFZoom Results.PSFmeas(center,center,i)];
    end
    
    subplot(3,4,[11,12]);   
        imagesc((abs(PSFZoom)))
        colorbar
        colormap('jet')
%         colormap('gray')
        title('diversity PSFs (not all used)')

%% Update the figure 
   drawnow
end