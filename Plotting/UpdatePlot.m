%%% Update plot
if sim_prop.showplot
    figure(3); 
    if any(HighResControl == 1)
        PlotCLResults(Results,pred);
    else
        PlotOLResults(Results,pred);
    end
end