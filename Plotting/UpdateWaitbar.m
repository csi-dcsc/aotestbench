%%% Update waitbar
if ~sim_prop.showplot 
    if rem(it,2) == 0 
        sim_prop.timelapsed = toc(sim_prop.trun);
        sim_prop.ETA = (sim_prop.timelapsed/sim_prop.loopcount)*(sim_prop.N_total_loops - sim_prop.loopcount);
        waitbar(((ipar-1)*sim_prop.NMC*sim_prop.Nt + (iMC-1)*sim_prop.Nt + it)/(sim_prop.N_total_loops),hs,['ETA: ' num2str(sim_prop.ETA,4) 'sec']);       
    end
    sim_prop.loopcount = sim_prop.loopcount + 1;
end