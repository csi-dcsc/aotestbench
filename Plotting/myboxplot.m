function myboxplot(xd,Data3,styles)
%%% Boxplot that handles a distribution of data
%%% Data is given in a 3D tensor

% check if xd from low to high
[xd,ixs] = sort(xd);
Data3 = Data3(ixs,:,:);

% x-axis labels
widths = abs(diff(xd)/4);
% widths = xd/4;
% widths = ones(size(widths))*min(widths);

figure; %set(gcf,'Position',[150 100 1400 800]);
for i = 1:size(Data3,3)
    tmp = Data3(:,:,i);
    tmp_mean = mean(tmp');
    tmp_median = median(tmp');
    boxplot(tmp',xd,...
        'plotstyle','traditional',...
        'position',xd,...
        'color',styles{i}(1),...
        'MedianStyle','target',...
        'Widths',[widths],...
        'symbol',[styles{i}(1) 'x']); 
    hold on
    plot(xd,tmp_median,styles{i},'linewidth',1.5);
end

axis([min(xd)-widths(1) max(xd)+0.7*widths(end) 0.9*min(min(min(Data3))) 1.1*max(max(max(Data3)))])

end