%%% Initialize the plot, or setup waitbar
if sim_prop.showplot
    figure(3); set(gcf,'Position',[50 50 1600 900])
else
    if iMC == 1 && ipar == 1
        hs = waitbar(0,'Simulation is running');
        sim_prop.trun = tic;
        sim_prop.loopcount = 0;
    end
end