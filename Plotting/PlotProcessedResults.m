function PlotProcessedResults(Res)

%% Plot only for one inner MC run the GPF distance
% Dimensions of Res.GPFdist arhee as follows:
% (time) x (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'k:','r--','b-','m-.','c-'};

myboxplot(Res.xval(Res.xRangesel),...
    squeeze(permute(Res.GPFdist(Res.Nt0:end,Res.Methodsel,Res.xRangesel,1),[3 1 2 4])),...
    styles)
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on

%% Plot GPF distance RMS over all inner MC runs
% Dimensions of Res.rms_GPFdist are as follows:
% (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'k:','r--','b-','m-.','c-'};
myboxplot(Res.xval(Res.xRangesel),...
    permute(Res.rms_GPFdist(Res.Methodsel,Res.xRangesel,:),[2 3 1]),...
    styles)
% axis([min(Res.xval)-1 max(Res.xval)+1 0.1 0.7])
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on
xlabel(Res.xlabel, 'Interpreter', 'Latex');
% xlabel('$$ Wind speed [m/s] $$', 'Interpreter', 'Latex');
ylabel(Res.ylabel, 'Interpreter', 'Latex');


%% Plot GPF distance RMS over all inner MC runs
% Dimensions of Res.rms_GPFdist are as follows:
% (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'k:','r--','b-','m-.','c-'};
myboxplot(Res.xval(Res.xRangesel),...
    permute(Res.mean_Strehl(Res.Methodsel,Res.xRangesel,:),[2 3 1]),...
    styles)
% axis([min(Res.xval)-1 max(Res.xval)+1 0.1 0.7])
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on
xlabel(Res.xlabel, 'Interpreter', 'Latex');
% xlabel('$$ Wind speed [m/s] $$', 'Interpreter', 'Latex');
ylabel(Res.ylabel, 'Interpreter', 'Latex');


%% If there is a zero (comparison value) it will be plotted as a horizontal line
try 
    hold on; 
    plot(Res.xval,mean(Res.rms_GPFdist0(Res.Methodsel,:,:,:),3)*ones(length(Res.xval),1),...
        'k--','linewidth',1.5);
    legendcell{1} = 'Reconstructed measurements';
    legendcell{2} = 'Perfect measurements';
    legend(legendcell,'location','northeast')
catch
end


%% Plot performance vs. RMS phi over all inner MC runs
% Dimensions of Res.rms_GPFdist are as follows:
% (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'k:','r--','b-','m-.','c-'};
myboxplot(Res.xval(Res.xRangesel),...
    permute(Res.rms_GPFdist(Res.Methodsel,Res.xRangesel,:),[2 3 1]),...
    styles)
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on
xlabel(Res.xlabel, 'Interpreter', 'Latex');
ylabel(Res.ylabel, 'Interpreter', 'Latex');
ax1 = gca; % current axes
ax1.YLim = [0.1 1.3];
% Second axis
ax1 = gca; % current axes
ax1.Position(4) = 0.72;
xlimits = ax1.XLim;
ticks = Res.xval(Res.xRangesel);
ticklabels_values = round(100*Res.meanRMSPhase(Res.xRangesel))/100;
for i = 1:length(ticklabels_values)
    ticklabels{i} = num2str(ticklabels_values(i));
end
xticks_norm = (ticks - xlimits(1))/(xlimits(2)-xlimits(1));
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,'Units','Normalized',...
    'XAxisLocation','top',...
    'Color','none',...
    'xtick',xticks_norm,'xTickLabel',ticklabels,'ytick',[]);
hold on
set(gca,'fontsize',12)
xlabel('$$ RMS(\phi) [rad] $$', 'Interpreter', 'Latex');



%% Plot only for one inner MC run the GPF distance
% Dimensions of Res.GPFdist are as follows:
% (time) x (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'r--','b-','m-.','c-'};
Data = squeeze(permute(Res.runtimes(Res.Nt0:end,Res.Methodsel,Res.xRangesel,1),[3 1 2 4]));

% Remove missing data
Data([6:7],:,1) = NaN;

myboxplot(Res.xval(Res.xRangesel),...
    Data,...
    styles)
axis([min(Res.xval(Res.xRangesel))-80 max(Res.xval(Res.xRangesel))+400 0.01 600])
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on
xlabel(Res.xlabel, 'Interpreter', 'Latex');
% xlabel('$$ Wind speed [m/s] $$', 'Interpreter', 'Latex');
ylabel('Runtime [s]', 'Interpreter', 'Latex');
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')

%% Theoretic lines and plot KF if too slow
ns = [1 log10(Res.xval(Res.xRangesel)) 5];
slope3init = squeeze(median(log10(Res.runtimes(Res.Nt0:end,3,4,1))));
t3s = slope3init + 3*(ns-ns(4+1));
% t2s = slope3init + 2*(ns-ns(2));
plot(10.^(ns),10.^(t3s),'k:','LineWidth',1.5)
% plot(ns,t2s,'k')

slope1init = squeeze(median(log10(Res.runtimes(Res.Nt0:end,4,5,1))));
t1s = slope1init + 1*(ns-ns(5+1));
% t2s = slope1init + 2*(ns-ns(2));
% plot(ns,t2s,'k')
plot(10.^(ns),10.^(t1s),'k:','LineWidth',1.5)
% text(5000,1,'$$\mathcal{O}(n^3)$$','Fontsize',12,'Interpreter','latex')


xticks([100 1000 10000])
set(gca,'TickLabelInterpreter', 'tex');
xticklabels({'10^{2}','10^3','10^4'})

lastmethod = length(Res.Methodsel);
Res.MethodsLegend = cell(lastmethod,1);
for i = 1:lastmethod
    Res.MethodsLegend{i} = Res.Methods{Res.Methodsel(i)};
end
% Res.MethodsLegend{lastmethod+1} = 'Theory';
lh = legend(Res.MethodsLegend,'location','northwest');

axis([100 1.5*10^4 0.01 1000])


%%% Chose x range for slow method
% xd = Res.xval([1 2 3 4]);
% Data3 = squeeze(permute(Res.runtimes(Res.Nt0:end,3,[1 2 3 4],1),[3 1 2 4]));
% % check if xd from low to high
% [xd,ixs] = sort(xd);
% Data3 = Data3(ixs,:,:);
% 
% % x-axis labels
% widths = abs(diff(xd)/4);
% widths = ones(size(widths))*min(widths);
% hold on
%     tmp = Data3(:,:,1);
%     tmp_mean = mean(tmp');
%     tmp_median = median(tmp');
%     boxplot(tmp',xd,...
%         'plotstyle','traditional',...
%         'position',xd,...
%         'color','r',...
%         'MedianStyle','target',...
%         'Widths',[widths],...
%         'symbol','rx'); 
%     hold on
%     plot(xd,tmp_median,styles{i},'linewidth',1.5);




%% Plot runtimes over all inner MC runs
% Dimensions of Res.avgruntimes are as follows:
% (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'k:','r--','b-','m-.','c-'};
myboxplot(Res.xval(Res.xRangesel),...
    permute(Res.mean_runtime(Res.Methodsel,Res.xRangesel,:),[2 3 1]),...
    styles)
axis([-inf inf 0 600])
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on
xlabel(Res.xlabel, 'Interpreter', 'Latex');
% xlabel('$$ Wind speed [m/s] $$', 'Interpreter', 'Latex');
ylabel('Runtime [s]', 'Interpreter', 'Latex');
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')



%% Plot only for one inner MC run the GPF distance
% Dimensions of Res.GPFdist are as follows:
% (time) x (methods) x (nr. param changed) x (nr MC loops)
% last method is open loop
styles = {'k:','r--','b-','m-.','c-'};
myboxplot(log10(Res.xval(Res.xRangesel)),...
    squeeze(permute(log10(Res.runtimes(Res.Nt0:end,Res.Methodsel,Res.xRangesel,1)),[3 1 2 4])),...
    styles)
axis([2 4 -2 3])
lh = legend(Res.Methods{Res.Methodsel},'location','northeast');
%     lh.String{4} = 'AP PD';
set(gca,'fontsize',12)
grid on
xlabel(Res.xlabel, 'Interpreter', 'Latex');
% xlabel('$$ Wind speed [m/s] $$', 'Interpreter', 'Latex');
ylabel('Runtime [s]', 'Interpreter', 'Latex');



end
