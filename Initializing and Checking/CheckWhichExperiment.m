
if sim_prop.MCflag
% Check the tag that was called
tag = sim_prop.varpar;

% update the corresponding parameter
if strcmp(tag,'pvwind')
    atm_prop.pvwind = sim_prop.varparvalues(ipar)*atm_prop.pvwind0;
    pred_prop.r_graph_pix = ceil(max(atm_prop.pvwind)) + 1; % Radius constraint for VAR identification
elseif strcmp(tag,'r0')
    atm_prop.r0 = sim_prop.varparvalues(ipar);
elseif strcmp(tag,'r0_D')
    atm_prop.r0 = sim_prop.varparvalues(ipar);
    tel_prop.D = (5/6)*atm_prop.r0*wfs_prop.nL;
elseif strcmp(tag,'mag')
    source_prop.mag = sim_prop.varparvalues(ipar);
elseif strcmp(tag,'cndQ')
    pred_prop.cndQ = sim_prop.varparvalues(ipar);
elseif strcmp(tag,'cdiv')
    cam_prop.d = sim_prop.varparvalues(ipar)*[0 1 -1];
elseif strcmp(tag,'nL')
    wfs_prop.nL = sim_prop.varparvalues(ipar);
    tel_prop.n = wfs_prop.nL*sim_prop.ndivnL;
    tel_prop.D = (5/6)*atm_prop.r0*wfs_prop.nL;   
elseif strcmp(tag,'ytrunc')
    pred_prop.ytrunc = sim_prop.varparvalues(ipar);
    pred_prop.SNRtrunc = pred_prop.ytrunc/sqrt(pred_prop.ytrunc + cam_prop.read_noise^2);
elseif  strcmp(tag,'r_graph_pix')
    pred_prop.r_graph_pix = sim_prop.varparvalues(ipar);
end
end