function out = CheckMethods(Methods,ControlMethods)
% Automatically set some parameters. 

% Number of methods
M = length(Methods);

% Methods + No control
N = M+1;

% Initialize
out.nrcams = 1;     % Result viewing cameras, One for no control
out.nmcams = 0;     % Measurement cameras
out.nwfss = 0;      % SH sensors
out.ndms = 0;       % DMs

% Check all methods and change the parameters for the simulation accordingly
for i = 1:M
    % Method specific
    if strcmp(Methods{i},'MVM')         % Integrator control (Matrix-Vector-Multiplication)
        out.nwfss = out.nwfss + 1;
        out.PathType{i} = 'wfs';
        out.PredType{i} = 'no';
    elseif strcmp(Methods{i},'DMopt')   % DM optimal control (actually not realistic, just as comparison)
        out.nwfss = out.nwfss + 1;
        out.PathType{i} = 'wfs';    
        out.PredType{i} = 'no';  
%     elseif strcmp(Methods{i},'KF')      % WFS-based Kalman filter
%         out.nwfss = out.nwfss + 1;
%         out.PathType{i} = 'wfs';
%         out.PredType{i} = 'KF';    
    elseif strcmp(Methods{i},'IEKF')    % Iterated extended kalman filter
        out.nmcams = out.nmcams + 1;
        out.PathType{i} = 'cam';
        out.PredType{i} = 'KF';
%     elseif strcmp(Methods{i},'EnKF')    % Iterated extended kalman filter
%         out.nmcams = out.nmcams + 1;
%         out.PathType{i} = 'cam';
%         out.PredType{i} = 'KF';
    elseif strcmp(Methods{i},'AP')      % Alternating projections
        out.nmcams = out.nmcams + 1;
        out.PathType{i} = 'cam';
        out.PredType{i} = 'no';  
    elseif strcmp(Methods{i},'MBAP')    % Modal-based AP
        out.nmcams = out.nmcams + 1;
        out.PathType{i} = 'cam';
        out.PredType{i} = 'no';    
    elseif strcmp(Methods{i},'MethodName')    % Add a new method here!
        out.nmcams = out.nmcams + 1;
        out.PathType{i} = MethodPathType; 
%               --> replace MethodPathType with 'cam' for WFSless, 
%                   or 'wfs' for WFS-based prediction methods
        out.PredType{i} = MethodPredType; 
%               --> specify: currently this input does not have any function other than an indication for your own reference
    end
    % For all methods
        out.nrcams = out.nrcams + 1;
        out.ndms = out.ndms + 1;
end

% Check corresponding control methods and decide if we need both wfs and phase diversity cameras
for i = 1:M
    if strcmp(ControlMethods{i},'MVM')         % Matrix-Vector-Multiplication
        if strcmp(out.PathType{i},'cam')
            out.nwfss = out.nwfss + 1;         % extra WFS
            out.PathType{i} = 'wfs-cam';   
        end
    elseif strcmp(ControlMethods{i},'DMopt')   % DM optimal control (actually not realistic, just as comparison)     
        if strcmp(out.PathType{i},'cam')      
            out.nwfss = out.nwfss + 1;
            out.PathType{i} = 'wfs-cam';   
        end
    elseif strcmp(ControlMethods{i},'AP')      % Alternating projections
        if strcmp(out.PathType{i},'wfs')
            out.nmcams = out.nmcams + 1;
            out.PathType{i} = 'wfs-cam';   
        end
%     elseif strcmp(ControlMethods{i},'ControlMethodName')      
%         if strcmp(out.PathType{i},MethodPathCheck)  
              % ---> replace MethodPathCheck with 'wfs' if 
              %      ControlMethodName is a WFSless method, and to 'cam' 
              %      if it is a WFS-based method.
%             out.nmcams = out.nmcams + 1;
%             out.PathType{i} = 'wfs-cam';   
%         end        
    end
end


end