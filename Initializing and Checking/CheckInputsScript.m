
%% Check method types inputs
ntmp(1) = length(Methods);
ntmp(2) = length(ControlMethods);
ntmp(3) = length(BasisType);
ntmp(4) = length(SettingsType);
ntmp(5) = length(TruncType);
ntmp(6) = length(ModelType);
ntmp(7) = length(SIDataType);
ntmp(8) = length(PhaseDiversities);
ntmp(9) = length(PhaseDiversitiesControl);

if all(diff(sort(ntmp(ntmp ~= 0))))
    error('Input mismatch, check if all cell arrays that specify the method are of equal length')
end

if rem(tel_prop.n,2) == 1
    error('The current version of the code only works for tel_prop.n is even! This will be corrected for in the next version.')
end