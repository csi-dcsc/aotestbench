%% Short file for listing Experimental Setup parameters
%%% Include these files in Main.m to generate results for
%%% different simulation studies.
%%% Pieter Piscaer, TU Delft, 2020

%% Check the given tag
tag = sim_prop.varpar;

%% Load corresponding parameters
if strcmp(tag,'pvwind')
    atm_prop.pvwind0 = [0.73 0.93];  % Fix this to a certain ratio
    sim_prop.varparvalues = [0.5 1 2 3]; 
    sim_prop.Nt = 120;      % Number of time steps
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 2;       % Number of Monte Carlo runs
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'r0')
    sim_prop.varparvalues = [0.05 0.075 0.1 0.15 0.2];
    sim_prop.Nt = 500;      % Number of time steps
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 10;       % Number of Monte Carlo runs
%     tel_prop.D = 1;
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'r0_D')
    sim_prop.varparvalues = [0.05 0.075 0.1 0.15 0.2];
    sim_prop.Nt = 500;      % Number of time steps
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 10;       % Number of Monte Carlo runs
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'mag')
    sim_prop.varparvalues = [4 6 8 10];
    sim_prop.Nt = 150;      % Number of time steps
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 2;      % Number of Monte Carlo runs
    cam_prop.d = 1*[0 1 -1];
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'cndQ')
    sim_prop.varparvalues = [100 500 1000 2000 5000 10000];
    sim_prop.Nt = 500;      % Number of time steps
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 10;       % Number of Monte Carlo runs
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'cdiv')
    sim_prop.varparvalues = [0.5 1 2 4];
    sim_prop.Nt = 500;
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 10;
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'nL')     % Acts as demension increase
    % Also increases n and D accordingly, but since n has to be divisible
    % by nL, nL is the real constraint.
    sim_prop.ndivnL = 5;
    sim_prop.varparvalues = [4 6 8 10 14 18 22]; %  if ndivL is uneven, these have to be even
    sim_prop.Nt = 500;
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 10;
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'ytrunc')
    sim_prop.varparvalues = [0 1 3 5 10];
    sim_prop.Nt = 500;
    sim_prop.Nt_0 = 50;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 5;
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
elseif strcmp(tag,'r_graph_pix')
    sim_prop.varparvalues = [0.05 1 1.5 2 2.5 3 4 5];
    sim_prop.Nt = 6;
    sim_prop.Nt_0 = 2;     % Number of time steps of start discarded in results due to convergence
    sim_prop.NMC = 3;
    sim_prop.Nid = 20000;   % Number of time-steps for identification (if identification data has to be generated)
else
    error('The given tag is not known, specify the parameters in ExperimentSetup.m')
end

%% For all:
sim_prop.showplot = 0;
sim_prop.Npar = length(sim_prop.varparvalues);
sim_prop.N_total_loops = sim_prop.Npar*sim_prop.NMC*sim_prop.Nt;