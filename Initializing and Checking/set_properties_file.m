%%% Advanced Settings

%% Check dimensions to see compatibility
    if rem(tel_prop.n/wfs_prop.nL,1) ~= 0
        error('Dimensions do not add up')
    end

%% Check if first method is MVM
    for i = 1:length(ControlMethods)
        control_prop{i}.Type = ControlMethods{i}; 
        pred_prop.BasisType = BasisType{i};
        if strcmp(control_prop{i}.Type,'MVM')
            control_prop{i}.cMVM = 0.75;
        end
    end
    
    
%% Advanced settings    
%%% Telescope properties
    tel_prop.fov = 30;
    tel_prop.fs = 500;
    tel_prop.n_fried = wfs_prop.nL+1;
%%% Atmosphere properties
    atm_prop.L0 = 15;
%%% Cameras properties
    cam_prop.nyquistrate = 1;
    cam_prop.photon_noise = true;
    cam_prop.nPhotonBackground = 0;
%%% wfs properties
%     wfs_prop.validLenslet = 0.85;
    wfs_prop.photon_noise = false; %true;
    wfs_prop.read_noise = 0.1;
    wfs_prop.wfson = 1;
%%% DM properties
    dm_prop.cdm = 0.3; %0.3; %was 0.125; %orig 0.75
%%% Predictor properties    
%     pred_prop.max_wind_speed = atm_prop.pvwind;
%     pred_prop.max_wind_speed(pred_prop.max_wind_speed > 5) = [];
%     pred_prop.r_graph_pix = ceil(max(atm_prop.pvwind)) + 1; 
    pred_prop.ARorder = 1;
      
%% Automatic renaming and settings - please do not change    

%%% basis porperties
% basis.tag = 'dm';
% basis.r_ap = 1 + 1/tel_prop.n;

%%% source properties
source_prop.n = tel_prop.n;
source_prop.magwfs = source_prop.mag;
source_prop.PathType = MethodsChecked.PathType;

%%% Atmospheric properties
atm_prop.vwind = atm_prop.pvwind*tel_prop.fs*tel_prop.D/(tel_prop.n-1); % wind speed in [m/s]

%%% Focal plane camera properties
cam_prop.nrcams = MethodsChecked.nrcams;        % number of result viewing cameras
cam_prop.nmcams = MethodsChecked.nmcams;        % number of measuring cameras
cam_prop.ndiv = length(cam_prop.d);             % number of diversities
cam_prop.fieldstopsize = tel_prop.n;
cam_prop.fieldstopsize_fried = tel_prop.n_fried;    

%%% WFS properties
wfs_prop.n = tel_prop.n;
wfs_prop.nwfss = MethodsChecked.nwfss;        % number of wfs paths
wfs_prop.mag = source_prop.magwfs;          % Do not change, will be removed

%%% DM properties
dm_prop.ndms = MethodsChecked.ndms;          % number of DMs
dm_prop.dmres = wfs_prop.nL+1; 
% dm_prop.validActuator = logical(ApertureMask(dm_prop.dmres,0.5-0.5/n));
dm_prop.n = tel_prop.n;
dm_prop.sourcemag = source_prop.mag;        % Do not change, will be removed
dm_prop.actuatorSpacing = tel_prop.D/(dm_prop.dmres - 1);

%%% Predictor properties
pred_prop.Methods = Methods;
pred_prop.ControlMethods = ControlMethods;
pred_prop.SensorType = MethodsChecked.PathType;
pred_prop.PredType = MethodsChecked.PredType;
pred_prop.BasisType = BasisType;
pred_prop.ModelType = ModelType;
pred_prop.SettingsType = SettingsType;
pred_prop.TruncType = TruncType;
pred_prop.basis_prop = basis_prop;
pred_prop.PhaseDiversities = PhaseDiversities;
pred_prop.PhaseDiversitiesControl = PhaseDiversitiesControl;
pred_prop.DataGeneration = DataGeneration;
pred_prop.SIDataType = SIDataType;
% pred_prop.ytrunc = 0.5*(pred_prop.SNRtrunc.^2 + ...
%     sqrt(pred_prop.SNRtrunc.^4 + ...
%     4*pred_prop.SNRtrunc.^2*cam_prop.read_noise.^2));
pred_prop.SNRtrunc = pred_prop.ytrunc/sqrt(pred_prop.ytrunc + cam_prop.read_noise^2);

%% Automatic file extension name generation
fileext = [ '_n' num2str(tel_prop.n) '_nL' num2str(wfs_prop.nL) ...
            '_wv' strcat(mat2str(atm_prop.pvwind,3)) 'wd' strcat(mat2str(atm_prop.dirwind/pi,3)) ...
            'r' num2str(atm_prop.r0) '_rf' strcat(mat2str(atm_prop.r0frac,2)) ...
            'L' num2str(atm_prop.L0) '_D' num2str(tel_prop.D) ...
            '_fs' num2str(tel_prop.fs) '_mag' num2str(source_prop.mag)];
fileext(fileext == '.') = [];        
fileext(fileext == '[') = [];     
fileext(fileext == ']') = ['_'];   
fileext(fileext == ' ') = ['x'];  

% Aberration Data
filestr = [datafilebase fileext '.mat'];
% pred_prop.filestr = filestr;
% Results
resfilestr = [resfilebase sim_prop.varpar '_' fileext];
% pred_prop.resfilestr = resfilestr;
