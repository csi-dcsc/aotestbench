%%% Reset Simulation  

%%% Clear some large data structures
    clear Results basis
    
%%% Reset the predictor class and results structure
    pred = pred0;
    Results = Results0;
    for j = 1:length(dm)
        dm{j}.coefs = 0*dm{j}.coefs;            
        dm_hr{j}.coefs = 0*dm_hr{j}.coefs;        
        pred{j}.u_dm = 0*dm{j}.coefs;          
        pred{j}.us_dm = pred{j}.u_dm;       
        pred{j}.u_dm_hr = 0*dm_hr{j}.coefs;     
        pred{j}.us_dm_hr = pred{j}.u_dm_hr;     
    end  
    Results.t = 0;

