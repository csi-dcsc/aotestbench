
MCResults{ipar,iMC} = Results;
MCResults{ipar,iMC}.sim_prop = sim_prop;
MCResults{ipar,iMC}.pred_prop = pred_prop;
MCResults{ipar,iMC}.atm_prop = atm_prop;
MCResults{ipar,iMC}.cam_prop = cam_prop;
MCResults{ipar,iMC}.wfs_prop = wfs_prop;
MCResults{ipar,iMC}.tel_prop = tel_prop;
MCResults{ipar,iMC}.source_prop = source_prop;