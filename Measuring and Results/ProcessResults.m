function [PrRes] = ProcessResults(Results)

%%% Get info 
nmc = size(Results,1);                          % number of varied parameters in MC sim
innernmc = size(Results,2);                     % number of inner MC runs
nmethods = length(Results{1,1}.Methods) + 1;    % Number of methods + open loop
Nt = Results{1,1}.t;
Nt0 = Results{1,1}.sim_prop.Nt_0;

%
PrRes.Methods = Results{1,1}.Methods;
PrRes.Nt0 = Nt0;

%%% Declare sizes of result structures 
PrRes.GPFdist = zeros(Nt,nmethods,nmc,innernmc);    
PrRes.StrehlRatios = zeros(Nt,nmethods,nmc,innernmc);    
PrRes.runtimes = zeros(Nt,nmethods-1,nmc,innernmc);
PrRes.avgruntimes = zeros(nmethods-1,nmc,innernmc);
PrRes.meanRMSPhase = zeros(nmc,innernmc);
PrRes.ModelMSE = zeros(nmc,innernmc);

%%% GPF dist
for imc = 1:nmc                 % Varied parameter loop         
    for iimc = 1:innernmc       % Inner MC loop
        PrRes.GPFdist(:,:,imc,iimc) = Results{imc,iimc}.distGPF_A;
        PrRes.runtimes(:,:,imc,iimc) = Results{imc,iimc}.runtime;
        PrRes.avgruntimes(:,imc,iimc) = Results{imc,iimc}.avgruntime;
        PrRes.meanRMSPhase(imc,iimc) = mean(Results{imc,iimc}.RMSRealPhaseZM);
        PrRes.ModelMSE(imc,iimc) = Results{imc,iimc}.ModelMSE;
        PrRes.Strehl(:,:,imc,iimc) = Results{imc,iimc}.StrehlRatios;
    end
end
PrRes.meanRMSPhase = mean(PrRes.meanRMSPhase,2);

%%% GPF dist RMS
PrRes.mean_runtime = squeeze(mean(PrRes.runtimes(Nt0:end,:,:,:)));

%%% GPF dist RMS
PrRes.rms_GPFdist = squeeze(rms(PrRes.GPFdist(Nt0:end,:,:,:)));

%%% GPF dist mean
PrRes.mean_GPFdist = squeeze(mean(PrRes.GPFdist(Nt0:end,:,:,:)));

%%% Strehl mean
PrRes.mean_Strehl = squeeze(mean(PrRes.Strehl(Nt0:end,:,:,:)));
end