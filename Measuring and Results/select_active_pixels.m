function obj = select_active_pixels(obj)
% Selects active pixels of the PSF based on the truncation value and the
% distance from the center of the PSF

    %%% full PSF
    yfull = obj.yPSF;     
    p1 = sqrt(length(yfull));
    if p1 == 0
        p1 = 2*size(obj.pupilLogical,1);
    end
    [y_sort,y_idx] = sort(yfull,'descend');

    %%% Minimum pixel value truncation
    if strcmp(obj.TruncType,'ytrunc')
        y_cutoff_idx = find(abs(y_sort) <= obj.ytrunc,1);
        ysel = y_idx(1:y_cutoff_idx);
        p = length(ysel);
        obj.active_pixels = ysel;
        obj.n_active_pixels = length(ysel);
    end
    
    %%% Distance truncation
    % If used, this cuts of a square in the full PSF. Only pixels in this
    % square are considered to be valid.
 
    if strcmp(obj.TruncType,'ybox')
%         % Compute center
%         Yfull = reshape(yfull,[p1,p1]);
%         x = linspace(-1,1,p1);
%         y = linspace(-1,1,p1);
%         [X, Y] = meshgrid(x, y);
%         meanY = mean(Yfull(:));
%         COMx = mean(Yfull(:) .* X(:)) / meanY;
%         COMy = mean(Yfull(:) .* Y(:)) / meanY;
%         pix_size = 2/(p1-1);
% 
%         % Create weight mask based on the distance from the center 
%         Ymask = zeros(p1);
%         Ymask(ysel) = 1;
%         D_center = sqrt((X-COMx).^2 + (Y-COMy).^2)/pix_size;
%         ysel_D = D_center(ysel);

        % Get size of the box
        nb = obj.ybox;
        n_pad_left = floor(0.5*(p1-nb));
%         n_pad_right = p1 - nb - n_pad_left;
        range_box = n_pad_left + 1 : n_pad_left + nb; 
        ybox_mask = zeros(p1);
        ybox_mask(range_box,range_box) = 1;
        iy = 1:p1^2;
        ysel_box = iy(logical(ybox_mask(:)));
        obj.active_pixels = ysel_box;
        obj.n_active_pixels = length(ysel_box);
    end


end