function cell_pred = GetMeasurements(cell_pred,cell_cam,cell_wfs)
    % Extract measurements from all cameras and wfss

    for j = 1:size(cell_pred,1)       % nr of cameras
        
        if strcmp(cell_pred{j}.SensorType,'Camera')
            %%% Extract Camera Measurement
            camnr = cell_pred{j}.SensorNumber;
            cell_pred{j}.PSFmeasurement = zeros(cell_cam{1,camnr}.resolution(1)^2,size(cell_cam,1));
            for k = 1:size(cell_cam,1)   % nr of diversities
                cell_pred{j}.PSFmeasurement(:,k) = vec(grab(cell_cam{k,camnr}));      % read out camera
            end
        elseif strcmp(cell_pred{j}.SensorType,'WFS')
            wfsnr = cell_pred{j}.SensorNumber;
            cell_pred{j}.WFSmeasurement = cell_wfs{wfsnr}.slopes;
        end
        
    end



end