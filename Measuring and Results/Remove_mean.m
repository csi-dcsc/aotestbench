function [Phi,Phi_mean] = Remove_mean(Phi)
    % Removes the spatial mean of a 2D wavefront
    A = double(Phi ~= 0);
    [p1,p2] = size(Phi);
    if p1 ~= p2
        error('Take a square phase screen')
    end
    p = p1;     
    sumA = sum(sum(A));
    Phi = double(fun_phaseunwrap(Phi,'Miguel')).*A;
    if sumA~=0
        Phi_mean = sum(sum(Phi))/sumA;
        Phi = (Phi - Phi_mean).*A;     
    else
        Phi = Phi.*A;     
    end
end

