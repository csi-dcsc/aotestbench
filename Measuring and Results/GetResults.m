function Results = GetResults(Results,pred,ngs_fried,ngsview,rcam,cam)
%%% Each time step, this function processes the obtained data and results

%%% Short hand notations
    t = Results.t + 1;
    ap_fried = pred{1}.validActuators;
    ap = pred{1}.pupilLogical;
    n = size(ngsview{1}.phase,1);
    ni = size(rcam{1}.frame,1);
    fftphasor = pred{1}.CreatePhasor(n,ni);

%%% Process 'true' data (i.e. not estimated)
    % Fried Geometry
    Results.RealPhaseZM_fried = ngs_fried{1}.meanRmPhase; 
    Results.RealPhase_fried = ngs_fried{1}.phase;   
    Results.PhaseVar_fried(t) = var(ngs_fried{end}.meanRmPhase(ap_fried));    % variance res. phase in radians
    % Pixel grid
    Results.RealPhaseZM = ngsview{1}.meanRmPhase;      
    Results.RMSRealPhaseZM(t) = rms(Results.RealPhaseZM(ap));                   
    Results.RealPhase = ngsview{1}.phase;                     
    Results.RealGPF = ngsview{1}.wave;    
    Results.RealPhaseWrapped = angle(Results.RealGPF);
    
    %%% PSFs
    for j = 1:size(cam,1)
        Results.PSFmeas(:,:,j) = cam{j,end}.frame;
    end
    
%%% True wavefronts per lightpath (all methods + the zero estimate)
    Results.RealPhases_all = zeros(n,n,length(pred)+1);
    Results.PSFmeas_IF_all = zeros(ni,ni,length(pred)+1);
    for j = 1:length(pred)+1
        tmp = ngsview{j}.meanRmPhase;
        Results.RealPhasesZM_all(:,:,j) = tmp;
        Results.RMSRealPhasesZM(j,t) = rms(tmp(ap));
        Results.PSFmeas_IF_all(:,:,j) = rcam{j}.frame;
    end

%%% DM surfaces 
    try 
        Results.DM_surf_all;
    catch
        Results.DM_surf_all = zeros(n,n,length(pred));
        Results.DM_hr_surf_all = zeros(n,n,length(pred));
    end
    for j = 1:length(pred)
        Results.DM_hr_d_surf_all(:,:,j) = pred{j}.DM.DM_hr_surf - Results.DM_hr_surf_all(:,:,j);
        Results.DM_d_surf_all(:,:,j) = pred{j}.DM.DM_surf - Results.DM_surf_all(:,:,j);
    end
    for j = 1:length(pred)
        Results.DM_hr_surf_all(:,:,j) = pred{j}.DM.DM_hr_surf;
        Results.DM_surf_all(:,:,j) = pred{j}.DM.DM_surf;
    end

%%% Strehl
    for j = 1:length(rcam)
        Results.StrehlRatios(t,j) = rcam{j}.strehl;
    end

%%% Save results per method
for k = 1:length(pred)

    if strcmp(pred{k}.SensorType,'wfs')
    %% WFS-based methods
        %%% Fried geometry Phase estimation results
            % Zero-mean residual wavefront    
            tmp0 = Results.RealPhaseZM_fried;
            tmpx = zeros(size(ap_fried));
            tmpx(ap_fried) = pred{k}.VPhase_est;
            Results.PredictedPhaseZM_fried(:,:,k) = tmpx;    
            % Normalised mean-squared error of the wavefront
            tmp1 = Results.PredictedPhaseZM_fried(:,:,k);
            Results.PhaseNMSE_fried(t,k) = norm(tmp1(ap_fried) - tmp0(ap_fried))/norm(tmp0(ap_fried));
        
        %%% Inter and extrapolation and repeat for pixel geometry
            % Zero-mean residual wavefront    
            tmp0 = Results.RealPhaseZM;
            tmpx = Fried_to_pixels(Results.PredictedPhaseZM_fried(:,:,k),ap,'linear');
            Results.PredictedPhaseZM(:,:,k) =  Remove_mean(pred{k}.pupilLogical.*tmpx);
            % Normalised mean-squared error of the wavefront
            tmp1 = Results.PredictedPhaseZM(:,:,k);
            Results.PhaseNMSE(t,k) = norm(tmp1(ap) - tmp0(ap))/norm(tmp0(ap));
        
        %%% GPF estimation in pixel geometry
            GPF_est = abs(pred{k}.GPF_real).*exp(1i*Results.PredictedPhaseZM(:,:,k));
            [Results.distGPF(t,k),...        % GPF distance measure
             Results.distGPF_A(t,k),...      % GPF distance measure, A known exactly
             Results.GPF_est(:,:,k),...      % GPF estimate
             Results.GPF_A_est(:,:,k)] ...   % GPF estimate, A known exactly
                = Compare_GPF(GPF_est,Results.RealGPF);
        
        %%% PSF-based comparsion
            PSF_est = alt_propagateThrough(Results.GPF_est(:,:,k),fftphasor,ni,ni);
            Results.PSF_est(:,:,k) = abs(PSF_est).^2;       % Estimated PSF
            Results.PSF(:,:,k) = rcam{k}.frame;             % True (noiseless) PSF
            Results.PSFNMSE(t,k) = norm(Results.PSF(:,:,k) - Results.PSF_est(:,:,k))/norm(Results.PSF(:,:,k));     % NMSE PSF  
        
        %%% Run times
            Results.runtime(t,k) = pred{k}.times.all(t);
            Results.avgruntime(k,1) = pred{k}.times.avg; 
   
    elseif strcmp(pred{k}.SensorType,'cam') || strcmp(pred{k}.SensorType,'wfs-cam')
    %% WFSless Methods
        %%% Pixel base phase estimation results
            % Zero-mean estimated Phase  
            tmp0 = Results.RealPhaseZM;
            tmpx = zeros(size(ap));
            tmpx(ap) = pred{k}.VPhase_est;
            Results.PredictedPhaseZM(:,:,k) = Remove_mean(pred{k}.pupilLogical.*tmpx);
            % NMSE Phase
            tmp1 = Results.PredictedPhaseZM(:,:,k);
            Results.PhaseNMSE(t,k) = norm(tmp1(ap) - tmp0(ap))/norm(tmp0(ap));
        %%% GPF comparison
            GPF_est = pred{k}.GPF_est;
            [Results.distGPF(t,k),...       % GPF distance metrics
             Results.distGPF_A(t,k),...     % " A knowns    
             Results.GPF_est(:,:,k),...     % Estimated GPFs
             Results.GPF_A_est(:,:,k)] ...  % " A known
             = Compare_GPF(GPF_est,Results.RealGPF);
        %%% PSF comparsion
            PSF_est = alt_propagateThrough(Results.GPF_est(:,:,k),fftphasor,ni,ni);
            Results.PSF_est(:,:,k) = abs(PSF_est).^2;
            Results.PSF(:,:,k) = rcam{k}.frame;                         % PSF
            Results.PSFNMSE(t,k) = norm(Results.PSF(:,:,k) - Results.PSF_est(:,:,k))/norm(Results.PSF(:,:,k));
        
        %%% Run times
            Results.runtime(t,k) = pred{k}.times.all(t);
            Results.avgruntime(k,1) = pred{k}.times.avg;
            
    end
    
end

%%% GPF_error if phase is zero and amplitude known
    k = k+1;
    GPF_est = abs(Results.RealGPF);
    [Results.distGPF(t,k),Results.distGPF_A(t,k),...         % distance metrics
     Results.GPF_est(:,:,k),Results.GPF_A_est(:,:,k)] ...    % estimated GPFs
                = Compare_GPF(GPF_est,Results.RealGPF);

%%% Other
    Results.RealPhaseZM_all(:,:,t) = Results.RealPhaseZM;
    Results.t = t;

end