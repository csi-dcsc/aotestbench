function [accur, accur_angle, GPF_est, GPF_est_angle] = Compare_GPF(GPF,GPF0)
   
% Pre-processing
n = size(GPF,1);    % size of grid
A = (GPF ~=0);      % aperture
A2 = (GPF0 ~= 0);
if A ~= A2          % check
    error('GPFs are not matching')
end
N = sum(sum(A));

% Compare GPFs fully
% log_GPF0 = log(GPF0);
% log_GPF = log(GPF);
% y = log_GPF0(A) - log_GPF(A);
% e = ones(1,N);
% phi = -1i*1/N*e*y;
% GPF_est = GPF.*exp(1i*phi);
% GPF_error = GPF0 - GPF_est;
% accur = norm(GPF_error(A))./norm(GPF0(A));
piston = angle(trace(GPF0(A)'*GPF(A)));
GPF_est = double(A).*(exp(-1i*piston)*GPF);
accur = norm(GPF0(A) - GPF_est(A))/norm(GPF0(A));     

% Compare Phase only - avoid unwrapping problems
GPF2 = A.*exp(1i*angle(GPF));
piston = angle(trace(GPF0(A)'*GPF2(A)));
GPF_est_angle = double(abs(GPF0)).*(exp(-1i*piston)*GPF2);
accur_angle = norm(GPF0(A) - GPF_est_angle(A))/norm(GPF0(A));    

% log_GPF2 = log(GPF2);
% y2 = log_GPF0(A) - log_GPF2(A);
% e2 = ones(1,N);
% phi2 = -1i*1/N*e2*y2;
% GPF2_est = GPF2.*exp(1i*phi2);
% GPF2_error = GPF0 - GPF2_est;
% accur = norm(GPF2_error);
% accur_angle = norm(angle(GPF0 - GPF2_est));
% GPF_est_angle = GPF2_est;
end