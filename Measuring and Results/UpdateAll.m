function [ngs,ngswfs,ngsview,tel,dm,dm_hr,ngsfried,dm_fried] = ...
                UpdateAll(objs,ngs,ngswfs,ngsview,tel,dm,dm_hr,ngsfried,dm_fried)    
        for j = 1:length(dm)
            dm{j}.coefs = objs{j}.u_dm; %dm{j}.coefs + objs{j}.u;             % Apply DM command
        end   
        for j = 1:length(dm)
            dm_hr{j}.coefs = objs{j}.u_dm_hr; %dm_hr{j}.coefs + objs{j}.u_hr;    % Apply DM command
        end   
        +tel;                               % Propagate atmosphere
        for k = 1:size(ngs,2)
            for j = 1:size(ngs,1)
                 +ngs{j,k};                 % Noisy PSF measurements
            end
        end
        for k = 1:length(ngswfs)
            +ngswfs{k};                     % Noisy shack-hartmann measurement
        end
        for k = 1:length(ngsview)
            +ngsview{k};                    % Noisless PSFs
        end 
        
        %%% Virtual Fried telescope
        for j = 1:length(dm_fried)
            dm_fried{j}.coefs = dm_fried{j}.coefs + objs{j}.u;    % Apply DM command
        end  
        for k = 1:length(ngsfried)
            +ngsfried{k};            % Noisless PSFs
        end 
end