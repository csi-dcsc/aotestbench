function PlotCLResults(Results,pred)
% Show the performance of the methods, updates every time step

%% Distance measure (Predicted GPF - Real GPF, amplitude fixed)
    subplot(4,4,1:2)
        semilogy(Results.RMSRealPhasesZM','LineWidth',1.3)
        legend([Results.Methods,{'Zero'}],'Location','southwest')
        title('RMS of wavefront')
        grid on    
 
    subplot(4,4,3:4)
        plot(Results.StrehlRatios,'LineWidth',1.3)
        legend([Results.Methods,{'Zero'}],'Location','southwest')
        title('Strehl ratio')
        grid on        
        
%% Display the true wavefronts    
    n = size(Results.PredictedPhaseZM,1);
    M = length(Results.Methods);
    PhaseScreenMatrix = reshape(Results.RealPhasesZM_all,[n n*(M+1)]);
    PhaseScreenMatrix2 = [PhaseScreenMatrix];
    
    subplot(4,4,5:7);   
        imagesc(PhaseScreenMatrix2)
        colorbar
        colormap('jet')
        title('Residual wavefronts, f.l.t.r: Methods in order, right: no control wavefront')
        caxis([-pi pi])

%% DM surfaces
HR_mirror_d_surfaces = reshape(Results.DM_hr_d_surf_all,[n n*M]);
mirror_d_surfaces = reshape(Results.DM_d_surf_all,[n n*M]);

    subplot(4,4,13:14);   
        imagesc(HR_mirror_d_surfaces + mirror_d_surfaces)
        colorbar
        colormap('jet')
        title('Change DM surface')
        caxis(1e-7*[-1 1])  
              

    subplot(4,4,15:16);   
        imagesc(HR_mirror_d_surfaces)
        colorbar
        colormap('jet')
        title('Change high-res DM surface')
         
%% Display the estimated wavefronts    
    n = size(Results.PredictedPhaseZM,1);
    M = length(Results.Methods);
    PhaseScreenMatrix = reshape(Results.PredictedPhaseZM,[n n*(M)]);
    PhaseScreenMatrix2 = [PhaseScreenMatrix];
    
    subplot(4,4,9:11);   
        imagesc(PhaseScreenMatrix2)
        colorbar
        colormap('jet')
        title('Estimated Residual, f.l.t.r: Methods in order, right: no control wavefront')
        caxis([-pi pi])     
        
%%  Run times
    for i = 1:M
        xcats{i} = pred{i}.Method;
%         timeavg(i) = pred{i}.times.avg;
        timeavg(i) = pred{i}.times.all(end);
    end
    
    subplot(4,4,8);
        bar(log10(1000*timeavg))
        set(gca,'xticklabel',xcats)
        ylabel('log_{10}(t) [ms]')  
        title('Run times')
        
        
        
%% Display phase diversity PSFs
    %%% Apply a 'zoom' factor (only display the center, set zoom = 1 for full PSF)
    zoom = 2;
    ni = 2*n;
    nn = round(ni/zoom);
    if rem(nn,2) == 0
        center_left = (ni/2 - nn/2) + 1;
        center_right = (ni/2 + nn/2);
        center = center_left:center_right;
    else
        center_left = (ni/2 - (nn-1)/2) + 1;
        center_right = (ni/2 + (nn-1)/2) + 1;
        center = center_left:center_right;
    end
%     PSFZoom = Results.PSFmeas_IF_all(center,center,1);
%     for i = 2:size(Results.PSFmeas_IF_all(center,center,:),3)
%         PSFZoom = [PSFZoom Results.PSFmeas_IF_all(center,center,i)];
%     end
%     PSFZoom = Results.PSFmeas_IF_all(center,center,5);
    PSFZoom = Results.PSFmeas(:,:,2);
    
    subplot(4,4,12);   
        imagesc((abs(PSFZoom)))
        colorbar
        colormap('jet')
%         colormap('gray')
        title('PSF')

%% Update the figure 
   drawnow
end