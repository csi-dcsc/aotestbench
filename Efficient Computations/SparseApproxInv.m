function [X] = SparseApproxInv(A,E)
% Function that computes a sparse approximation of a matrix inv(A).
% Note: A has to be sparse and banded as well. 
% Based on Method used in: A. Haber, M.Verhaegen, Framework to trade 
% optimality for local processing in large-scale wavefront reconstruction problems

% Inputs:
% A: sparse matrix
% E: sparsity pattern of X, saved as a cell array:
%       E{i} = [index1, index2, ..., indexr];
%        denoting all non-zero indices in row i of X!

% Outputs:
% X: approximate inverse of A

% Solves the following LS problem
% min_X || I - X*A||_F^2, X having the sparsity of E

% Dimensions
n = size(A,1);
e = sparse(1,n);
nnz = 0;
for i = 1:n
    nnz = nnz + length(E{i});
end

is = zeros(1,nnz);
js = zeros(1,nnz);
xs = zeros(1,nnz);

% Solve the sparse LS
idxrange = 1:length(E{1}); 
nnzi = length(E{1});
AA = A*A';
for i = 1:n
    % Compute the LS
    Ei = E{i};
    is(idxrange) = i*ones(1,nnzi);
    js(idxrange) = Ei;
    AiAi = AA(Ei,Ei);
    xs(idxrange) = A(Ei,i)'/AiAi;
    % new range of indices
    if i < n
        nnzi = length(E{i+1});
        idxrange = idxrange(end) + [1:nnzi]; 
    end
end
X = sparse(is,js,xs);
end


