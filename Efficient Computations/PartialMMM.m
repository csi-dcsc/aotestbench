function [X] = PartialMMM(A,B,E)


% dimensions 
[n,q] = size(A);
[q1,m] = size(B);
if q ~= q1
    error('Dimension mismatch')
end

% Compute number of final non-zeros according to E
nnz = 0;
for i = 1:n
    nnz = nnz + length(E{i});
end

% declare vectors
is = zeros(1,nnz);
js = zeros(1,nnz);
xs = zeros(1,nnz);

% find all indices
idxrange = 1:length(E{1}); 
nnzi = length(E{i});
for i = 1:n
    Ei = E{i};
    is(idxrange) = i;
    js(idxrange) = Ei;
    % new range of indices
    if i < n
        nnzi = length(E{i+1});
        idxrange = idxrange(end) + [1:nnzi]; 
    end
end

% Compute matrix-matrix product
isprev = 0;
for ii = 1:nnz
    if isprev ~= is(ii)
        ai = A(is(ii),:);
    end
    bi = B(:,js(ii));
    xs(ii) = ai*bi;
    isprev = is(ii);
end

% store as spare matrix
X = sparse(is,js,xs);


end