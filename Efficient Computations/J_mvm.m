function [y] = J_mvm(J,x,varargin)
% Applies the Jacobian operator efficiently
%
% phi has to be a vectorization of a (real-valued) square wavefront!
%
% J*phi = 2*Real( 1i*D1 * 2D-DFT * D2 )*phi

% Dimensions
% n = length(x);
sn = sqrt(length(J.d2));
sni = sqrt(length(J.d1));

if length(varargin) == 1
    type = varargin{1};
else 
    type = 'standard';
end

if strcmp(type,'standard')
    % Computes 2*Real( 1i*D1 * 2D-DFT * D2 )*phi
    
    % Step 1: D2*phi
    y1 = J.d2.*x;

    % Step 2: 2D-DFT of squared y1
    y2 = vec(fft2(reshape(y1,[sn,sn]).*J.fftphasor/sni,sni,sni));

    % Step 3: D1*y2
    y3 = J.d1.*y2;

    % Step 4: scale and take imag
    y = real(2*1i*y3);
    
elseif strcmp(type,'transposed')
    % Computes 2*Real( 1i*D2 * 2D-DFT^T * D1 )*y
    
    % Step 1: D1*x
    p1 = conj(J.d1).*x;

    % Step 2: 2D-IDFT of squared p1
    P1 = reshape(p1,[sni,sni]);
    p21 = ifft2(P1);
    p2 = vec(p21(1:sn,1:sn).*sni./J.fftphasor);
    % Comments: this is equivalent to the following formulations
%     p2 = vec(alt_propagateBack(reshape(p1,[sni,sni]),J.fftphasor,sn,sni));
%     p2 = J.FF'*p1; % (if J.FF is stored in the Lin_PSF_fun.m function)

    % Step 3: D2*y2
    p3 = conj(J.d2).*p2;

    % Step 4: scale and take imag
    y = real(-2*1i*p3);
end
    
end