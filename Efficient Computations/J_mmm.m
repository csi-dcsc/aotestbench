function [Y] = J_mmm(J,X,varargin)
% Applies the Jacobian operator efficiently
%
% X has to be a real valued matrix
%
% J*X = 2*Real( 1i*D1 * 2D-DFT * D2 )*X

% Dimensions
n = length(J.d2);
ni = length(J.d1);
m = size(X,2);
if m == 1
    % Assume diagonal matrix
    diagM = true;
    m = length(X);
else 
    diagM = false;
end

if length(varargin) == 1
    type = varargin{1};
else 
    type = 'standard';
end

if strcmp(type,'standard')
    Y = zeros(ni,m);
    x = zeros(n,1);
    for i = 1:m
        if diagM
            x(i) = X(i);
            % Computes 2*Real( 1i*D1 * 2D-DFT * D2 )*X(:,i)
            Y(:,i) = J_mvm(J,x);
            % reset x
            x(i) = 0;
        else
            x = X(:,i);
            Y(:,i) = J_mvm(J,x);
        end
    end
elseif strcmp(type,'transposed')
    Y = zeros(n,m);
    x = zeros(ni,1);
    for i = 1:m
        if diagM
            x(i) = X(i);
             % Computes 2*Real( 1i*D2 * 2D-DFT^T * D1 )*X(:,i)
             Y(:,i) = J_mvm(J,x,type);
            % reset x
            x(i) = 0;
        else
            x = X(:,i);
            Y(:,i) = J_mvm(J,x,type);
        end

    end
end
    
end